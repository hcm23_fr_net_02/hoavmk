CREATE DATABASE DMS

USE [DMS]
GO

/* 1. Add at least 8 records into each created tables. */
INSERT INTO DEPT (dept_no, dept_name, admr_dept, location)
VALUES ('DEV', 'Development', 'IT', 'HCM'),
	('MKT', 'Marketing', 'IT', 'HCM'),
	('SAL', 'Sales', 'IT', 'HCM'),
	('SEC', 'Security', 'IT', 'HCM'),
	('AI', 'AI', 'AI', 'HN'),
	('IT', 'IT', 'IT', 'HCM'),
	('FAL', 'FAL', 'FAL', 'HCM'),
	('AC', 'AC', 'AC', 'HN') 
GO

INSERT INTO EMP(emp_no, last_name, first_name, dept_no, job, salary, bonus, ed_level)
VALUES ('1', 'Long', 'Tran', 'DEV', 'Developer', 1200, 1000, 2),
	('2', 'Hoang', 'Tran', 'SAL', 'Saler', 1200, 1000, 2),
	('3', 'Vu', 'Tran', 'DEV', 'Developer', 2000, 1200, 2),
	('4', 'Quang', 'Tran', 'SAL', 'Saler', 2500, 1200, 4),
	('5', 'Duc', 'Tran', 'AI', 'AI', 1200, 1000, 3),
	('6', 'Dat', 'Tran', 'FAL', 'FAL', 3500, 1000, 2),
	('7', 'Loc', 'Tran', 'FAL', 'FAL', 4000, 1000, 1),
	('8', 'Lam', 'Tran', 'DEV', 'Developer', 3200, 1000, 5)
GO

INSERT INTO EMPMAJOR(emp_no, major, major_name)
VALUES ('1', 'SE', 'Software Engineer'),
	('2', 'SAL', 'Saler'),
	('3', 'SE', 'Software Engineer'),
	('4', 'SAL', 'Saler'),
	('5', 'AI', 'AI'),
	('6', 'FAL', 'FAL'),
	('7', 'FAL', 'FAL'),
	('8', 'SE', 'Software Engineer')
GO

INSERT INTO ACT(act_no, act_des)
VALUES (1, 'Description 1'),
	(2, 'Description 2'),
	(3, 'Description 3'),
	(4, 'Description 4'),
	(5, 'Description 5'),
	(6, 'Description 6'),
	(7, 'Description 7'),
	(8, 'Description 8'),
	(99, 'Description 99')
GO


INSERT INTO EMPPROJACT(emp_no, proj_no, act_no)
VALUES ('1', 'PROJ01', 1),
	('2', 'PROJ01', 1),
	('3', 'PROJ01', 1),
	('4', 'PROJ02', 2),
	('5', 'PROJ02', 2),
	('6', 'PROJ03', 3),
	('7', 'PROJ03', 3),
	('8', 'PROJ04', 4),
	('1', 'PROJ05', 5),
	('2', 'PROJ99', 99)
GO

/* 2. Find employees who are currently working on a project or projects. Employees working on projects will have a row(s) on the EMPPROJACT table. */

SELECT *
FROM EMP
WHERE EMP.emp_no in 
	(SELECT EP.emp_no
	FROM EMPPROJACT EP)
GO

/* 3. Find all employees who major in math (MAT) and computer science (CSI). */
SELECT *
FROM EMP
WHERE EMP.emp_no in
	(SELECT EM.emp_no
	FROM EMPMAJOR EM
	WHERE EM.major = 'MAT' AND EM.major = 'CSI')
GO

/* 4. Find employees who work on all activities between 90 and 110 */
SELECT *
FROM EMP
WHERE EMP.emp_no in
	(SELECT EP.emp_no
	FROM EMPPROJACT EP
	WHERE EP.act_no BETWEEN 90 AND 110)
GO

/* 5. Provide a report of employees with employee detail information along with department aggregate information. Give >=2 solutions (Scalar Fullselect, Join, CTE, ect) */

SELECT
	E.emp_no,
	E.first_name,
	E.last_name,
	E.salary,
	DS.dept_name,
	DS.DEPT_AVG_SAL
FROM EMP E
INNER JOIN
	(SELECT D.dept_no, D.dept_name, AVG(E.salary) DEPT_AVG_SAL
	FROM EMP E
	INNER JOIN DEPT D
	ON E.dept_no = D.dept_no
	GROUP BY D.dept_no, D.dept_name) DS
ON E.dept_no = DS.dept_no

SELECT
	E.emp_no,
	E.last_name,
	E.first_name,
	E.salary,
	D.dept_no,
	D.dept_name,
	AVG(E.salary) OVER (PARTITION BY D.dept_no) DEPT_AVG_SAL
FROM EMP E
INNER JOIN DEPT D
ON E.dept_no = D.dept_no
GO

/* 6. Use CTE technique to provide a report of employees whose education levels are higher than the average education level of their respective department. */
WITH AvgDeptEdLevelCTE AS
(
	SELECT
		*,
		AVG(E.ed_level) OVER (PARTITION BY E.dept_no) dept_avg_ed
	FROM EMP E
)
SELECT *
FROM AvgDeptEdLevelCTE A
WHERE A.ed_level > A.dept_avg_ed
GO

/* 7. Return the department number, department name and the total payroll for the department that has the highest payroll. Payroll will be defined as the sum of all salaries and bonuses for the department. */

WITH DeptPayrollsCTE
AS
(
	SELECT E.dept_no, SUM(E.salary + E.bonus) Payroll
	FROM EMP E
	GROUP BY E.dept_no
)
SELECT D1.dept_no, D2.dept_name, D1.Payroll TotalPayroll
FROM DeptPayrollsCTE D1
INNER JOIN DEPT D2
ON D1.dept_no = D2.dept_no
WHERE D1.Payroll = (SELECT MAX(DP.Payroll)
					FROM DeptPayrollsCTE DP)
GO

/* 8. Return the employees with the top 5 salaries. */
-- Could be 5 employees with different salaries.
SELECT DISTINCT TOP(5)  E.*
FROM EMP E
ORDER BY E.salary DESC
GO

-- Could be many employees having the same salaries.
SELECT TOP(5) E.*
FROM EMP E
ORDER BY E.salary DESC
GO
