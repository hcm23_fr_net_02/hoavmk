CREATE DATABASE EMS;
GO

USE EMS;
GO

/* 2. Add at least 8 records into each created tables. */

INSERT INTO Department (DeptName)
VALUES ('Development'),
	('HR'),
	('Accounting'),
	('Sales'),
	('Marketing'),
	('IT'),
	('Legal'),
	('Support')
GO

INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status)
VALUES (1, 'Le Giang', '09-20-1999', 'a@gmail.com', 1, 0, '09-20-2023', 1000, 1, 0),
	(2, 'Tran Long', '12-01-2000', 'b@gmail.com', 1, 1, '09-20-2023', 2000, 1, 0),
	(3, 'Le Hoang', '05-16-2000', 'c@gmail.com',  2, 1, '09-20-2023', 1500, 2, 0),
	(4, 'Nguyen An', '11-01-2000', 'd@gmail.com',3, 1, '09-20-2023', 1200, 1, 0),
	(5, 'Nguyen Phuc', '05-26-2001', 'e@gmail.com', 2, 5, '09-20-2023', 1000, 1, 0),
	(6, 'Tran Loc', '09-20-1999', 'f@gmail.com', 1, 2, '09-20-2023', 2500, 4, 0),
	(7, 'Le Pham', '12-01-2000', 'g@gmail.com', 1, 3, '09-20-2023', 2200, 5, 0),
	(8, 'Le Lam', '12-01-2000', 'h@gmail.com', 6, 3, '09-20-2023', 1300, 5, 0),
	(9, 'Le Tai', '12-01-2000', 'j@gmail.com', 6, 3, '09-20-2022', 1300, 5, 0)
GO

INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status)
VALUES (10, 'Quang Loc', '09-20-1999', 'k@gmail.com', 1, 0, '09-20-2017', 1000, 1, 0),
	(11, 'Le Van', '12-01-2000', 'l@gmail.com', 1, 1, '09-20-2018', 2000, 1, 0),
	(12, 'Tran Nam', '05-16-2000', 'm@gmail.com',  2, 1, '09-20-2018', 1500, 2, 0)
GO

INSERT INTO SKILL (SkillName)
VALUES
	('Java'),
	('.NET'),
	('React'),
	('Angular'),
	('Database'),
	('English'),
	('Communication'),
	('Leader')
GO

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
	(1, 1, 1, '09-20-2023'),
	(1, 2, 2, '09-20-2023'),
	(2, 1, 1, '09-20-2023'),
	(3, 3, 1, '09-20-2023'),
	(4, 5, 2, '09-20-2023'),
	(5, 4, 2, '09-20-2023'),
	(6, 7, 1, '09-20-2023'),
	(7, 8, 3, '09-20-2023')
GO

/* 3. Write a stored procedure (without parameter) to update employee level to 2 of the employees that has employee level = 1 and has been working at least three year ago (from starting date). Print out the number updated records. */
CREATE PROC UpdateEmployeeLevel
AS
BEGIN
	UPDATE Employee
	SET Level = 2
	WHERE DATEDIFF(Year, StartDate, GETDATE()) >= 3
END;
GO

/* 4. Write a stored procedure (with EmpNo parameter) to print out employee�s name, employee�s email address and department�s name of employee that has been out. */
CREATE PROC EmployeeWithDepartmentInfo
	@EmpNo int
AS
BEGIN
	SELECT E.EmpName, E.Email, D.DeptName
	FROM Employee E
	INNER JOIN Department D
	ON E.DeptNo = D.DeptNo
	WHERE E.EmpNo = @EmpNo
END
GO

EXEC EmployeeWithDepartmentInfo 1
GO

/* Write a user function named Emp_Tracking (with EmpNo parameter) that return the salary of the employee has been working. */
CREATE FUNCTION Emp_Tracking(@EmpNo int)
RETURNS money
AS
BEGIN
	DECLARE @result int
	SELECT @result = E.Salary
	FROM Employee E
	WHERE E.EmpNo = @EmpNo

	RETURN @result
END
GO

DECLARE @EmpNo int = 1
SELECT Emp_Tracking(@EmpNo) Salary
GO

/* 6. Write the trigger(s) to prevent the case that the end user to input invalid employees information (level = 1 and salary >10.000.000) */
CREATE TRIGGER BeforeInsertEmployee
ON Employee
INSTEAD OF INSERT
AS
BEGIN
	IF EXISTS (
		SELECT 1
		FROM inserted I
		WHERE Level = 1 AND
			I.Salary > 10000000
	)
	BEGIN
		RAISERROR ('Invalid level and salary', 16, 1)
		ROLLBACK TRANSACTION
	END
	ELSE
	BEGIN
		INSERT INTO Employee
		SELECT * FROM inserted
	END
END
GO

DROP TRIGGER BeforeInsertEmployee
GO

CREATE TRIGGER BeforeUpdateEmployee
ON Employee
INSTEAD OF UPDATE
AS
BEGIN
	IF EXISTS (
		SELECT 1
		FROM inserted I
		WHERE Level = 1 AND
			I.Salary > 10000000
	)
	BEGIN
		RAISERROR ('Invalid level and salary', 16, 1)
		ROLLBACK TRANSACTION
	END
	ELSE
	BEGIN
		UPDATE Employee
		SET
			EmpName = I.EmpName,
			BirthDay = I.BirthDay,
			DeptNo = I.DeptNo,
			MgrNo = I.DeptNo,
			StartDate = I.StartDate,
			Salary = I.Salary,
			Status = I.Status,
			Note = I.Note,
			Level = I.Level
		FROM Employee E
		INNER JOIN inserted I
		ON E.EmpNo = I.EmpNo
	END
END
GO