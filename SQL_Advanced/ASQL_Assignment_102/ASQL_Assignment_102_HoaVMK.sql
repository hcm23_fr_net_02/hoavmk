CREATE DATABASE SQLAdvanced102;
GO

USE SQLAdvanced102;
GO

/* Create the tables (with the most appropriate field/column constraints & types) and add at least 3 records into each created table. */

CREATE TABLE San_Pham (
	Ma_SP int PRIMARY KEY IDENTITY(1, 1),
	Ten_SP nvarchar(100) NOT NULL,
	Don_Gia decimal(18, 2) NOT NULL
);
GO

CREATE TABLE Khach_Hang (
	Ma_KH int PRIMARY KEY IDENTITY(1, 1),
	Ten_KH nvarchar(100) NOT NULL,
	Phone_No nvarchar(11) NOT NULL,
	Ghi_Chu text
);
GO

CREATE TABLE Don_Hang (
	Ma_DH int PRIMARY KEY IDENTITY(1, 1),
	Ngay_DH date NOT NULL,
	Ma_SP int NOT NULL FOREIGN KEY REFERENCES San_Pham(Ma_SP),
	So_Luong int NOT NULL,
	Ma_KH int NOT NULL FOREIGN KEY REFERENCES Khach_Hang(Ma_KH)
);
GO

INSERT INTO San_Pham (Ten_SP, Don_Gia)
VALUES ('T-shirt', 50),
	('Coffee mug', 10),
	('Hat', 15)
GO

INSERT INTO Khach_Hang (Ten_KH, Phone_No)
VALUES ('Long Pham', '0912382130'),
	('Le Loc', '0912382132'),
	('Tran Dai', '0912382133'),
	('Tran Duong', '0912382134')
GO

INSERT INTO Don_Hang (Ngay_DH, Ma_SP, So_Luong, Ma_KH)
VALUES ('03-28-2022', 1, 2, 1),
	('06-09-2022', 1, 2, 1),
	('05-25-2022', 2, 1, 3),
	('07-28-2022', 1, 2, 2),
	('11-02-2023', 1, 2, 4)
GO

CREATE TABLE Department (
	DepartmentNumber int PRIMARY KEY IDENTITY(1, 1),
	DepartmentName nvarchar(100) NOT NULL
)
GO

CREATE TABLE Employee (
	EmployeeNumber int PRIMARY KEY IDENTITY(1, 1),
	EmployeeName nvarchar(100) NOT NULL,
	DepartmentNumber int NOT NULL FOREIGN KEY REFERENCES Department(DepartmentNumber)
);
GO

CREATE TABLE Skill (
	SkillCode int PRIMARY KEY IDENTITY(1, 1),
	SkillName nvarchar(100) NOT NULL
);
GO

CREATE TABLE EmployeeSkill (
	EmployeeNumber int NOT NULL FOREIGN KEY REFERENCES Employee(EmployeeNumber),
	SkillCode int NOT NULL FOREIGN KEY REFERENCES Skill(SkillCode),
	Date_Registered date NOT NULL,
	PRIMARY KEY (EmployeeNumber, SkillCode)
);
GO

INSERT INTO Department (DepartmentName)
VALUES ('Development'),
	('Sales'),
	('Marketing'),
	('Support')
GO

INSERT INTO Employee (EmployeeName, DepartmentNumber)
VALUES ('Nguyen An', 1),
	('Tran Long', 1),
	('Le Loc', 1),
	('Ninh Gia', 2),
	('Quang Loc', 2),
	('An Hong', 3)
GO

INSERT INTO Skill (SkillName)
VALUES ('Java'),
	('.NET'),
	('Embedded'),
	('SQL')
GO

INSERT INTO EmployeeSkill (EmployeeNumber, SkillCode, Date_Registered)
VALUES (1, 1, '12-20-2020'),
	(1, 2, '02-20-2021'),
	(1, 3, '02-25-2021'),
	(2, 2, '02-20-2021'),
	(2, 3, '02-20-2021'),
	(3, 1, '02-20-2021'),
	(3, 4, '02-20-2021'),
	(4, 1, '02-20-2021'),
	(5, 3, '02-20-2021')
GO

/*
 2. Specify the names of the employees whose have skill of �Java� � give >=2 solutions:              
	a. Use JOIN selection
	b. Use sub query
*/

SELECT E.EmployeeName
FROM Employee E
INNER JOIN EmployeeSkill ES
ON E.EmployeeNumber = ES.EmployeeNumber
INNER JOIN Skill S
ON ES.SkillCode = S.SkillCode
WHERE S.SkillName = 'Java'
GO

SELECT E.EmployeeName
FROM Employee E
INNER JOIN	
	(SELECT ES.SkillCode, ES.EmployeeNumber
	FROM Skill S
	INNER JOIN EmployeeSkill ES
	ON S.SkillCode = ES.SkillCode
	WHERE S.SkillName = 'Java') JS
ON E.EmployeeNumber = JS.EmployeeNumber
GO

/* 3. Specify the departments which have >=3 employees, print out the list of departments� employees right after each department. */
SELECT C.DepartmentNumber, D.DepartmentName, C.EmployeeNumber, C.EmployeeName
FROM
	(SELECT
		*,
		COUNT(E.EmployeeNumber) OVER (PARTITION BY E.DepartmentNumber) num_employees
	FROM Employee E) C
INNER JOIN Department D
ON C.DepartmentNumber = D.DepartmentNumber
WHERE C.num_employees >= 3
GO

/* 4. Use SUB-QUERY technique to list out the different employees (include employee number and employee names) who have multiple skills. */
SELECT * FROM
Employee E
WHERE E.EmployeeNumber in
	(SELECT ES.EmployeeNumber
	FROM EmployeeSkill ES
	GROUP BY ES.EmployeeNumber
	HAVING COUNT(ES.SkillCode) > 1)
GO

/* 5. Create a view to show different employees (with following information: employee number and employee name, department name) who have multiple skills. */
CREATE VIEW EmployeesHaveSkillsView
AS
SELECT *
FROM Employee E
WHERE E.EmployeeNumber in
	(SELECT ES.EmployeeNumber
	FROM EmployeeSkill ES
	GROUP BY ES.EmployeeNumber
	HAVING COUNT(ES.SkillCode) > 1)
GO

SELECT * FROM EmployeesHaveSkillsView
