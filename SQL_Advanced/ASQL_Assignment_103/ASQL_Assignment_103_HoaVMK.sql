DROP DATABASE IF EXISTS SQLAdvanced103;

CREATE DATABASE SQLAdvanced103;
GO

USE SQLAdvanced103;
GO

/* 1. Create the tables with the most appropriate/economic field/column constraints & types. Add at least 8 records into each created tables. */

CREATE TABLE EMPLOYEE (
	EmpNo int PRIMARY KEY,
	EmpName nvarchar(50) NOT NULL,
	BirthDay date NOT NULL,
	Email nvarchar(255) NOT NULL UNIQUE,
	DeptNo int NOT NULL,
	MgrNo int NOT NULL DEFAULT(0),
	StartDate date NOT NULL,
	Salary decimal(18, 2) NOT NULL,
	Level int NOT NULL CHECK (Level between 0 and 7),
	Status int NOT NULL CHECK (Status between 0 and 2) DEFAULT 0,
	Note text
)
GO

CREATE TABLE SKILL (
	SkillNo int PRIMARY KEY IDENTITY(1, 1),
	SkillName nvarchar(30) NOT NULL,
	Note text
);
GO

CREATE TABLE DEPARTMENT (
	DeptNo int PRIMARY KEY IDENTITY(1, 1),
	DeptName nvarchar(30) NOT NULL,
	Note text
);
GO

CREATE TABLE EMP_SKILL (
	SkillNo int NOT NULL FOREIGN KEY REFERENCES SKILL(SkillNo),
	EmpNo int NOT NULL FOREIGN KEY REFERENCES EMPLOYEE(EmpNo),
	SkillLevel int CHECK (SkillLevel between 1 and 3),
	RegDate date NOT NULL,
	Description text,
	PRIMARY KEY(SkillNo, EmpNo)
);
GO

INSERT INTO DEPARTMENT (DeptName)
VALUES ('Development'),
	('HR'),
	('Accounting'),
	('Sales'),
	('Marketing'),
	('Security'),
	('IT'),
	('Support')
GO

INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, StartDate, Salary, Level, Status)
VALUES (1, 'Le Giang', '09-20-1999', 'a@gmail.com', 1, '09-20-2023', 1000, 1, 0),
	(2, 'Tran Long', '12-01-2000', 'b@gmail.com', 1, '09-20-2023', 2000, 2, 0),
	(3, 'Le Hoang', '05-16-2000', 'c@gmail.com',  2, '09-20-2023', 2100, 2, 0),
	(4, 'Nguyen An', '11-01-2000', 'd@gmail.com',3, '09-20-2023', 3000, 3, 0),
	(5, 'Nguyen Phuc', '05-26-2001', 'e@gmail.com', 5, '09-20-2023', 2500, 1, 0),
	(6, 'Tran Loc', '09-20-1999', 'f@gmail.com', 1, '09-20-2023', 2000, 4, 0),
	(7, 'Le Pham', '12-01-2000', 'g@gmail.com', 1, '09-20-2023', 1000, 5, 0),
	(8, 'Quang Tran', '12-02-2001', 'h@gmail.com', 1, '09-20-2023', 2000, 6, 1),
	(9, 'Do An', '12-01-2000', 'i@gmail.com', 1, '04-15-2022', 1200, 5, 0),
	(10, 'Van Hoang', '12-02-2001', 'j@gmail.com', 1, '02-21-2022', 2300, 2, 1)
GO

INSERT INTO SKILL (SkillName)
VALUES
	('Java'),
	('.NET'),
	('React'),
	('Angular'),
	('SQL'),
	('Devops'),
	('English'),
	('Embedded')
GO

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
	(1, 1, 1, '09-20-2023'),
	(1, 2, 2, '09-20-2023'),
	(2, 1, 1, '09-20-2023'),
	(3, 3, 1, '09-20-2023'),
	(2, 2, 1, '09-20-2023'),
	(3, 8, 2, '09-22-2022'),
	(3, 1, 1, '09-22-2022'),
	(4, 5, 3, '09-22-2022'),
	(4, 6, 2, '09-22-2022'),
	(5, 7, 3, '09-22-2022'),
	(5, 2, 1, '09-22-2022')
GO

/* 2. Specify name, email and department name of the employees that have been working at least six months. */
SELECT E.EmpName, E.Email, D.DeptName
FROM Employee E
INNER JOIN Department D
ON E.DeptNo = D.DeptNo
WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6
GO

/* 3. Specify the names of the employees whore have either 'C++' or '.NET' skills. */
SELECT E.EmpName
FROM Employee E
INNER JOIN Emp_Skill ES
ON E.EmpNo = ES.EmpNo
INNER JOIN Skill S
ON ES.SkillNo = S.SkillNo
WHERE S.SkillName = '.NET' OR S.SkillName = 'C++'
GO

UPDATE Employee SET MgrNo = 1 WHERE EmpNo = 2
UPDATE Employee SET MgrNo = 2 WHERE EmpNo = 3
UPDATE Employee SET MgrNo = 3 WHERE EmpNo = 4
UPDATE Employee SET MgrNo = 5 WHERE EmpNo = 4
GO

/* 4. List all employee names, manager names, manager emails of those employees. */
SELECT E.EmpName, M.EmpName ManagerName, M.Email ManagerEmail
FROM Employee E
LEFT JOIN Employee M
ON E.EmpNo = M.MgrNo
GO

/* 5. Specify the departments which have >=2 employees, print out the list of departments' employees right after each department. */
SELECT
	DE.DeptNo,
	DE.DeptName,
	DE.EmpNo,
	DE.DeptName,
	DE.BirthDay,
	DE.MgrNo,
	DE.StartDate,
	DE.Salary,
	DE.Email,
	DE.Level,
	DE.Status
FROM
	(SELECT
		D.DeptNo,
		D.DeptName,
		E.EmpNo,
		E.EmpName,
		E.BirthDay,
		E.StartDate,
		E.MgrNo,
		E.Salary,
		E.Status,
		E.Level,
		E.Email,
		COUNT (E.EmpNo) OVER (PARTITION BY E.DeptNo) AS num_employees
	FROM Employee E
	INNER JOIN Department D
	ON E.DeptNo = D.DeptNo) DE
WHERE num_employees >= 2
GO

/* 6. List all name, email and skill number of the employees and sort ascending order by employee's name. */
SELECT E.EmpName, E.Email, C.num_skills
FROM Employee E
INNER JOIN
	(SELECT E.EmpNo, COUNT(ES.SkillNo) num_skills
	FROM Employee E
	LEFT JOIN Emp_Skill ES
	ON E.EmpNo = ES.SkillNo
	GROUP BY E.EmpNo) C
ON E.EmpNo = C.EmpNo
ORDER BY E.EmpName
GO

/* 7. Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills. */
SELECT E.EmpName, E.Email, E.BirthDay
FROM Employee E
INNER JOIN
	(SELECT ES.EmpNo
	FROM Emp_Skill ES
	GROUP BY ES.EmpNo
	HAVING COUNT(ES.SkillNo) > 1) MS
ON E.EmpNo = MS.EmpNo
WHERE E.Status = 0
GO

/* 8. Create a view to list all employees are working (include: name of employee and skill name, department name) */
CREATE VIEW WorkingEmployees
AS
SELECT E.EmpName, E.Email, E.BirthDay
FROM Employee E
WHERE E.Status = 0
GO

SELECT * FROM WorkingEmployees
