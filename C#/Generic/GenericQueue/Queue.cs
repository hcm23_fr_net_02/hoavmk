namespace GenericQueue;

public class Queue<T> where T : notnull
{
    private T[] _array;

    // Lưu sức chứa của queue
    private int _capacity;

    // biến đung để theo dõi đầu của queue
    private int _front = 0;

    // biến dùng để theo dõi đuôi của queue,
    // khi rear đạt đến cuối mảng, sẽ được quay lại đầu của mảng
    private int _rear = -1;

    // biến dùng để đếm số lượng phần tử trong queue
    private int _count = 0;

    // Constructor tạo queue với sức chứa mong muốn (mặc định = 100)
    public Queue(int capacity = 100)
    {
        ValidateQueueCapacity(capacity);
        _capacity = capacity;
        _array = new T[capacity];
    }

    // Kiểm tra sức chứa của queue, sau đó thêm phẩn từ vào queue
    // và cập nhật lại rear, count
    public void Enqueue(T value)
    {
        CheckQueueCapacity();

        _rear = (_rear + 1) % _capacity;
        _array[_rear] = value;
        _count++;
    }

    // Kiểm tra queue rỗng, sau đó trả về phần tử ở đầu queue (phần từ cũ nhất)
    public T Peek()
    {
        CheckQueueEmpty();

        return _array[_front];
    }

    // Kiểm tra queue rỗng, sau đó lấy phần tử ở đầu ra khỏi queue
    // và cập nhật lại front và count
    public T Dequeue()
    {
        CheckQueueEmpty();

        T value = _array[_front];

        _front = (_front + 1) % _capacity;
        _count--;

        return value;
    }

    public bool IsEmpty()
    {
        return _count == 0;
    }

    public void Clear()
    {
        Array.Clear(_array);
        _count = 0;
        _front = 0;
        _rear = -1;
    }

    // Kiểm tra sức chứa của queue
    private void CheckQueueCapacity()
    {
        if (_count == _capacity)
        {
            throw new InvalidOperationException("Queue is full.");
        }
    }

    // Kiểm tra queue rỗng
    private void CheckQueueEmpty()
    {
        if (_count == 0)
        {
            throw new InvalidOperationException("Queue is empty.");
        }
    }

    private void ValidateQueueCapacity(int capacity)
    {
        if (capacity < 1)
            throw new ArgumentException("Queue capacity must be larger than 0.");
    }
}
