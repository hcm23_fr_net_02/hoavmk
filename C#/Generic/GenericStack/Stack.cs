namespace GenericStack;

public class Stack<T> where T : notnull
{
    private T[] _array;
    private int _current = -1;
    private int _count = 0;
    private int _capacity;

    public Stack(int capacity)
    {
        ValidateStackCapacity(capacity);

        _capacity = capacity;
        _array = new T[capacity];
    }

    public void Push(T element)
    {
        CheckEnoughCapacity();

        _array[++_current] = element;
        _count++;
    }

    public T Peek()
    {
        CheckEmptyStack();

        return _array[_current];
    }

    public T Pop()
    {
        CheckEmptyStack();

        var value = _array[_current];

        _current--;
        _count--;

        return value;
    }

    public int Count => _count;

    public bool IsEmpty()
    {
        return _count == 0;
    }

    public void Clear()
    {
        Array.Clear(_array);
        _count = 0;
        _current = -1;
    }



    private void CheckEnoughCapacity()
    {
        if (_count >= _capacity)
        {
            throw new InvalidOperationException("Stack is full.");
        }
    }

    private void CheckEmptyStack()
    {
        if (_count == 0)
        {
            throw new InvalidOperationException("Stack is empty.");
        }
    }

    private void ValidateStackCapacity(int capacity)
    {
        if (capacity < 1)
            throw new ArgumentException("Stack capacity must be larger than 0.");
    }
}
