namespace TodoApp;

public class Todo
{
    public string Id { get; set; } = null!;

    public string Task { get; set; } = null!;

    public int Priority { get; set; }

    public bool Completed { get; set; }

    public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

    public DateTime? CompletedAt { get; set; } = null;
}
