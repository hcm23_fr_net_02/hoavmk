namespace TodoApp;

public interface ITodoRepository
{
    Task<List<Todo>> GetTodosAsync();
    Task<Todo?> GetTodoByIdAsync(string id);
    Task AddTodoAsync(Todo todo);
    Task DeleteTodoAsync(string id);
    Task UpdateTodoAsync(string id, Todo todo);
}
