﻿using TodoApp;

const string ApiUrl = "https://651e32f044e393af2d5a970f.mockapi.io/api/v1/todos";
var todoRepository = new TodoRepository(new HttpClient(), ApiUrl);
var app = new App(todoRepository);

while (true)
{
    PrintMenu();
    Console.Write("Select an option: ");

    string? choice = Console.ReadLine();

    if (string.IsNullOrEmpty(choice))
    {
        Console.WriteLine("Please select an option!");
        continue;
    }

    switch (choice)
    {
        case "1":
            await app.ShowTodosAsync();
            break;
        case "2":
            await app.AddTodoAsync();
            break;
        case "3":
            await app.RemoveTodoAsync();
            break;
        case "4":
            await app.MarkTodoAsCompletedAsync();
            break;
        case "5":
            await app.IncreasePriorityAsync();
            break;
        case "6":
            return;
        default:
            Console.WriteLine("Invalid option! Please select again!");
            break;
    }
}

void PrintMenu()
{
    Console.WriteLine("----------------------------Todo App----------------------------");
    Console.WriteLine("1. Show todos");
    Console.WriteLine("2. Add a new todo");
    Console.WriteLine("3. Delete a todo");
    Console.WriteLine("4. Mark a todo as completed");
    Console.WriteLine("5. Increase priority");
    Console.WriteLine("6. Exit");
}
