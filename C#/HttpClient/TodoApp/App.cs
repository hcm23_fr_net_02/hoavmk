using System.Net.Http.Json;

namespace TodoApp;

public class App
{
    private readonly ITodoRepository _todoRepository;

    public App(ITodoRepository todoRepository)
    {
        _todoRepository = todoRepository;
    }
    
    public async Task ShowTodosAsync()
    {
        Console.WriteLine("1. Show todos based on created date");
        Console.WriteLine("2. Show todos based on priority");
        Console.WriteLine("3. Exit");
        Console.Write("Please select an option: ");

        if (int.TryParse(Console.ReadLine(), out var option))
        {
            if (option != 1 && option != 2)
            {
                return;
            }

            var todos = await _todoRepository.GetTodosAsync();

            if (option == 1)
            {
                var sortedTodos = todos.OrderByDescending(t => t.CreatedAt).ToList();
                PrintTodos(sortedTodos);
                return;
            }


            var sortedTodosByPriority = todos.OrderByDescending(t => t.Priority).ToList();
            PrintTodos(sortedTodosByPriority);
        }
        else
        {
            Console.WriteLine("Invalid option!");
        }
    }

    public async Task AddTodoAsync()
    {
        Console.Write("Enter task: ");

        string? task = Console.ReadLine();

        if (string.IsNullOrEmpty(task))
        {
            Console.WriteLine("Task is required.");
            return;
        }

        Console.Write("Enter priority: ");

        if (int.TryParse(Console.ReadLine(), out int priority))
        {
            var todo = new Todo
            {
                Task = task,
                Priority = priority
            };

            await _todoRepository.AddTodoAsync(todo);
            Console.WriteLine("Added successfully!");
        }
        else
        {
            Console.WriteLine("Priority must be a number.");
        }
    }

    public async Task RemoveTodoAsync()
    {
        Console.Write("Enter the id of todo: ");
        string? todoId = Console.ReadLine();

        if (string.IsNullOrEmpty(todoId))
        {
            Console.WriteLine("Invalid todo's id");

            return;
        }

        await _todoRepository.DeleteTodoAsync(todoId);
        Console.WriteLine("Deleted successfully!");
    }

    public async Task MarkTodoAsCompletedAsync()
    {
        Console.Write("Enter the id of todo: ");

        string? todoId = Console.ReadLine();

        if (string.IsNullOrEmpty(todoId))
        {
            Console.WriteLine("Invalid todo's id");

            return;
        }

        Todo? todo = await _todoRepository.GetTodoByIdAsync(todoId);

        if (todo == null)
        {
            Console.WriteLine($"Todo with id = {todoId} was not found.");

            return;
        }

        todo.Completed = true;
        todo.CompletedAt = DateTime.UtcNow;
        await _todoRepository.UpdateTodoAsync(todoId, todo);
        Console.WriteLine("Mark completed successfully!");
    }

    public async Task IncreasePriorityAsync()
    {
        Console.Write("Enter the id of todo: ");
        
        string? todoId = Console.ReadLine();

        if (string.IsNullOrEmpty(todoId))
        {
            Console.WriteLine("Invalid todo's id");

            return;
        }

        Todo? todo = await _todoRepository.GetTodoByIdAsync(todoId);

        if (todo == null)
        {
            Console.WriteLine($"Todo with id = {todoId} was not found.");

            return;
        }

        todo.Priority++;
        await _todoRepository.UpdateTodoAsync(todoId, todo);
        Console.WriteLine("Increase priority successfully!");
    }



    private void PrintTodos(List<Todo> todos)
    {
        var index = 1;
        Console.WriteLine("--------------------------My todo list--------------------------");
        foreach (var todo in todos)
        {
            Console.WriteLine($"{index}. Id = {todo.Id}, Task = {todo.Task}"
                + $", Completed = {todo.Completed}, Priority = {todo.Priority}"
                + $", Created At = {todo.CreatedAt}{(todo.CompletedAt != null ? $", Completed At = {todo.CompletedAt}" : "")}");
        }
    }

}
