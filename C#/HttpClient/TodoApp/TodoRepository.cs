
using System.Net.Http.Json;

namespace TodoApp;

public class TodoRepository : ITodoRepository
{
    private readonly HttpClient _httpClient;
    private readonly string _apiUrl;

    public TodoRepository(HttpClient httpClient, string apiUrl)
    {
        _httpClient = httpClient;
        _apiUrl = apiUrl;
    }

    public async Task AddTodoAsync(Todo todo)
    {

        HttpResponseMessage response = await _httpClient.PostAsJsonAsync(_apiUrl, todo);
        
        if (!response.IsSuccessStatusCode)
        {
            Console.WriteLine("Can not send the request!");
        }
    }

    public Task DeleteTodoAsync(string id)
    {
        throw new NotImplementedException();
    }

    public async Task<List<Todo>> GetTodosAsync()
    {
        HttpResponseMessage response = await _httpClient.GetAsync(_apiUrl);

        if (response.IsSuccessStatusCode)
        {
            var todos = await response.Content.ReadFromJsonAsync<List<Todo>>();

            if (todos != null)
            {
                return todos;
            }
        }
        else
        {
            Console.WriteLine("Can not send the request!");
        }


        return new List<Todo>();
    }

    public async Task<Todo?> GetTodoByIdAsync(string id)
    {
        HttpResponseMessage response = await _httpClient.GetAsync($"{_apiUrl}/{id}");

        if (!response.IsSuccessStatusCode)
        {
            return null;
        }

        return await response.Content.ReadFromJsonAsync<Todo>();
    }

    public async Task UpdateTodoAsync(string id, Todo todo)
    {
        HttpResponseMessage response = await _httpClient.PutAsJsonAsync($"{_apiUrl}/{todo.Id}", todo);

        if (!response.IsSuccessStatusCode)
        {
            Console.WriteLine("Can not send the request!");
        }
    }
}
