﻿
using ImageDownloader;

var urls = new List<string>()
{
    "https://purepng.com/public/uploads/large/nature-8gp.png",
    "https://images.hdqwalls.com/wallpapers/beautiful-greenery-landscape-cl.jpg",
    "https://images.hdqwalls.com/wallpapers/cloud-horizon-landscape-nature-road-a4.jpg"
};

var httpClient = new HttpClient();


var imageDownloader = new Downloader(httpClient);
await imageDownloader.DownloadImagesAsync(urls);
