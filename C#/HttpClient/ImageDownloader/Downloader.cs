namespace ImageDownloader;

public class Downloader
{
    private readonly HttpClient _httpClient;

    public Downloader(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task DownloadImagesAsync(List<string> imageUrls)
    {
        var tasks = new List<Task>();

        foreach (var url in imageUrls)
        {
            tasks.Add(DownloadImageAsync(url));
        }

        await Task.WhenAll(tasks);
    }

    private async Task DownloadImageAsync(string imageUrl)
    {
        try
        {
            var response = await _httpClient.GetAsync(imageUrl);

            if (response.IsSuccessStatusCode)
            {
                string? contentType = response.Content.Headers.ContentType.ToString();

                if (!string.IsNullOrEmpty(contentType))
                {
                    string extension = contentType.Replace("image/", "");

                    if (extension == "jpeg")
                        extension = extension.Replace("jpeg", "jpg");

                    byte[] bytes = await response.Content.ReadAsByteArrayAsync();

                    await File.WriteAllBytesAsync($"images/{DateTime.UtcNow.Ticks}.{extension}", bytes);
                }
                
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }


}
