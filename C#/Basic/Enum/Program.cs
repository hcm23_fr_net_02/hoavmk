﻿
// - enum là một value type định nghĩa một tập hợp các hằng số có tên
// - enum giúp code tránh sử dụng các giá trị số cứng, dễ đọc và dễ hiểu hơn
// - enum không thể thay đổi giá trị sau khi đã định nghĩa
// - mỗi giá trị enum có một giá trị số nguyên tương ứng (bắt đầu từ 0 và tăng đần theo mỗi phần tử enum)
// và ta có thể thay đổi giá trị này cho từng phần tử enum

public enum DaysOfWeek
{
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}

// mặc định kiểu giá trị số nguyên tương ứng của enum là int
// và ta có thể chỉ định kiểu số nguyên cho enum

public enum ErrorCode : ushort
{
    None = 0,
    Unknown = 1,
    ConnectionLost = 100,
    OutlierReading = 200
}

class Program
{
    static void Main()
    {
        // Sử dụng enum để khai báo biến
        DaysOfWeek today = DaysOfWeek.Wednesday;

        // So sánh và in giá trị enum
        if (today == DaysOfWeek.Wednesday)
        {
            Console.WriteLine("Today is wednesday.");
        }

        // Chuyển đổi giá trị enum thành số nguyên
        int dayNumber = (int)today; // dayNumber sẽ là 2
    }
}
