﻿
/* - struct: là một kiểu value type (biến của value type chứa instance của type đó
* ngược lại biến của reference type chứa tham chiếu đến một instance của type)
* - struct được dùng để lưu trữ những dữ liệu nhỏ
* và được sao chép giá trị khi gán hoặc truyền vào hàm
* - struct không thể được kế kế thừa từ struct hoặc class khác và class khác kế thừa từ struct
* tuy nhiên struct có thể implement interfaces
*/

public struct Coords
{
    public int X;
    
    public int Y;

    public Coords(int x, int y)
    {
        X = x;
        Y = y;
    }

    public override string ToString() => $"({X}, {Y})";
}

// readonly struct được sử dụng để khai báo một immutable struct
public readonly struct Coords1
{
    public Coords1(double x, double y)
    {
        X = x;
        Y = y;
    }

    public double X { get; init; }
    public double Y { get; init; }

    public override string ToString() => $"({X}, {Y})";
}

public class Program
{
    static void Main()
    {
         // Khởi tạo và sử dụng một biến kiểu struct
        Coords coords1 = new(10, 20);
        Coords coords2 = coords1; // Sao chép giá trị từ coords1 vào coords2

        Console.WriteLine($"coords1: X = {coords1.X}, Y = {coords1.Y}");
        Console.WriteLine($"coords2: X = {coords2.X}, Y = {coords2.Y}");

        // Thay đổi giá trị trong coords2 không ảnh hưởng đến coords1
        coords2.X = 30;

        Console.WriteLine($"coords1: X = {coords1.X}, Y = {coords1.Y}");
        Console.WriteLine($"coords2: X = {coords2.X}, Y = {coords2.Y}");

        Coords1 readonlyCoords = new(1, 2);

        // không thể thay đổi member của readonly struct
        // readonlyCoords.X = 10;
    }
}
