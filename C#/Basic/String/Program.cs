﻿
/* string */

/*
* string là kiểu dữ liệu dùng để lưu trữ chuỗi ký tự.
* một string object là một chuỗi tập hợp chứa các các char objects.
* giá trị của string object là nội dung của tập hơp char objects và giá trị này là immutable (read-only)
*/

/* Khai báo và khởi tạo string */

// Khai báo
string message1;

// Khởi tạo với giá trị null
string? message2 = null;

// Khởi tạo với string literal
string oldPath = "c:\\Program Files\\Microsoft Visual Studio 8.0";

//Khởi tạo với verbatim string literal
string newPath = @"c:\Program Files\Microsoft Visual Studio 9.0";

// Khởi tạo từ String constructor (từ char*, char[], sbyte*)
char[] letters = { 'A', 'B', 'C' };
string alphabet = new string(letters);

// string objects là immutable và không thể thay đổi sau khi tạo.
// các method của string và C# operator làm thay đổi một string sẽ trả về một string object mới.
string s1 = "A string is more ";
string s2 = "than the sum of its chars.";

// Nối 2 string lại với nhau (concatenate) sẽ tạo ra một string object mới và được lưu lại ở s1
// và giải phóng tham chiếu đến object cũ.
s1 += s2;


/* string interpolation */
// Nó cho phép nhúng biểu thức vào giá trị chuỗi một cách tiện lợi và dễ đọc bằng cách sử dụng ký tự $ và {}
var jh = (firstName: "Jupiter", lastName: "Hammon", born: 1711, published: 1761);
Console.WriteLine($"{jh.firstName} {jh.lastName} was an African American poet born in {jh.born}.");

// Các phương thức của string

// concat: dùng đẻ nối hai hoặc nhiều chuỗi lại với nhau và trả về một chuỗi mới
string str1 = "Hello";
string str2 = "World";
string result = string.Concat(str1, " ", str2);

// ToUpper: dùng để chuyển chuỗi chữ thường thanh chuỗi chữ hoa
string uppercase = str1.ToUpper();

// ToLower: dùng để chuyển chuỗi thành chuỗi chữ thường
string lowercase = str1.ToLower();

// Equals: dùng để so sánh hai chuỗi với nhau
bool euqual = str1.Equals(str2);

// IndexOf: dùng để tìm vị trí đầu tiên của một chuỗi con trong chuỗi gốc
string myString = "Hello, World!";
int firstIndex = myString.IndexOf("o");

// LastIndexOf: dùng để tìm vị trí cuối cùng của chuỗi con
int lastIndex = myString.LastIndexOf("o");

// Split: dùng để tách chuỗi thành một mảng các chuỗi dựa trên ký tự ngăn cách
string fruitsStr = "apple,banana,cherry";
string[] fruits = fruitsStr.Split(',');

// Substring: dùng để trích xuất một phần của chuỗi dựa trên vị trí bắt đầu và độ dài
string substring = myString.Substring(7, 5);

// Trim: dùng để loại bỏ các ký tự trắng ở đầu và cuối string.
myString = "   Hello, World!   ";
string trimmedString = myString.Trim();


// Replace: dùng để thay thế tất cả các ký tự hoặc chuỗi ký tự con trong chuỗi bằng một giá trị khác
string replacedString = myString.Replace("Hello", "Hi");

// StartsWith: kiểm tra chuỗi có bắt đầu bằng một chuỗi con cụ thể không
bool startsWithHello = myString.StartsWith("Hello");

// EndsWith: kiểm tra xem chuỗi có kết thúc bằng chuỗi con cụ thể không
bool endsWithWorld = myString.EndsWith("World"); 

// Contains: kiểm tra chuỗi có chứa một chuỗi con cụ thể không
bool containsWorld = myString.Contains("World");

// Join: dùng để nối các chuỗi trong cùng một mảng hoặc một tập hợp thành một chuỗi duy nhất ngăn cách dựa trên ký tự tùy chọn.
string[] words = { "Hi", "nice", "to", "meet", "you" };
string ketQua = string.Join(" ", words);
