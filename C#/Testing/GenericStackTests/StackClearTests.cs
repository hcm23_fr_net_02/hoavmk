using Xunit;

namespace GenericStackUnitTests;

public class StackClearTests
{
    [Fact]
    public void StackClear_WhenStackHaveItems_ShouldClearAll()
    {
        var stack = new GenericStack.Stack<int>(5);

        stack.Push(2);
        stack.Push(5);

        stack.Clear();

        Assert.Equal(expected: 0, actual: stack.Count);
        Assert.True(stack.IsEmpty());
    }
}
