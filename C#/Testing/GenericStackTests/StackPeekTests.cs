using System;
using Xunit;

namespace GenericStackUnitTests;

public class StackPeekTests
{
    [Fact]
    public void StackPeek_WhenStackIsEmpty_ShouldThrowException()
    {
        var stack = new GenericStack.Stack<int>(5);

        Assert.Throws<InvalidOperationException>(() => stack.Peek());
    }

    [Fact]
    public void StackPeek_WhenStackHaveItems_ShouldReturnTopElementValue()
    {
        var value = 1;
        var stack = new GenericStack.Stack<int>(5);
        stack.Push(value);

        var result = stack.Peek();

        Assert.Equal(expected: value, actual: result);
        Assert.Equal(expected: 1, actual: stack.Count);
    }
}
