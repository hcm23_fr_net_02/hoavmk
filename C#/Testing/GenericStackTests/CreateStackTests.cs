using System;
using Xunit;

namespace GenericStackUnitTests;

public class CreateStackTests
{
    [Fact]
    public void CreateStack_WhenInputCapacityIsValid_ShouldCreate()
    {
        var capacity = 5;

        var stack = new GenericStack.Stack<int>(capacity);

        Assert.True(stack.IsEmpty());
        Assert.Equal(expected: 0, actual: stack.Count);
    }

    [Fact]
    public void CreateStack_WhenInputCapacityInvalid_ShouldThrowException()
    {
        var capacity = -1;

        Assert.Throws<ArgumentException>(() => new GenericStack.Stack<int>(capacity));
    }
}
