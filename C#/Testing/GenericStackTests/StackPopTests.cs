using System;
using Xunit;

namespace GenericStackUnitTests;

public class StackPopTests
{
    [Fact]
    public void StackPop_WhenStackIsEmpty_ShouldThrownException()
    {
        var stack = new GenericStack.Stack<int>(5);

        Assert.Throws<InvalidOperationException>(() => stack.Pop());
    }

    [Fact]
    public void StackPop_WhenStackHaveItems_ShouldReturnTopElement()
    {
        var value = 1;
        var stack = new GenericStack.Stack<int>(5);
        stack.Push(value);

        var result = stack.Pop();

        Assert.Equal(expected: value, actual: result);
        Assert.Equal(expected: 0, actual: stack.Count);
    }
}
