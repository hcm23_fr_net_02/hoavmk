using System;
using Xunit;

namespace GenericStackUnitTests;

public class StackPushTests
{
    [Fact]
    public void StackPush_WhenStackIsFull_ShouldThrowException()
    {
        var stack = new GenericStack.Stack<int>(1);
        stack.Push(1);
        

        Assert.Throws<InvalidOperationException>(() => stack.Push(1));
    }

    [Theory]
    [ClassData(typeof(StackEnoughCapacityData))]
    public void StackPush_WhenStackEnoughCapacity_ShouldPush(int capacity, string value)
    {
        var stack = new GenericStack.Stack<string>(capacity);

        stack.Push(value);

        Assert.Equal(expected: 1, actual: stack.Count);
        Assert.Equal(expected: value, actual: stack.Peek());
    }
}

public class StackEnoughCapacityData : TheoryData<int, string>
{
    public StackEnoughCapacityData()
    {
        Add(1, "hello");
        Add(1, "world");
    }
}

