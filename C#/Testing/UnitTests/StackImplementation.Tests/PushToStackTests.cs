using System;
using Xunit;
using StackImpl;

namespace StackImplementation.Tests;

// Unit test: kiểm thử từng đơn vị trong phần mềm (unit: function, method)
// Tạo class unit test sử dụng xUnit
// xUnit cung cấp 2 bộ test là Fact và Theory để thực hiện unit test
// Fact: bộ test đơn giản, đại diện cho một trường hợp test và có đầu vào cố định
// Theory: test nhiều trường hợp với các dữ liệu đầu vào khác nhau trên cùng một method test
// Với Theory, xUnit cung cấp 3 cách chính để truyền dữ liệu đầu vào:
// InlineData: truyền dữ liệu đầu vào trực tiếp vào các bộ test thông qua các thuộc tính
// MemberData: truyền dữ liệu đầu vào thông qua một thuộc tính của lớp test
// ClassData: truyền dữ liệu đầu vào từ một lớp khác vào các bộ test
public class PushToStackTests
{
    // Sử dụng Arrange, Act, Assert pattern
    // Arrange: khởi tạo object, thiết lập dữ liệu được truyền vào method được test
    // Act: thực hiện method được test với những thiết lập từ Arrange
    // Assert: xác định hành động của method được test hoạt động như mong đợi
    [Theory]
    [ClassData(typeof(PushToStackWhenEnoughCapacityData))]
    public void PushToStack_WhenEnoughCapacity_ShouldPush(int capacity, int value)
    {
        // arrange
        var stack = new Stack(capacity);

        // act
        stack.Push(value);

        // assert
        Assert.Equal(1, stack.Count);
        Assert.Equal(stack.Peek(), value);
    }

    [Fact]
    public void PushToStack_WhenCapacityIsFull_ShouldThrowException()
    {
        var stack = new Stack(0);

        Assert.Throws<InvalidOperationException>(() => stack.Push(1));
    }
}

public class PushToStackWhenEnoughCapacityData : TheoryData<int, int>
{
    public PushToStackWhenEnoughCapacityData()
    {
        Add(1, 1);
        Add(2, 1);
    }
}
