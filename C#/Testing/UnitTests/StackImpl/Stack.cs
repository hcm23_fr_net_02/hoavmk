namespace StackImpl;

public class Stack
{
    private int[] _array;
    private int _current = -1;
    private int _count = 0;
    private int _capacity;

    public Stack(int capacity)
    {
        _capacity = capacity;
        _array = new int[capacity];
    }

    public void Push(int element)
    {
        CheckEnoughCapacity();

        _array[++_current] = element;
        _count++;
    }

    public int Peek()
    {
        CheckEmptyStack();

        return _array[_current];
    }

    public int Pop()
    {
        CheckEmptyStack();

        var value = _array[_current];

        _current--;
        _count--;

        return value;
    }

    public int Count => _count;

    private void CheckEnoughCapacity()
    {
        if (_count >= _capacity)
        {
            throw new InvalidOperationException("Stack is full.");
        }
    }

    private void CheckEmptyStack()
    {
        if (_count == 0)
        {
            throw new InvalidOperationException("Stack is empty.");
        }
    }
}
