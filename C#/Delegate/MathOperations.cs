namespace Delegate101;

public class MathOperations
{
    public delegate int MathOperation(int a, int b);

    public int Add(int a, int b) => a + b;

    public int Subtract(int a, int b) => a - b;

    public int Multiply(int a, int b) => a * b;

    public int Divide(int a, int b) => a / b;

    public int PerformOperation(int a, int b, MathOperation operation)
        => operation(a, b);
}
