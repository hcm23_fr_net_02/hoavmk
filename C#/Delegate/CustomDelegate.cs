namespace Delegate101;

public class CustDelegate
{
    public delegate int Custom(int number);

    public int Double(int number)
        => number * 2;

    public int Triple(int number)
        => number * 3;

    public int Square(int number)
        => number * number;

    public int Cube(int number)
        => number * number * number;

    public void PrintResult(int number, Custom operation)
    {
        Console.WriteLine(operation(number));
    }
}
