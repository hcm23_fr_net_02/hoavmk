namespace Delegate101;

public class GenericDelegate
{
    public delegate T MathOperation<T>(T a, T b);

    public T PerformOperation<T>(T a, T b, Func<T, T, T> operation)
        => operation(a, b);
}
