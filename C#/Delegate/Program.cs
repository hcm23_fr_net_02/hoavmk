﻿using Delegate101;

Console.WriteLine("Math Operation Delegate");
var mathOperations = new MathOperations();

Console.WriteLine(mathOperations.PerformOperation(2, 3, mathOperations.Add));
Console.WriteLine(mathOperations.PerformOperation(3, 1, mathOperations.Subtract));
Console.WriteLine(mathOperations.PerformOperation(3, 5, mathOperations.Multiply));
Console.WriteLine(mathOperations.PerformOperation(3, 1, mathOperations.Divide));


Console.WriteLine();
Console.WriteLine("Custom Delegate");
var cust = new CustDelegate();

cust.PrintResult(2, cust.Double);
cust.PrintResult(2, cust.Triple);
cust.PrintResult(2, cust.Square);
cust.PrintResult(2, cust.Cube);

Console.WriteLine();
Console.WriteLine("Generic Delegate");

var genericDel = new GenericDelegate();

var point1 = new Point(1, 2);
var point2 = new Point(2, 4);
var animal1 = new Animal
{
    Age = 2,
    Weight = 20
};

var animal2 = new Animal
{
    Age = 1,
    Weight = 10
};

Func<int, int, int> add = (a, b) => a + b;
Func<double, double, double> subtract = (a, b) => a - b;
Func<string, string, string> concat = (a, b) => a + b;
Func<Point, Point, Point> addPoint = (a, b) => new Point(a.X + b.X, a.Y + b.Y);
Func<Animal, Animal, Animal> addStat = (a, b) => new Animal { Age = a.Age + b.Age, Weight = a.Weight + b.Weight };

Console.WriteLine(genericDel.PerformOperation(1, 2, add));
Console.WriteLine(genericDel.PerformOperation(1, 2, subtract));
Console.WriteLine(genericDel.PerformOperation("hello ", "world", concat));
Console.WriteLine(genericDel.PerformOperation(point1, point2, addPoint));
Console.WriteLine(genericDel.PerformOperation(animal1, animal2, addStat));


struct Point
{
    public int X;
    public int Y;

    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }
}

public class Animal
{
    public int Age { get; set; }

    public int Weight { get; set; }
}