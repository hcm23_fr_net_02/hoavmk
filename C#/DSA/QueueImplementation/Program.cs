﻿
// Triển khai queue theo circular queue
// Circular queue sử dụng mảng vòng tròn
// Khi phần tử cuối cùng của queue được thêm vào
// sẽ nói với phần tử đầu tiên của queue
public class Queue
{
    // Lưu các phần tử được thêm vào queue
    private int[] _array;

    // Lưu sức chứa của queue
    private int _capacity;

    // biến đung để theo dõi đầu của queue
    private int _front = 0;

    // biến dùng để theo dõi đuôi của queue,
    // khi rear đạt đến cuối mảng, sẽ được quay lại đầu của mảng
    private int _rear = -1;

    // biến dùng để đếm số lượng phần tử trong queue
    private int _count = 0;

    // Constructor tạo queue với sức chứa mong muốn (mặc định = 100)
    public Queue(int capacity = 100)
    {
        ValidateQueueCapacity(capacity);
        _capacity = capacity;
        _array = new int[capacity];
    }

    // Kiểm tra sức chứa của queue, sau đó thêm phẩn từ vào queue
    // và cập nhật lại rear, count
    public void Enqueue(int value)
    {
        CheckQueueCapacity();

        _rear = (_rear + 1) % _capacity;
        _array[_rear] = value;
        _count++;
    }

    // Kiểm tra queue rỗng, sau đó trả về phần tử ở đầu queue (phần từ cũ nhất)
    public int Peek()
    {
        CheckQueueEmpty();

        return _array[_front];
    }

    // Kiểm tra queue rỗng, sau đó lấy phần tử ở đầu ra khỏi queue
    // và cập nhật lại front và count
    public int Dequeue()
    {
        CheckQueueEmpty();

        int value = _array[_front];

        _front = (_front + 1) % _capacity;
        _count--;

        return value;
    }

    // Kiểm tra sức chứa của queue
    private void CheckQueueCapacity()
    {
        if (_count == _capacity)
        {
            throw new InvalidOperationException("Queue is full.");
        }
    }

    // Kiểm tra queue rỗng
    private void CheckQueueEmpty()
    {
        if (_count == 0)
        {
            throw new InvalidOperationException("Queue is empty.");
        }
    }

    private void ValidateQueueCapacity(int capacity)
    {
        if (capacity < 1)
            throw new ArgumentException("Queue capacity must be larger than 0.");
    }
}

