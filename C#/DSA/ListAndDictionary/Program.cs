﻿
using ListAndDictionary;

var app = new App();

while (true)
{
    PrintMenu();

    string? choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            app.AddStudent();
            break;
        case "2":
            app.FindStudentGrade();
            break;
        case "3":
            app.ShowStudents();
            break;
        case "4":
            return;
        default:
            Console.WriteLine("Invalid option!");
            break;
    }
}

void PrintMenu()
{
    Console.WriteLine("-------------------------------Student management-------------------------------");
    Console.WriteLine("1. Add new student");
    Console.WriteLine("2. Find student by name");
    Console.WriteLine("3. Show all students");
    Console.WriteLine("4. Exit");
    Console.Write("Please select an option: ");
}
