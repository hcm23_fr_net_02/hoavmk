namespace ListAndDictionary;

public class App
{
    private readonly Dictionary<string, double> _dict = new();

    public void AddStudent()
    {
        Console.Write("Enter student name: ");
        string? name = Console.ReadLine();

        if (string.IsNullOrEmpty(name))
        {
            Console.WriteLine("Invalid name!");

            return;
        }

        Console.Write("Enter grade: ");

        if (double.TryParse(Console.ReadLine(), out var grade))
        {
            if (!_dict.ContainsKey(name))
            {
                _dict.Add(name, grade);
            }
            else
            {
                _dict[name] = grade;
            }

            return;
        }

        Console.WriteLine("Invalid grade!");
    }

    public void FindStudentGrade()
    {
        Console.Write("Enter student name: ");
        string? name = Console.ReadLine();

        if (string.IsNullOrEmpty(name))
        {
            Console.WriteLine("Invalid name!");

            return;
        }

        if (!_dict.ContainsKey(name))
        {
            Console.WriteLine($"Student with name = {name} was not found.");

            return;
        }

        Console.WriteLine($"Student: {name}, grade: {_dict[name]}");
    }

    public void ShowStudents()
    {
        Console.WriteLine("-------------------------------Students-------------------------------");

        foreach (var pair in _dict)
        {
            Console.WriteLine($"Student: {pair.Key}, grade: {pair.Value}");
        }
    }
}
