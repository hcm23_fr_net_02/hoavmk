﻿
var stack = new Stack(10);


stack.Push(5);
stack.Push(8);
stack.Push(9);

stack.Push(10);

stack.Pop();
stack.Pop();
stack.Pop();
stack.Pop();
stack.Peek();

Console.WriteLine(stack.Count);

public class Stack
{
    // array dùng để lưu các phần tử được thêm vào stack
    private int[] _array;

    // biến dùng để theo dõi đỉnh stack
    private int _current = -1;

    // biến dùng để đếm số lượng phần tử trong stack
    private int _count = 0;

    // lưu sức chứa của stack
    private int _capacity;

    
    // Constructor tạo stack với sức chứa mong muốn
    public Stack(int capacity)
    {
        ValidateStackCapacity(capacity);
        _capacity = capacity;
        _array = new int[capacity];
    }

    
    // Kiểm tra stack còn đủ sức chứa, sau đó thêm phần tử vào stack dựa trên biến current
    // và tăng kích thước của stack, current lên 1
    public void Push(int element)
    {
        CheckEnoughCapacity();

        _array[++_current] = element;
        _count++;
    }

    // Kiểm tra stack rỗng và trả về giá trị phần tử ở đỉnh stack dựa trên biến current
    public int Peek()
    {
        CheckEmptyStack();

        return _array[_current];
    }

    // Kiểm tra stack rỗng và lấy phần tử ở đỉnh stack ra khỏi stack dựa trên biến current
    // và sau đó cập nhật lại current và count
    public int Pop()
    {
        CheckEmptyStack();

        var value = _array[_current];

        _current--;
        _count--;

        return value;
    }

    // Trả về số lượng phần tử chứa trong stack
    public int Count => _count;

    // Kiểm tra stack rỗng
    public bool IsEmpty()
    {
        return _count == 0;
    }

    public void Clear()
    {
        Array.Clear(_array);
        _count = 0;
        _current = -1;
    }

    // Kiểm tra sức chứa của stack
    private void CheckEnoughCapacity()
    {
        if (_count >= _capacity)
            throw new InvalidOperationException("Stack is full.");
    }

    // Kiểm tra stack rỗng
    private void CheckEmptyStack()
    {
        if (_count == 0)
            throw new InvalidOperationException("Stack is empty.");
    }

    // Kiểm tra sức chứa của stack
    private void ValidateStackCapacity(int capacity)
    {
        if (capacity < 1)
            throw new ArgumentException("Stack capacity must be larger than 0.");
    }
}

