namespace AggregateExercise;

public class Order
{
    public int Id { get; set; }

    public string CustomerName { get; set; } = null!;

    public decimal TotalAmount { get; set; }

    public override string ToString()
    {
        return $"Id = {Id} | CustomerName = {CustomerName} | TotalAmount = {TotalAmount}";
    }
}
