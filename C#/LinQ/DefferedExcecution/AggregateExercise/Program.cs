﻿using System.Text.Json;
using AggregateExercise;

var fileName = "MOCK_DATA.json";

List<Order>? orders;

try
{
    using FileStream openStream = File.OpenRead(fileName);

    var serializerOption = new JsonSerializerOptions
    {
        PropertyNameCaseInsensitive = true
    };

    orders = await JsonSerializer.DeserializeAsync<List<Order>>(openStream, serializerOption);
}
catch (Exception e)
{
    Console.WriteLine(e.Message);

    return;
}

if (orders is null)
{
    Console.WriteLine("Something went wrong when deserializing data.");

    return;
}

// Lấy ra tổng số đơn đặt hàng có trong danh sách

var totalOrders = orders.Count();

Console.WriteLine($"Total orders: {totalOrders}");

// 1. Tính tổng số tiền của tất cả các đơn đặt hàng.

var totalAmount = orders.Sum(o => o.TotalAmount);

Console.WriteLine($"1. Total amount: {totalAmount}");

// 2. Lấy ra đơn hàng có giá trị (TotalAmount) cao nhất.

var highestOrder = orders.Aggregate((current, next) =>
    current.TotalAmount > next.TotalAmount ? current : next);

Console.Write("2. The order with the highest total amount: ");
Console.WriteLine(highestOrder.ToString());

// 3. Lấy ra đơn hàng có giá trị thấp nhất.

var lowestOrder = orders.Aggregate((current, next) =>
    current.TotalAmount < next.TotalAmount ? current : next);

Console.Write("3. The order with the lowest total amount: ");
Console.WriteLine(lowestOrder.ToString());

// 4. Tính giá trị trung bình của các đơn hàng.

var averageTotalAmount = orders.Average(o => o.TotalAmount);

Console.WriteLine($"4. Average total amount: {averageTotalAmount}");

// 5. Tính tổng số đơn đặt hàng của một khách hàng cụ thể.

var customerName = "Christian Burder";

var numOrders = orders.Where(o => o.CustomerName == customerName)
                                        .Count();

Console.WriteLine($"5. Order times of {customerName}: {numOrders}");

// 6. Tính tổng số tiền của tất cả các đơn đặt hàng của một khách hàng cụ thể.

var totalSpending = orders.Where(o => o.CustomerName == customerName)
                          .Sum(o => o.TotalAmount);

Console.WriteLine($"6. Total spending of {customerName}: {totalSpending}");

// 7. Lấy ra đơn hàng có giá trị cao nhất của một khách hàng cụ thể.

var highestOrderPriceOfACustomer = orders.Where(o => o.CustomerName == customerName)
    .Aggregate((current, next) =>
        current.TotalAmount >
            next.TotalAmount ? current : next);

Console.WriteLine($"7. {customerName}'s highest order: {highestOrderPriceOfACustomer}");

// 8. Lấy ra đơn hàng có giá trị thấp nhất cho một khách hàng cụ thể.

var lowestOrderPriceOfACustomer = orders.Where(o => o.CustomerName == customerName)
    .Aggregate((current, next) =>
        current.TotalAmount <
            next.TotalAmount ? current : next);

Console.WriteLine($"8. {customerName}'s lowest order: {lowestOrderPriceOfACustomer}");

// 9. Tính giá trị trung bình các đơn đặt hàng của một khách hàng cụ thể

var averageTotalAmountOfACustomer = orders.Where(o => o.CustomerName == customerName)
                                          .Average(o => o.TotalAmount);

Console.WriteLine($"9. {customerName}'s average total amount: {averageTotalAmountOfACustomer}");
