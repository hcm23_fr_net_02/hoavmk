﻿
var numbers = new List<int> { 3, 8, 1, 5, 2, 10, 7 };

// a. Lấy ra các số chẵn và sắp xếp tăng dần kq

var sortedEvenNumbers = numbers.Where(n => n % 2 == 0)
    .OrderBy(n => n);

Console.Write("Even numbers order by ascending: ");
foreach (var number in sortedEvenNumbers)
{
    Console.Write($"{number} ");
}

Console.WriteLine();

// b. Lấy ra các số lẻ và sắp xếp và sắp xếp giảm dần kq

var sortedOddNumbers = numbers.Where(n => n % 2 != 0)
    .OrderByDescending(n => n);

Console.Write("Odd numbers order by descending: ");
foreach (var number in sortedOddNumbers)
{
    Console.Write($"{number} ");
}

Console.WriteLine();

// c. Lấy ra các số lớn hơn 5 và nhỏ hơn 10 và sắp xếp tăng dần kq

var selectedNumbers = numbers.Where(n => n > 5 && n < 10)
    .OrderBy(n => n);

Console.Write("Numbers > 5 and < 10, order by ascending: ");
foreach (var number in selectedNumbers)
{
    Console.Write($"{number} ");
}
