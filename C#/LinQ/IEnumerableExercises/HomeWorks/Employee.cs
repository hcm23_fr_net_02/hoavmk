namespace HomeWorks;

public class Employee
{
    public int Id { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public double Salary { get; set; }

    public string Gender { get; set; } = null!;

    public string? Email { get; set; }

    public DateTime? StartDate { get; set; }

    public string? PostalCode { get; set; }

    public string Country { get; set; } = null!;

    public string? JobTitle { get; set; }

    public override string ToString()
    {
        return $"{FirstName} {LastName} | Email: {Email} | StartDate: {StartDate} | JobTitle = {JobTitle} | Salary: {Salary}"
            + $" | Gender: {Gender} | Country: {Country} | PostalCode: {PostalCode}";
    }
}
