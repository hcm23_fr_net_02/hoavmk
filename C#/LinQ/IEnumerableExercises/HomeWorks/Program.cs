﻿using System.Text.Json;
using HomeWorks;

// đọc file mock-data.json trong project để lấy dữ liệu

var fileName = "MOCK_DATA.json";

List<Employee>? employees;

try
{
    using FileStream openStream = File.OpenRead(fileName);

    var serializerOptions = new JsonSerializerOptions
    {
        PropertyNameCaseInsensitive = true,
        
    };

    serializerOptions.Converters.Add(new DateTimeJsonConverter());

    employees = await JsonSerializer.DeserializeAsync<List<Employee>>(openStream, serializerOptions);
}
catch (Exception e)
{
    Console.WriteLine(e.Message);
    return;
}


if (employees is null)
{
    Console.WriteLine("Something went wrong when deserialize data.");

    return;
}

// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.

var employeeFirstNames = employees.Select(e => e.FirstName);

Console.WriteLine("First name list:");
foreach (var firstName in employeeFirstNames)
{
    Console.WriteLine(firstName);
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.

var highSalaryEmployees = employees.Where(e => e.Salary > 50000);

Console.WriteLine("Employee with salary > 50000");

foreach (var employee in employees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.

var maleEmployees = employees.Where(e => e.Gender == "Male")
    .OrderBy(e => e.FirstName);

Console.WriteLine("Employees with gender = \"Male\" order by FirstName");

foreach (var employee in maleEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".

var indonesianManagers = employees.Where(e =>
    e.Country == "Indonesia" &&
    e.JobTitle == "Manager");

Console.WriteLine("Indonesian managers: ");

foreach (var employee in indonesianManagers)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.

var employeesHaveEmail = employees.Where(e => e.Email != null)
    .OrderByDescending(e => e.LastName);

Console.WriteLine("Employees that have email address: ");

foreach (var employee in employeesHaveEmail)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.

var markDate = new DateTime(2022, 1, 1);
var highSalarySeniorEmployees = employees.Where(e => e.StartDate < markDate && e.Salary > 60000);

Console.WriteLine("Employees that start working before 2022-01-01 and have salary > 60000: ");

foreach (var employee in highSalarySeniorEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.

var noPostalCodeEmployees = employees.Where(e => string.IsNullOrEmpty(e.PostalCode))
    .OrderBy(e => e.LastName);

Console.WriteLine("Employees that have no PostalCode: ");

foreach (var employee in noPostalCodeEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();


//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.

var uppercaseFullNameEmployees = employees.Where(e =>
    e.FirstName == e.FirstName.ToUpper() &&
    e.LastName == e.LastName.ToUpper())
    .OrderByDescending(e => e.Id);

Console.WriteLine("Employees that have uppercase FirstName and LastName: ");

foreach (var employee in uppercaseFullNameEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.

var rangedSalaryEmployees = employees.Where(e =>
    e.Salary >= 50000 &&
    e.Salary <= 70000)
    .OrderBy(e => e.Salary);

Console.WriteLine("Employees that have salary between 50000 and 70000: ");

foreach (var employee in rangedSalaryEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();

//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.

var aAEmployees = employees.Where(e =>
    e.FirstName.Contains('a') ||
    e.FirstName.Contains('A'))
    .OrderByDescending(e => e.LastName);

Console.WriteLine("Employees that contain 'a' or 'A' in FirstName: ");

foreach (var employee in aAEmployees)
{
    Console.WriteLine(employee.ToString());
}

Console.WriteLine();
