﻿
var products = new List<Product>
{
    new() { Id = 1, Name = "Laptop", Price = 1000 },
    new() { Id = 2, Name = "Phone", Price = 500 },
    new() { Id = 3, Name = "Headphones", Price = 50 },
    new() { Id = 4, Name = "Mouse", Price = 20 },
    new() { Id = 5, Name = "Keyboard", Price = 30 }
};

// Yêu cầu 1: Lấy ra "tên" các sản phẩm có giá trị lớn hơn 100, sắp xếp giảm dần theo giá.

var sortedProducts = products.Where(p => p.Price > 100)
    .OrderByDescending(p => p.Price);

Console.WriteLine("Products having price larger than 100, order by price descending:");
foreach (var product in sortedProducts)
{
    Console.WriteLine($"Product {product.Id}: {product.Name} - ${product.Price}");
}

Console.WriteLine();

// Yêu cầu 2: Lấy ra thông tin các sản phẩm có tên chứa từ "o", sắp xếp tăng dần theo tên.

var productsContainOCharacter = products.Where(p => p.Name.Contains('o'))
    .OrderBy(p => p.Name);

Console.WriteLine("Products containing 'o' character in their name, order by name ascending:");
foreach (var product in productsContainOCharacter)
{
    Console.WriteLine($"Product {product.Id}: {product.Name} - ${product.Price}"); 
}


class Product
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public decimal Price { get; set; }
}
