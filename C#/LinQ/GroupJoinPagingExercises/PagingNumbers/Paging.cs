namespace PagingNumbers;

public class Paging<T>
{
    private readonly List<T> _items;
    private int _pageSize = 10;
    private int _page = 1;
    private int _totalPage;

    public bool HasNext => _page < _totalPage;

    public bool HasPrevious => _page > 1;

    public int TotalItems => _items.Count;

    public int Page => _page;

    public int TotalPage => _totalPage;

    public Paging(List<T> items)
    {
        _items = items;
        _totalPage = CalculateTotalPages(_pageSize);
    }

    public void UpdatePageSize(int pageSize)
    {
        _totalPage = CalculateTotalPages(pageSize);
        _pageSize = pageSize;
        _page = 1;
    }

    public List<T> GetCurrentPage()
    {
        return _items.Skip((_page - 1) * _pageSize)
                       .Take(_pageSize)
                       .ToList();
    }

    public void NextPage()
    {
        if (_page < _totalPage)
            _page++;
    }

    public void PreviousPage()
    {
        if (_page > 1)
            _page--;
    }

    private int CalculateTotalPages(int pageSize)
    {
        var numPages = _items.Count / pageSize;

        return _items.Count % pageSize == 0 ? numPages : numPages + 1;
    }
}