namespace PagingNumbers;

public class App
{
    private readonly Paging<int> _paging;

    public App()
    {
        _paging = new(Enumerable.Range(0, 50).ToList());
    }

    public void UpdatePageSize()
    {
        Console.Write("Enter page size: ");

        if (int.TryParse(Console.ReadLine(), out var pageSize))
        {
            if (pageSize > 0 && pageSize <= _paging.TotalItems)
            {
                _paging.UpdatePageSize(pageSize);
                ShowCurrentPage();

                return;
            }
        }

        Console.WriteLine("Invalid page size!");
    }

    public void NextPage()
    {
        _paging.NextPage();
        ShowCurrentPage();
    }

    public void PreviousPage()
    {
        _paging.PreviousPage();
        ShowCurrentPage();
    }

    public void ShowCurrentPage()
    {
        Console.Write($"Page {_paging.Page}: ");

        foreach (var number in _paging.GetCurrentPage())
        {
            Console.Write($"{number} ");
        }

        Console.WriteLine();
    }

    public void PrintMenu()
    {
        Console.WriteLine("------------------------------PagingNumbers------------------------------");
        Console.WriteLine("1. Page size");

        if (_paging.HasPrevious)
        {
            Console.WriteLine("2. Previous");
        }

        if (_paging.HasNext)
        {
            Console.WriteLine("3. Next");
        }

        Console.WriteLine("4. Exit");
        Console.Write("Select an option: ");
    }
}
