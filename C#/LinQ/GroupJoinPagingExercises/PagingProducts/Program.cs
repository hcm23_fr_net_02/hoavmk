﻿using PagingProducts;

try
{
    var fileName = "MOCK_DATA.json";
    var app = await App.CreateAsync(fileName);

    Console.WriteLine("------------------------------PagingProducts------------------------------");
    app.ShowCurrentPage();

    while (true)
    {
        app.PrintMenu();

        string? choice = Console.ReadLine();

        switch (choice)
        {
            case "1":
                app.UpdatePageSize();
                break;
            case "2":
                app.PreviousPage();
                break;
            case "3":
                app.NextPage();
                break;
            case "4":
                return;
            default:
                Console.WriteLine("Invalid option, please select again!");
                break;
        }
    }
}
catch (Exception e)
{
    Console.WriteLine(e.Message);
}
