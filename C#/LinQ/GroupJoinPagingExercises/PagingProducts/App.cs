using System.Text.Json;

namespace PagingProducts;

public class App
{
    private Paging<Product> _paging;

    private App(Paging<Product> paging)
    {
        _paging = paging;
    }

    public static async Task<App> CreateAsync(string fileName)
    {
        var products = await LoadDataAsync(fileName) ??
            throw new Exception("Can not load data.");
        
        return new App(new Paging<Product>(products));
    }

    private static async Task<List<Product>?> LoadDataAsync(string fileName)
    {
        using FileStream openStream = File.OpenRead(fileName);

        var serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };

        return await JsonSerializer.DeserializeAsync<List<Product>>(openStream, serializerOptions);
    }


   public void UpdatePageSize()
    {
        Console.Write("Enter page size: ");

        if (int.TryParse(Console.ReadLine(), out var pageSize))
        {
            if (pageSize > 0 && pageSize <= _paging.TotalItems)
            {
                _paging.UpdatePageSize(pageSize);
                ShowCurrentPage();

                return;
            }
        }

        Console.WriteLine("Invalid page size!");
    }

    public void NextPage()
    {
        _paging.NextPage();
        ShowCurrentPage();
    }

    public void PreviousPage()
    {
        _paging.PreviousPage();
        ShowCurrentPage();
    }

    public void ShowCurrentPage()
    {
        Console.WriteLine($"Page {_paging.Page}:");

        foreach (var item in _paging.GetCurrentPage())
        {
            Console.WriteLine($"{item}");
        }

        Console.WriteLine();
    }

    public void PrintMenu()
    {
        Console.WriteLine("------------------------------PagingProducts------------------------------");
        Console.WriteLine("1. Page size");

        if (_paging.HasPrevious)
        {
            Console.WriteLine("2. Previous");
        }

        if (_paging.HasNext)
        {
            Console.WriteLine("3. Next");
        }

        Console.WriteLine("4. Exit");
        Console.Write("Select an option: ");
    }
}
