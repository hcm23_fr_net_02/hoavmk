﻿// Đọc dữ liệu từ file JSON
using Newtonsoft.Json;

var productsJson = File.ReadAllText("products.json");
var categoriesJson = File.ReadAllText("categories.json");

// Chuyển đổi JSON thành danh sách các đối tượng
var products = JsonConvert.DeserializeObject<List<Product>>(productsJson);
var categories = JsonConvert.DeserializeObject<List<Category>>(categoriesJson);

if (products is null)
    return;

if (categories is null)
    return;

//Lấy danh sách tên sản phẩm và giá cho các sản phẩm có giá trị lớn hơn $100, sắp xếp theo giá giảm dần.

var productNames = products.Where(p => p.Price > 100)
    .OrderByDescending(p => p.Price)
    .Select(p => p.Name);

//Lấy danh sách tất cả các danh mục và số lượng sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

// method syntax
// Thực hiện left outer join bằng GroupJoin với DefaultIfEmpty,
// flatten group thành một sequence sử dụng select many
// group lại sequence theo category
// sắp xếp và projection
var categoryWithProductNumbers = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .OrderBy(g => g.Key.Name)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        NumProducts = g.Count()
    });


// query syntax
var categoryWithProductsQuery = from category in categories
                                join product in products
                                on category.Id equals product.CategoryId into CategoryProductGroup
                                from productCate in CategoryProductGroup.DefaultIfEmpty()
                                select new
                                {
                                    Category = category,
                                    Product = productCate
                                };

var categoryWithProductsGroupQuery = from categoryProduct in categoryWithProductsQuery
                                     group categoryProduct by categoryProduct.Category into categoryGroup
                                     orderby categoryGroup.Key.Name
                                     select categoryGroup;

var categoryWithProductNumbersQuery = from cps in categoryWithProductsGroupQuery
                                      select new
                                      {
                                        CategoryName = cps.Key.Name,
                                        NumProducts = cps.Count()
                                      };

Console.WriteLine("Categories with their number of products:");

foreach (var category in categoryWithProductNumbers)
{
    Console.WriteLine($"- {category.CategoryName}: {category.NumProducts}");
}

Console.WriteLine("Categories with their number of products (query syntax):");

foreach (var category in categoryWithProductNumbersQuery)
{
    Console.WriteLine($"- {category.CategoryName}: {category.NumProducts}");
}

//Lấy giá trung bình của các sản phẩm cho mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

var categoryWithAveragePrices = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .OrderBy(g => g.Key.Name)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        AveragePrice = g.Average(x => x.Product!.Price)
    });

Console.WriteLine("Categories with their product average price:");

foreach (var category in categoryWithAveragePrices)
{
    Console.WriteLine($"- {category.CategoryName}: {category.AveragePrice}");
}

//Lấy danh sách tên sản phẩm và giá cho 10 sản phẩm đắt nhất, sắp xếp theo giá giảm dần.

var top10ExpensiveProducts = products.OrderByDescending(p => p.Price)
    .Take(10)
    .Select(p => new
    {
        Name = p.Name,
        Price = p.Price
    });

Console.WriteLine("Top 10 expensive products:");

foreach (var product in top10ExpensiveProducts)
{
    Console.WriteLine($"- {product.Name} | ${product.Price}");
}

//Lấy danh sách tất cả các danh mục và giá trung bình của các sản phẩm trong mỗi danh mục, sắp xếp theo giá trung bình giảm dần.

var categoryWithOrderedAveragePrices = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        AveragePrice = g.Average(x => x.Product!.Price)
    })
    .OrderBy(g => g.AveragePrice);

Console.WriteLine("Categories with their product average price (order by average price descending):");

foreach (var category in categoryWithOrderedAveragePrices)
{
    Console.WriteLine($"- {category.CategoryName}: {category.AveragePrice}");
}

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có giá trị nhỏ hơn $50, sắp xếp theo tên danh mục tăng dần.

var cheapProducts = products.Where(p => p.Price < 50)
    .Join(
        categories,
        product => product.CategoryId,
        category => category.Id,
        (product, category) => new
        {
            ProductName = product.Name,
            CategoryName = category.Name
        })
    .OrderBy(p => p.CategoryName);

Console.WriteLine("Products that price < 50: ");

foreach (var product in cheapProducts)
{
    Console.WriteLine($"- {product.ProductName} | {product.CategoryName}");
}

//Tính tổng giá của tất cả sản phẩm trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

var categoryWithSumPrice = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        TotalPrice = g.Sum(x => x.Product!.Price)
    })
    .OrderBy(g => g.CategoryName);

Console.WriteLine("Categories with their total product price:");

foreach (var category in categoryWithSumPrice)
{
    Console.WriteLine($"- {category.CategoryName}: {category.TotalPrice}");
}

//Lấy danh sách tên sản phẩm và danh mục cho các sản phẩm có tên chứa từ "Apple", sắp xếp theo tên sản phẩm tăng dần.

var productsContainApple = products.Where(p => p.Name.Contains("Apple"))
    .Join(
        categories,
        product => product.CategoryId,
        category => category.Id,
        (product, category) => new
        {
            ProductName = product.Name,
            CategoryName = category.Name
        })
    .OrderBy(p => p.ProductName);

Console.WriteLine("Products contain \"Apple\" in their name: ");

foreach (var product in productsContainApple)
{
    Console.WriteLine($"- {product.ProductName} | {product.CategoryName}");
}

//Lấy danh sách tất cả các danh mục và tổng giá của các sản phẩm trong mỗi danh mục, chỉ lấy những danh mục có tổng giá trị lớn hơn $1000.

var categoryWithHighSumPrice = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        TotalPrice = g.Sum(x => x.Product!.Price)
    })
    .Where(c => c.TotalPrice > 1000);

Console.WriteLine("Categories with their total product price > 1000:");

foreach (var category in categoryWithHighSumPrice)
{
    Console.WriteLine($"- {category.CategoryName}: {category.TotalPrice}");
}

//Kiểm tra xem có danh mục nào có sản phẩm có giá trị nhỏ hơn $10 không, nếu có, lấy danh sách tên của những danh mục đó.

var productsWithPriceLT10 = products.Where(p => p.Price < 10);

var categoriesHaveProductsWithPriceLT10 = categories.Join(
    productsWithPriceLT10,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        CategoryId = category.Id,
        CategoryName = category.Name
    })
    .DistinctBy(category => category.CategoryId);

Console.WriteLine("Categories that have product price < 10:");

foreach (var category in categoriesHaveProductsWithPriceLT10)
{
    Console.WriteLine($"- {category.CategoryId} - {category.CategoryName}");
}

//Lấy danh sách các sản phẩm có giá trị lớn nhất trong mỗi danh mục, sắp xếp theo tên danh mục tăng dần.

var categoriesWithHighestPrice = products.GroupBy(p => p.CategoryId)
    .Select(
        g => new
        {
            CategoryId = g.Key,
            HighestPrice = g.Max(p => p.Price)
        })
    .Join(
        products,
        category => category.CategoryId,
        product => product.CategoryId,
        (category, product) => new
        {
            CategoryId = category.CategoryId,
            HighestPrice = category.HighestPrice,
            Price = product.Price,
            ProductName = product.Name    
        });

var categoriesWithHighestProductPrice = categories.GroupJoin(
    categoriesWithHighestPrice,
    category => category.Id,
    categoryPrice => categoryPrice.CategoryId,
    (category, categoryPrice) => new
    {
        Category = category,
        Product = categoryPrice.DefaultIfEmpty()
    })
    .SelectMany(
        group => group.Product,
        (categoryProduct, product) => new
        {
            Category = categoryProduct.Category,
            Product = product
        })
    .Where(cp => cp.Product!.HighestPrice == cp.Product.Price)
    .GroupBy(cp => new { CategoryId = cp.Category.Id, CategoryName = cp.Category.Name });

Console.WriteLine("Categories with their highest product price:");

foreach (var group in categoriesWithHighestProductPrice)
{
    Console.WriteLine($"{group.Key.CategoryId}. {group.Key.CategoryName}:");

    foreach (var item in group)
    {
        Console.WriteLine($" - {item.Product!.ProductName}");
    }
}

//Lấy danh sách tất cả các danh mục và tổng số tiền của các sản phẩm trong mỗi danh mục, sắp xếp theo tổng số tiền giảm dần.

var categoryWithOrderedSumPrice = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        TotalPrice = g.Sum(x => x.Product!.Price)
    })
    .OrderByDescending(g => g.TotalPrice);

Console.WriteLine("Categories with their total product price (order by total product price descending):");

foreach (var category in categoryWithOrderedSumPrice)
{
    Console.WriteLine($"- {category.CategoryName} - {category.TotalPrice}");
}

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị lớn hơn giá trị trung bình của tất cả các sản phẩm.

var categoryIdsWithAveragePrice = products.GroupBy(p => p.CategoryId)
    .Select(g => new
    {
        CategoryId = g.Key,
        AveragePrice = g.Average(p => p.Price)
    });

var categoriesWithAveragePrice = categoryIdsWithAveragePrice.Join(
    categories,
    cate1 => cate1.CategoryId,
    cate2 => cate2.Id,
    (cate1, cate2) => new
    {
        CategoryId = cate1.CategoryId,
        CategoryName = cate2.Name,
        AveragePrice = cate1.AveragePrice
    });

var productsLGTCategoryAveragePrice = products.Join(
    categoriesWithAveragePrice,
    product => product.CategoryId,
    category => category.CategoryId,
    (product, category) => new
    {
        ProductName = product.Name,
        CategoryName = category.CategoryName,
        ProductPrice = product.Price,
        AveragePrice = category.AveragePrice
    })
    .Where(pc => pc.ProductPrice > pc.AveragePrice);

Console.WriteLine("Products that price > average category price:");

foreach (var product in productsLGTCategoryAveragePrice)
{
    Console.WriteLine($"- {product.ProductName} | {product.ProductPrice} | {product.AveragePrice}");
}

//Tính tổng số tiền của tất cả các sản phẩm.

var totalProductsPrice = products.Sum(p => p.Price);

Console.WriteLine("Total price of all products: " + totalProductsPrice);

//Lấy danh sách các danh mục và số lượng sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo số lượng sản phẩm giảm dần.

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị giảm dần.

var categoriesWithLowestPrice = products.GroupBy(p => p.CategoryId)
    .Select(
        g => new
        {
            CategoryId = g.Key,
            LowestPrice = g.Min(p => p.Price)
        })
    .Join(
        products,
        category => category.CategoryId,
        product => product.CategoryId,
        (category, product) => new
        {
            CategoryId = category.CategoryId,
            LowestPrice = category.LowestPrice,
            Price = product.Price,
            ProductName = product.Name    
        });

var categoriesWithLowestProductPrice = categories.GroupJoin(
    categoriesWithLowestPrice,
    category => category.Id,
    categoryPrice => categoryPrice.CategoryId,
    (category, categoryPrice) => new
    {
        Category = category,
        Product = categoryPrice.DefaultIfEmpty()
    })
    .SelectMany(
        group => group.Product,
        (categoryProduct, product) => new
        {
            Category = categoryProduct.Category,
            Product = product
        })
    .Where(cp => cp.Product!.LowestPrice == cp.Product.Price)
    .GroupBy(cp => new
    {
        CategoryId = cp.Category.Id,
        CategoryName = cp.Category.Name,
        LowestPrice = cp.Product!.LowestPrice
    })
    .OrderByDescending(g => g.Key.LowestPrice);

Console.WriteLine($"Products that have lowest price in their category:");

foreach (var group in categoriesWithLowestProductPrice)
{
    foreach (var item in group)
    {
        Console.WriteLine($"- {item.Product!.ProductName} | {group.Key.CategoryName}");
    }
}

//Lấy danh sách các danh mục có ít sản phẩm nhất.

var top5CategoriesWithLowestProducts = categories.GroupJoin(
    products,
    category => category.Id,
    product => product.CategoryId,
    (category, product) => new
    {
        Category = category,
        Product = product.DefaultIfEmpty()
    })
    .SelectMany(
    pc => pc.Product,
    (categoryProduct, product) => new
    {
        categoryProduct.Category,
        Product = product
    })
    .GroupBy(pc => pc.Category)
    .OrderBy(g => g.Count())
    .Select(g => new
    {
        CategoryName = g.Key.Name,
        NumProducts = g.Count()
    })
    .Take(5);

Console.WriteLine($"Top 5 categories have lowest products:");

foreach (var category in top5CategoriesWithLowestProducts)
{
    Console.WriteLine($"- {category.CategoryName}: {category.NumProducts}");
}

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị cao nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.

//Tính tổng số tiền của các sản phẩm có giá trị lớn hơn $50.

var totalPriceProductLGT50 = products.Where(p => p.Price > 50)
    .Sum(p => p.Price);

Console.WriteLine($"Total price of all products that price > 50: {totalPriceProductLGT50}");

//Lấy danh sách tên sản phẩm và danh mục của các sản phẩm có giá trị nhỏ nhất trong mỗi danh mục, sắp xếp theo giá trị tăng dần.


class Product
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int CategoryId { get; set; }

    public decimal Price { get; set; }
}

class Category
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;
}
