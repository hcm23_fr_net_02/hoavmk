﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:

var expensiveProducts = products.Where(p => p.Price > 500000);

Console.WriteLine("Products with price > 500000: ");

foreach (var product in expensiveProducts)
{
    Console.WriteLine($"Name: {product.Name} | Price: {product.Price} | Category: {product.Category}");
}

// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:

var cheapProducts = products.Where(p => p.Price < 300000)
                            .OrderByDescending(p => p.Price)
                            .Select(p => p.Name);

Console.WriteLine("Products with price < 300000 (order by price descending): ");

foreach (var product in cheapProducts)
{
    Console.WriteLine(product);
}

// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:

var groupProducts = products.GroupBy(p => p.Category)
                            .ToDictionary(p => p.Key);

Console.WriteLine("Product dictionary with category key:");

foreach (var category in groupProducts)
{
    Console.WriteLine($"{category.Key}:");

    foreach (var product in category.Value)
    {
        Console.WriteLine($" - Name: {product.Name} | Price: {product.Price}");
    }
}

// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":


var totalShirtCosts = products.Where(p => p.Category == "Áo")
    .Sum(p => p.Price);

Console.WriteLine("Total shirts price: " + totalShirtCosts);

// Tìm sản phẩm có giá cao nhất:

var highestPrice = products.Max(p => p.Price);
var highestPriceProduct = products.Where(p => p.Price == highestPrice)
                                  .First();

Console.WriteLine($"Product with highest price: {highestPriceProduct.Name} - {highestPriceProduct.Price}");

// Tạo danh sách các sản phẩm với tên đã viết hoa:

var uppercaseNameProducts = products.Select(p =>
                                    new Product
                                    {
                                        Name = p.Name.ToUpper(),
                                        Price = p.Price,
                                        Category = p.Category
                                    })
                                    .ToList();

Console.WriteLine("Products with uppercase name: ");

foreach (var product in uppercaseNameProducts)
{
    Console.WriteLine(product.Name);
}

class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}
