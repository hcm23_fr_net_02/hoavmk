namespace Animals;

public class Dog : Animal
{
    public string Breed { get; set; } = default!;

    public Dog(string name, int age, string breed)
        : base(name, age, typeof(Dog).Name)
    {
        Breed = breed;
        Sound = "Woof! Woof!";
    }

    public void Bark()
    {
        Console.WriteLine(Sound);
    }
}
