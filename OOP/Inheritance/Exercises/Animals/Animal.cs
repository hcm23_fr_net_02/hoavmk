namespace Animals;

public class Animal
{
    public string Name { get; private set; }

    public int Age { get; private set; }

    public string Type { get; private set; }

    public string Sound { get; protected set; } = string.Empty;

    public Animal(string name, int age, string type)
    {
        Name = name;
        Age = age;
        Type = type;
    }

    public void Speak()
    {
        Console.WriteLine($"The {Type} named {Name} says {Sound}");
    }
}
