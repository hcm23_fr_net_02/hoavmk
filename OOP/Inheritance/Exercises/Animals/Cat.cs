namespace Animals;

public class Cat : Animal
{
    public string FurColor { get; set; } = default!;

    public Cat(string name, int age, string furColor)
        : base(name, age, typeof(Cat).Name)
    {
        FurColor = furColor;
        Sound = "Meow! Meow!";
    }

    public void Meow()
    {
        Console.WriteLine(Sound);
    }
}
