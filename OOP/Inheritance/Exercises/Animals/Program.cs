﻿
using Animals;

List<Animal> animals = new()
{
    new Dog("Killer", 5, "Husky"),
    new Dog("Milo", 2, "Corgi"),
    new Cat("Yummy", 4, "Yellow"),
    new Dog("Lu", 4, "Shiba Inui"),
    new Cat("Tom", 10, "Gray Blue"),
    new Cat("Max", 3, "Black"),
};

foreach (var animal in animals)
{
    animal.Speak();
}

foreach (var animal in animals)
{
    if (animal.Type.Equals(typeof(Dog).Name))
    {
        var dog = (Dog)animal;
        dog.Bark();
    }

    if (animal.Type.Equals(typeof(Cat).Name))
    {
        var cat = (Cat)animal;
        cat.Meow();
    }
}
