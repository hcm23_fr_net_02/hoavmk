namespace BankAccounts;

public class Customer
{
    private decimal _accountBalance;

    public string Name { get; init; } = default!;

    public string Email { get; private set; } = default!;

    public decimal AccountBalance
    {
        get => _accountBalance;

        private set
        {
            if (_accountBalance < 0)
            {
                throw new Exception("AccountBalance must be larger than or qual 0.");
            }

            _accountBalance = value;
        }
    }

    public Customer(string name, string email, decimal accountBalance)
    {
        Name = name;
        Email = email;
        AccountBalance = accountBalance;
    }

    private bool CanAfford(decimal amount)
    {
        return _accountBalance >= amount;
    }

    public void AddFunds(decimal amount)
    {
        if (amount < 0)
        {
            throw new Exception("Amount must be larger than 0");
        }

        _accountBalance += amount;
    }

    public void MakePurchase(decimal amount)
    {
        if (!CanAfford(amount))
        {
            throw new Exception("Your account is not enough to make purchase.");
        }

        _accountBalance -= amount;
    }
}