namespace LibraryManagement01;

public class LibraryManagement
{
    private readonly Library _library = new();

    public void PrintMenu()
    {
        Console.WriteLine("*****Library Management*****");
        Console.WriteLine("1. View book list");
        Console.WriteLine("2. Borrow book");
        Console.WriteLine("3. Return book");
        Console.WriteLine("4. Exit");
        Console.Write("Select an option: ");
    }

    public void ShowBooks()
    {
        _library.PrintBookInfo();
    }

    public void ReturnBook()
    {
        int id = EnterBookId();

        try
        {
            _library.ReturnBook(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public void BorrowBook()
    {
        int id = EnterBookId();

         try
        {
            _library.CheckOut(id);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    public int EnterBookId()
    {
        string? input;
        int id;

        do
        {
            Console.Write("Enter book's id: ");
            input = Console.ReadLine();

            if (int.TryParse(input, out id))
            {
                break;
            }
            else
            {
                Console.WriteLine("Id must be an integer. Please enter again!");
            }
        }
        while (!string.IsNullOrEmpty(input));

        return id;
    }
}
