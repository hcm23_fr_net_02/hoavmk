namespace LibraryManagement01;

public class Book
{
    public int Id { get; init; }

    public string Title { get; private set; } = default!;

    public string Author { get; private set; } = default!;

    public bool Available { get; private set; }

    public Book(int id, string title, string author, bool available)
    {
        Id = id;
        Title = title;
        Author = author;
        Available = available;
    }

    public void SetAvailable(bool available)
    {
        Available = available;
    }
}
