namespace LibraryManagement01;

public class Library
{
    private readonly List<Book> _books = new()
    {
        new(1, "A", "MrA", true),
        new(2, "B", "MrB", true),
        new(3, "C", "MrC", false),
        new(4, "D", "MrA", true),
        new(5, "E", "MrD", true),
        new(6, "F", "MrA", false),
        new(7, "G", "MrA", true),
        new(8, "H", "MrB", true),
        new(9, "I", "MrC", true),
    };

    public void CheckOut(int id)
    {
        var book = GetBooKById(id);

        if (book.Available)
        {
            book.SetAvailable(false);
            Console.WriteLine($"Checkout the book with id = {id}");
            return;
        }

        Console.WriteLine($"The book with id = {id} is not available.");
    }


    public void PrintBookInfo()
    {
        Console.WriteLine("Books info");
        
        foreach (var book in _books)
        {
            Console.WriteLine($"Id = {book.Id} | Title = {book.Title} |"
                + $" Author = {book.Author} | Available = {book.Available}");
        }
    }

    public void ReturnBook(int id)
    {
        var book = GetBooKById(id);

        if (!book.Available)
        {
            book.SetAvailable(true);
            Console.WriteLine("Returned book.");

            return;
        }

        Console.WriteLine($"The book with id = {id} is already available.");
    }

    private Book GetBooKById(int id)
    {
        return _books.Find(b => b.Id == id) ??
            throw new Exception($"The book with id = {id} is not found.");
    }
}
