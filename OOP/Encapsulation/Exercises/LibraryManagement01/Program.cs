﻿
using LibraryManagement01;

var libraryManagement = new LibraryManagement();

while (true)
{
    libraryManagement.PrintMenu();

    string? choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            Console.Clear();
            libraryManagement.ShowBooks();
            break;
        case "2":
            Console.Clear();
            libraryManagement.BorrowBook();
            break;
        case "3":
            Console.Clear();
            libraryManagement.ReturnBook();
            break;
        case "4":
            return;
        default:
            Console.Clear();
            Console.WriteLine("Invalid option!");
            break;
    }
}
