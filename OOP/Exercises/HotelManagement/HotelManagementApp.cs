using HotelManagement.Enumerations;
using HotelManagement.Interfaces.Services;
using HotelManagement.Models;

namespace HotelManagement;

public class HotelManagementApp
{
    private readonly IRoomService _roomService;
    private readonly ICustomerService _customerService;
    private readonly IBookingService _bookingService;
    private readonly IInvoiceService _invoiceService;

    public HotelManagementApp(
        IRoomService roomService,
        ICustomerService customerService,
        IBookingService bookingService,
        IInvoiceService invoiceService)
    {
        _roomService = roomService;
        _customerService = customerService;
        _bookingService = bookingService;
        _invoiceService = invoiceService;
    }

    public void Run()
    {
        while (true)
        {
            Console.WriteLine("*******************************HotelManagement*******************************");
            Console.WriteLine("1. Add room");
            Console.WriteLine("2. Delete room");
            Console.WriteLine("3. Show rooms");
            Console.WriteLine("4. Book room");
            Console.WriteLine("5. Show bookings");
            Console.WriteLine("6. Create invoice");
            Console.WriteLine("7. Exit");
            Console.Write("Select an option: ");

            string? choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Console.Clear();
                    AddRoom();
                    break;

                case "2":
                    Console.Clear();
                    DeleteRoom();
                    break;
                
                case "3":
                    Console.Clear();
                    ShowRooms();
                    break;

                case "4":
                    Console.Clear();
                    BookRoom();
                    break;

                case "5":
                    Console.Clear();
                    ShowBookings();
                    break;
                
                case "6":
                    Console.Clear();
                    Checkout();
                    break;
                
                case "7":
                    Console.Clear();
                    var exited = Exit();

                    if (exited)
                        return;

                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Invalid option!");
                    break;
            }
        }
    }

    private void AddRoom()
    {
        Console.WriteLine("Create new room");
        Console.WriteLine("***Room types***");
        Console.WriteLine("1. Single");
        Console.WriteLine("2. Couple");
        Console.WriteLine("3. Family");
        
        var roomType = EnterRoomType();
        var price = EnterRoomPrice();
        var utilitiesInput = EnterUtilities();
        var utilities = utilitiesInput.Split(", ").ToList();
        var room = Room.Create((RoomType)roomType, price, true, utilities);

        _roomService.AddRoom(room);

        Console.WriteLine("Added new room.");
    }

    private void DeleteRoom()
    {
        var id = EnterNumberField("room's id");

        try
        {
            _roomService.DeleteRoom(id);
            Console.WriteLine($"Deleted the room with id = {id}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private void ShowRooms()
    {
        Console.WriteLine("*******************************Rooms*******************************");

        var rooms = _roomService.GetRooms();

        if (rooms.Count == 0)
        {
            Console.WriteLine("There is no room.");

            return;
        }

        foreach (var room in rooms)
        {
            Console.WriteLine($"Id = {room.Id} | RoomType = {room.RoomType} | Price = {room.Price}"
                + $" | Available = {room.Available}");

            Console.WriteLine("     Utilities: ");

            foreach (var item in room.Utilities)
            {
                Console.WriteLine($"        - {item}");
            }
        }
    }

    private void BookRoom()
    {
        var singleRooms = _roomService.GetAvailableRoomsByType(RoomType.Single);
        var coupleRooms = _roomService.GetAvailableRoomsByType(RoomType.Couple);
        var familyRooms = _roomService.GetAvailableRoomsByType(RoomType.Family);

        Console.WriteLine("************Available Rooms************");
        Console.Write("Single rooms: ");

        foreach (var room in singleRooms)
        {
            Console.Write($"{room.Id} ");
        }

        Console.WriteLine();
        Console.Write("Couple rooms: ");

        foreach (var room in coupleRooms)
        {
            Console.Write($"{room.Id} ");
        }
        
        Console.WriteLine();
        Console.Write("Family rooms: ");

        foreach (var room in familyRooms)
        {
            Console.Write($"{room.Id} ");
        }
        
        Console.WriteLine();

        List<int> roomIds = new();

        var roomIdsInput = EnterTextField("room ids", "Room ids are required.");
        var roomIdsStr = roomIdsInput.Split(", ").ToList();

        foreach (var roomId in roomIdsStr)
        {
            if (int.TryParse(roomId, out var parsedId))
            {
                roomIds.Add(parsedId);
            }
            else
            {
                Console.WriteLine($"Invalid id = {roomId}");

                return;
            }
        }

        List<Room> reservedRooms = new();

        foreach (var roomId in roomIds)
        {
            var room = _roomService.GetRoomById(roomId);
            
            if (room is null)
            {
                Console.WriteLine($"The room with id = {roomId} is not found.");

                return;
            }

            if (!room.Available)
            {
                Console.WriteLine($"The room with id = {roomId} is not Available.");

                return;
            }

            reservedRooms.Add(room);
        }

        var specialRequestsInput = EnterTextField(
            "special requests (separated by ', ')",
            "Special requests are required.");

        var specialRequests = specialRequestsInput.Split(", ").ToList();
        var checkInDate = EnterCheckInDate();
        var customerId = EnterNumberField("customer's id");
        var customer = _customerService.GetCustomerById(customerId);

        if (customer is null)
        {
            Console.WriteLine($"Customer with id = {customerId} is not found.");

            return;
        }

        var booking = Booking.Create(reservedRooms, checkInDate, specialRequests, customerId);

        try
        {
            _bookingService.AddBooking(booking);
            Console.WriteLine("Added new booking");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private void ShowBookings()
    {
        Console.WriteLine("*****************************Bookings*****************************");

        var bookings = _bookingService.GetBookingsWithCustomer();

        if (bookings.Count is 0)
        {
            Console.WriteLine("There is no bookings.");

            return;
        }

        foreach (var booking in bookings)
        {
            Console.WriteLine($"Id = {booking.Id} | BookingDate = {booking.BookingDate}"
                + $" | CheckInDate = {booking.CheckInDate} | CustomerId = {booking.CustomerId}"
                + $" | CustomerName = {booking.Customer!.FullName}");
            
            Console.WriteLine("     Reserved Rooms: ");

            foreach (var room in booking.ReservedRooms)
            {
                Console.WriteLine($"        - Id = {room.Id} | RoomType = {room.RoomType} | Price = {room.Price}");
            }

            Console.WriteLine("     Special Requests: ");

            foreach (var request in booking.SpecialRequests)
            {
                Console.WriteLine($"         - {request}");
            }
        }
    }

    private void Checkout()
    {
        var bookingId = EnterNumberField("booking's id");
        var booking = _bookingService.GetBookingById(bookingId);

        if (booking is null)
        {
            Console.WriteLine($"The booking with id = {bookingId} is not found.");

            return;
        }

        Console.Write("Enter used services: ");

        string? usedServicesInput = Console.ReadLine();
        var usedServices = new List<string>();

        if (!string.IsNullOrEmpty(usedServicesInput))
        {
            var services = usedServicesInput.Split(", ").ToList();

            usedServices.AddRange(services);
        }

        var invoice = new Invoice(
            bookingId: booking.Id,
            totalAmount: booking.CalculateTotalPrice(),
            usedServices: usedServices);

        _invoiceService.AddInvoice(invoice);
        
        var addedInvoice = _invoiceService.GetInvoiceById(invoice.Id);

        if (addedInvoice is null)
        {
            Console.WriteLine("Cannot create invoice.");

            return;
        }

        Console.WriteLine($"****************Invoice****************");
        Console.WriteLine($"- Invoice Id: {addedInvoice!.Id}");
        Console.WriteLine($"- Customer:");
        Console.WriteLine($"    Id: {addedInvoice.Id},      Name: {addedInvoice.Booking!.Customer!.FullName}");
        Console.WriteLine($"- Total amount: {addedInvoice.TotalAmount}");
    }

    private bool Exit()
    {
        Console.Write("Are you sure to exit (Y/N): ");

        string? choice = Console.ReadLine();
        
        if (choice is "Y" or "y")
            return true;

        return false;
    }

    private string EnterUtilities()
    {
        string? input;

        do
        {
            Console.Write("Enter utilities (separated by ', '): ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Utilities are required.");
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }

    private decimal EnterRoomPrice()
    {
        string? input;
        decimal price;
        bool parsedPrice;

        do
        {
            Console.Write("Enter room's price: ");
            input = Console.ReadLine();
            parsedPrice = decimal.TryParse(input, out price);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Price is required.");
            }
            else if (!parsedPrice)
            {
                Console.WriteLine("Invalid price.");
            }
            else if (price <= 0)
            {
                Console.WriteLine("Price must be larger than 0.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedPrice || price <= 0);

        return price;
    }

    private DateTime EnterCheckInDate()
    {
        string? checkInDateInput;
        bool parsedCheckInDate;
        DateTime checkInDate;

        do
        {
            Console.Write("Enter check-in date (MM/dd/yyyy): ");
            checkInDateInput = Console.ReadLine();
            parsedCheckInDate = DateTime.TryParse(checkInDateInput, out checkInDate);

            if (checkInDate < DateTime.UtcNow)
            {
                Console.WriteLine("Invalid check-in date.");
            }
        }
        while (string.IsNullOrEmpty(checkInDateInput) || !parsedCheckInDate);

        return checkInDate;
    }

    private int EnterNumberField(string fieldName)
    {
        string? input;
        int number;
        bool parsedNumber;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();
            parsedNumber = int.TryParse(input, out number);

            if (!parsedNumber)
            {
                Console.WriteLine($"Invalid {fieldName}");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedNumber);

        return number;
    }

    private string EnterTextField(string fieldName, string alertMessage)
    {
        string? input;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine($"{alertMessage}");
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }

    private int EnterRoomType()
    {
        string? input;
        int type;
        bool parsedRoomType;

        do
        {
            Console.Write("Select the room type: ");
            input = Console.ReadLine();
            parsedRoomType = int.TryParse(input, out type);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Room type is required.");
            }
            else if (!parsedRoomType)
            {
                Console.WriteLine("Invalid room type.");
            }
            else if (type < 1 || type > 3)
            {
                Console.WriteLine("Room type must be between 1 and 3.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedRoomType || type < 1 || type > 3);

        return type;
    }
}
