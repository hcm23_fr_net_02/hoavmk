namespace HotelManagement.Enumerations;

public enum RoomType
{
    Single,
    Couple,
    Family
}
