namespace HotelManagement.Models;

public class Invoice
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public decimal TotalAmount { get; private set; }

    public int BookingId { get; private set; }

    public Booking? Booking { get; private set; }

    public List<string> UsedServices { get; private set; }

    public Invoice(int bookingId, decimal totalAmount, List<string> usedServices, Booking? booking = null)
    {
        Id = s_identity++;
        BookingId = bookingId;
        TotalAmount = totalAmount;
        UsedServices = usedServices;
        Booking = booking;
    }

    public Invoice With(Booking booking)
    {
        Booking = booking;

        return this;
    }
}
