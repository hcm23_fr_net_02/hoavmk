namespace HotelManagement.Models;

public class Customer
{
    private static int s_identity = 1;

    public int MemberId { get; init; }

    public string FullName { get; private set; } = default!;

    public string PhoneNumber { get; private set; } = default!;

    public string Address { get; private set; } = default!;

    private Customer(
        int memberId,
        string fullName,
        string phoneNumber,
        string address)
    {
        MemberId = memberId;
        FullName = fullName;
        PhoneNumber = phoneNumber;
        Address = address;
    }

    public static Customer Create(
        string fullName,
        string phoneNumber,
        string address)
    {
        return new Customer(
            s_identity++,
            fullName,
            phoneNumber,
            address);
    }
}
