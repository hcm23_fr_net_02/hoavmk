namespace HotelManagement.Models;

public class Booking
{
    private static int s_identity = 1;
    private readonly List<Room> _reservedRooms;

    public int Id { get; init; }

    public DateTime BookingDate { get; private set; }

    public DateTime CheckInDate { get; private set; }

    public List<string> SpecialRequests { get; private set; }

    public int CustomerId { get; private set; }

    public Customer? Customer { get; private set; }

    public List<Room> ReservedRooms => _reservedRooms.ToList();

    private Booking(
        int id,
        List<Room> reservedRooms,
        DateTime bookingDate,
        DateTime checkInDate,
        List<string> specialRequests,
        int customerId,
        Customer? customer = null)
    {
        Id = id;
        _reservedRooms = reservedRooms;
        BookingDate = bookingDate;
        CheckInDate = checkInDate;
        SpecialRequests = specialRequests;
        CustomerId = customerId;
        Customer = customer;
    }

    public static Booking Create(
        List<Room> reservedRooms,
        DateTime checkInDate,
        List<string> specialRequests,
        int customerId,
        Customer? customer = null)
    {
        return new Booking(
            s_identity++,
            reservedRooms,
            DateTime.UtcNow,
            checkInDate,
            specialRequests,
            customerId,
            customer);
    }

    public Booking With(Customer customer)
    {
        Customer = customer;

        return this;
    }

    public decimal CalculateTotalPrice()
    {
        decimal totalPrice = 0;

        foreach (var room in _reservedRooms)
        {
            totalPrice += room.Price;
        }

        return totalPrice;
    }
}
