using HotelManagement.Enumerations;
using HotelManagement.Interfaces.Repositories;

namespace HotelManagement.Models;

public class Hotel : IRoomRepository
{
    private readonly List<Room> _rooms = new()
    {
        Room.Create(RoomType.Single, 50, true, new List<string> { "TV", "Swimming pool" }),
        Room.Create(RoomType.Couple, 60, true, new List<string> { "TV" }),
        Room.Create(RoomType.Family, 120, true, new List<string> { "TV", "Swimming pool" }),
        Room.Create(RoomType.Single, 30, false, new List<string> { "TV" })
    };

    public Hotel()
    {
    }

    public List<Room> GetRooms()
    {
        return _rooms.ToList();
    }

    public void AddRoom(Room room)
    {
        _rooms.Add(room);
    }

    public Room? GetRoomById(int id)
    {
        return _rooms.Find(x => x.Id == id);
    }

    public void DeleteRoom(Room room)
    {
        _rooms.Remove(room);
    }

    public List<Room> GetAvailableRoomsByType(RoomType roomType)
    {
        return _rooms.Where(r => r.RoomType == roomType && r.Available == true)
            .ToList();
    }
}
