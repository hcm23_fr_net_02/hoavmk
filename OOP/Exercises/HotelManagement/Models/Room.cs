using HotelManagement.Enumerations;

namespace HotelManagement.Models;

public class Room
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public RoomType RoomType { get; private set; }

    public decimal Price { get; private set; }

    public bool Available { get; private set; }

    public List<string> Utilities { get; private set; }

    private Room(
        int id,
        RoomType roomType,
        decimal price,
        bool available,
        List<string> utilities)
    {
        Id = id;
        RoomType = roomType;
        Price = price;
        Available = available;
        Utilities = utilities;
    }

    public static Room Create(
        RoomType roomType,
        decimal price,
        bool available,
        List<string> utilities)
    {
        return new Room(
            s_identity++,
            roomType,
            price,
            available,
            utilities);
    }

    public void UpdateAvailable(bool status)
    {
        Available = status;
    }
}
