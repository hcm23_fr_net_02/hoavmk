﻿using HotelManagement;
using HotelManagement.Models;
using HotelManagement.Repositories;
using HotelManagement.Services;

var hotel = new Hotel();
var customerRepository = new CustomerRepository();
var bookingRepository = new BookingRepository();
var invoiceRepository = new InvoiceRepository();
var roomService = new RoomService(hotel, bookingRepository);
var customerService = new CustomerService(customerRepository);
var bookingService = new BookingService(bookingRepository, customerRepository);
var invoiceService = new InvoiceService(
    invoiceRepository,
    bookingRepository,
    customerRepository);

var hotelManagementApp = new HotelManagementApp(
    roomService,
    customerService,
    bookingService,
    invoiceService);

hotelManagementApp.Run();
