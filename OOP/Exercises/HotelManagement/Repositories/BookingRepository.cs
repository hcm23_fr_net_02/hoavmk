using HotelManagement.Interfaces.Repositories;
using HotelManagement.Models;

namespace HotelManagement.Repositories;

public class BookingRepository : IBookingRepository
{
    private readonly List<Booking> _bookings = new();

    public BookingRepository()
    {
    }

    public void AddBooking(Booking booking)
    {
        _bookings.Add(booking);
    }

    public List<Booking> GetBookings()
    {
        return _bookings.ToList();
    }

    public Booking? GetBookingById(int id)
    {
        return _bookings.Find(b => b.Id == id);
    }

    public bool IsAnyBookingWithRoomId(int roomId)
    {
        foreach (var booking in _bookings)
        {
            foreach (var room in booking.ReservedRooms)
            {
                if (room.Id == roomId)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
