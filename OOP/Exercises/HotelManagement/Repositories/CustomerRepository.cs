using HotelManagement.Interfaces.Repositories;
using HotelManagement.Models;

namespace HotelManagement.Repositories;

public class CustomerRepository : ICustomerRepository
{
    private readonly List<Customer> _customers = new()
    {
        Customer.Create("Mr. A", "0987654321", "HCM"),
        Customer.Create("Mr. B", "0987654322", "HN"),
    };

    public Customer? GetCustomerById(int id)
    {
        return _customers.Find(x => x.MemberId == id);
    }

    public List<Customer> GetCustomers()
    {
        return _customers.ToList();
    }
}
