using HotelManagement.Interfaces.Repositories;
using HotelManagement.Models;

namespace HotelManagement.Repositories;

public class InvoiceRepository : IInvoiceRepository
{
    private readonly List<Invoice> _invoices = new();

    public InvoiceRepository()
    {
    }

    public void AddInvoice(Invoice invoice)
    {
        _invoices.Add(invoice);
    }

    public Invoice? GetInvoiceById(int id)
    {
        return _invoices.Find(x => x.Id == id);
    }

    public Invoice? GetInvoiceByBookingId(int bookingId)
    {
        return _invoices.Find(x => x.BookingId == bookingId);
    }

    public List<Invoice> GetInvoices()
    {
        return _invoices.ToList();
    }
}
