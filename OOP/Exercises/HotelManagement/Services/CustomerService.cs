using HotelManagement.Interfaces.Repositories;
using HotelManagement.Interfaces.Services;
using HotelManagement.Models;

namespace HotelManagement.Services;

public class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _customerRepository;

    public CustomerService(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    public Customer? GetCustomerById(int id)
    {
        return _customerRepository.GetCustomerById(id);
    }

    public List<Customer> GetCustomers()
    {
        return _customerRepository.GetCustomers();
    }
}
