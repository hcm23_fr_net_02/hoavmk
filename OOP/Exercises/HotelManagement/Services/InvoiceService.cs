using HotelManagement.Interfaces.Repositories;
using HotelManagement.Interfaces.Services;
using HotelManagement.Models;

namespace HotelManagement.Services;

public class InvoiceService : IInvoiceService
{
    private readonly IInvoiceRepository _invoiceRepository;
    private readonly IBookingRepository _bookingRepository;
    private readonly ICustomerRepository _customerRepository;

    public InvoiceService(
        IInvoiceRepository invoiceRepository,
        IBookingRepository bookingRepository,
        ICustomerRepository customerRepository)
    {
        _invoiceRepository = invoiceRepository;
        _bookingRepository = bookingRepository;
        _customerRepository = customerRepository;
    }

    public void AddInvoice(Invoice invoice)
    {
        var existingInvoice = GetInvoiceByBookingId(invoice.BookingId);

        if (existingInvoice is not null)
        {
            Console.WriteLine($"The invoice for booking with id = {invoice.BookingId} was already exist.");

            return;
        }

        var booking = _bookingRepository.GetBookingById(invoice.BookingId);

        if (booking is null)
        {
            Console.WriteLine($"Cannot add new invoice, because the booking with"
                + $" id = {invoice.BookingId} was not found.");

            return;
        }

        foreach (var room in booking.ReservedRooms)
        {
            room.UpdateAvailable(true);
        }

        _invoiceRepository.AddInvoice(invoice);
    }

    public Invoice? GetInvoiceById(int id)
    {
        var bookings = _bookingRepository.GetBookings().Join(
            _customerRepository.GetCustomers(),
            booking => booking.CustomerId,
            customer => customer.MemberId,
            (booking, customer) => booking.With(customer))
            .ToList();

        var invoices = _invoiceRepository.GetInvoices().Join(
            bookings,
            invoice => invoice.BookingId,
            booking => booking.Id,
            (invoice, booking) => invoice.With(booking))
            .ToList();

        return invoices.Find(x => x.Id == id);
    }

    public Invoice? GetInvoiceByBookingId(int bookingId)
    {
        return _invoiceRepository.GetInvoiceByBookingId(bookingId);
    }

    public List<Invoice> GetInvoices()
    {
        return _invoiceRepository.GetInvoices();
    }
}
