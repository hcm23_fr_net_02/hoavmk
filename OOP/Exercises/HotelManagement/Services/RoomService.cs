using HotelManagement.Enumerations;
using HotelManagement.Interfaces.Repositories;
using HotelManagement.Interfaces.Services;
using HotelManagement.Models;

namespace HotelManagement.Services;

public class RoomService : IRoomService
{
    private readonly IRoomRepository _roomRepository;
    private readonly IBookingRepository _bookingRepository;

    public RoomService(
        IRoomRepository roomRepository,
        IBookingRepository bookingRepository)
    {
        _roomRepository = roomRepository;
        _bookingRepository = bookingRepository;
    }

    public void AddRoom(Room room)
    {
        _roomRepository.AddRoom(room);
    }

    public void DeleteRoom(int id)
    {
        var room = _roomRepository.GetRoomById(id)
            ?? throw new Exception($"The room with id = {id} was not found.");

        var reservedRoom = _bookingRepository.IsAnyBookingWithRoomId(id);

        if (reservedRoom)
        {
            Console.WriteLine($"The room with id = {id} is already referenced by Booking table.");

            return;
        }
        
        _roomRepository.DeleteRoom(room);
    }

    public List<Room> GetAvailableRoomsByType(RoomType roomType)
    {
        return _roomRepository.GetAvailableRoomsByType(roomType);
    }

    public Room? GetRoomById(int id)
    {
        return _roomRepository.GetRoomById(id);
    }

    public List<Room> GetRooms()
    {
        return _roomRepository.GetRooms();
    }
}
