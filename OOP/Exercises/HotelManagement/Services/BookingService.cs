using HotelManagement.Interfaces.Repositories;
using HotelManagement.Interfaces.Services;
using HotelManagement.Models;

namespace HotelManagement.Services;

public class BookingService : IBookingService
{
    private readonly IBookingRepository _bookingRepository;
    private readonly ICustomerRepository _customerRepository;

    public BookingService(
        IBookingRepository bookingRepository,
        ICustomerRepository customerRepository)
    {
        _bookingRepository = bookingRepository;
        _customerRepository = customerRepository;
    }

    public void AddBooking(Booking booking)
    {
        foreach (var room in booking.ReservedRooms)
        {
            if (!room.Available)
                throw new Exception($"The room with id = {room.Id} is not available.");
        }

        foreach (var room in booking.ReservedRooms)
        {
            room.UpdateAvailable(false);
        }

        _bookingRepository.AddBooking(booking);
    }

    public List<Booking> GetBookings()
    {
        return _bookingRepository.GetBookings();
    }

    public List<Booking> GetBookingsWithCustomer()
    {
        return _bookingRepository.GetBookings().Join(
            _customerRepository.GetCustomers(),
            booking => booking.CustomerId,
            customer => customer.MemberId,
            (booking, customer) => booking.With(customer))
            .ToList();
    }

    public Booking? GetBookingById(int id)
    {
        return _bookingRepository.GetBookingById(id);
    }
}
