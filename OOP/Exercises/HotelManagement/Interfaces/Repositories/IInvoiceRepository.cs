using HotelManagement.Models;

namespace HotelManagement.Interfaces.Repositories;

public interface IInvoiceRepository
{
    List<Invoice> GetInvoices();
    void AddInvoice(Invoice invoice);
    Invoice? GetInvoiceById(int id);
    Invoice? GetInvoiceByBookingId(int bookingId);
}
