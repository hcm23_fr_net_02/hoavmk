using HotelManagement.Enumerations;
using HotelManagement.Models;

namespace HotelManagement.Interfaces.Repositories;

public interface IRoomRepository
{
    void AddRoom(Room room);
    void DeleteRoom(Room room);
    Room? GetRoomById(int id);
    List<Room> GetRooms();
    List<Room> GetAvailableRoomsByType(RoomType roomType);
}
