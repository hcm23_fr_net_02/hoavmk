using HotelManagement.Models;

namespace HotelManagement.Interfaces.Repositories;

public interface IBookingRepository
{
    List<Booking> GetBookings();
    void AddBooking(Booking booking);
    Booking? GetBookingById(int id);
    bool IsAnyBookingWithRoomId(int roomId);
}
