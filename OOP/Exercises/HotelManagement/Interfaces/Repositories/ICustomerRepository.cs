using HotelManagement.Models;

namespace HotelManagement.Interfaces.Repositories;

public interface ICustomerRepository
{
    List<Customer> GetCustomers();
    Customer? GetCustomerById(int id);
}
