using HotelManagement.Models;

namespace HotelManagement.Interfaces.Services;

public interface IInvoiceService
{
    List<Invoice> GetInvoices();
    void AddInvoice(Invoice invoice);
    Invoice? GetInvoiceById(int id);
    Invoice? GetInvoiceByBookingId(int bookingId);
}
