using HotelManagement.Models;

namespace HotelManagement.Interfaces.Services;

public interface IBookingService
{
    List<Booking> GetBookings();
    List<Booking> GetBookingsWithCustomer();
    void AddBooking(Booking booking);
    Booking? GetBookingById(int id);
    // bool IsAnyBookingWithRoomId(int roomId);
}
