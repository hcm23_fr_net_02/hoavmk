using HotelManagement.Models;

namespace HotelManagement.Interfaces.Services;

public interface ICustomerService
{
    List<Customer> GetCustomers();
    Customer? GetCustomerById(int id);
}
