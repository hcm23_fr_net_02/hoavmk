using HotelManagement.Enumerations;
using HotelManagement.Models;

namespace HotelManagement.Interfaces.Services;

public interface IRoomService
{
    void AddRoom(Room room);
    void DeleteRoom(int id);
    Room? GetRoomById(int id);
    List<Room> GetRooms();
    List<Room> GetAvailableRoomsByType(RoomType roomType);
}
