﻿using MobilePhoneStoreManagement;
using MobilePhoneStoreManagement.Models;
using MobilePhoneStoreManagement.Repositories;
using MobilePhoneStoreManagement.Services;

var store = new Store();
var customerRepository = new CustomerRepository();
var purchaseRepository = new PurchaseRepository();
var customerService = new CustomerService(customerRepository);
var mobilePhoneService = new MobilePhoneService(store);
var purchaseService = new PurchaseService(purchaseRepository, customerRepository);

var mobilePhoneStoreManagementApp = new MobilePhoneStoreManagementApp(
    mobilePhoneService,
    customerService,
    purchaseService);

mobilePhoneStoreManagementApp.Run();
