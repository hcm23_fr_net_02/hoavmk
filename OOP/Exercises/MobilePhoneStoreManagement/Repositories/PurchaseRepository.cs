using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Repositories;

public class PurchaseRepository : IPurchaseRepository
{
    private readonly List<Purchase> _purchases = new();

    public void AddPurchase(Purchase purchase)
    {
        _purchases.Add(purchase);
    }

    public List<Purchase> GetPurchases()
    {
        return _purchases.ToList();
    }
}
