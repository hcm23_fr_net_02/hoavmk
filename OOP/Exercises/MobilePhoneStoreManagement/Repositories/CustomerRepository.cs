using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Repositories;

public class CustomerRepository : ICustomerRepository
{
    private readonly List<Customer> _customers = new()
    {
        Customer.Create("Mr. A", "HCM"),
        Customer.Create("Mr. B", "HCM"),
        Customer.Create("Mr. C", "HN"),
        Customer.Create("Mr. D", "HCM"),
        Customer.Create("Mr. E", "HCM"),
    };

    public void AddCustomer(Customer customer)
    {
        _customers.Add(customer);
    }

    public Customer? GetCustomerById(int id)
    {
        return _customers.Find(x => x.Id == id);
    }

    public List<Customer> GetCustomers()
    {
        return _customers.ToList();
    }
}
