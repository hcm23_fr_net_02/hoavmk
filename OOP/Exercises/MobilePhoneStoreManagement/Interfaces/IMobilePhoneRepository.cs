using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface IMobilePhoneRepository
{
    void AddMobilePhone(MobilePhone mobilePhone);
    void DeleteMobilePhone(MobilePhone mobilePhone);
    List<MobilePhone> GetMobilePhones();
    MobilePhone? GetMobilePhoneById(int id);
}
