using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface IPurchaseService
{
    List<Purchase> GetPurchases();
    List<Purchase> GetPurchasesWithCustomer();
    void AddPurchase(Purchase purchase);
}
