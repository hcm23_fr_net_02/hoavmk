using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface ICustomerService
{
    List<Customer> GetCustomers();
    void AddCustomer(Customer customer);
    Customer? GetCustomerById(int id);
}
