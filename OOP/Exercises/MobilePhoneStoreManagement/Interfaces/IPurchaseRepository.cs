using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface IPurchaseRepository
{
    List<Purchase> GetPurchases();
    void AddPurchase(Purchase purchase);
}
