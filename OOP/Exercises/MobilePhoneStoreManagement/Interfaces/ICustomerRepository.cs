using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface ICustomerRepository
{
    List<Customer> GetCustomers();
    void AddCustomer(Customer customer);
    Customer? GetCustomerById(int id);
}
