using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Interfaces;

public interface IMobilePhoneService
{
    void AddMobilePhone(MobilePhone mobilePhone);
    void DeleteMobilePhone(int mobilePhoneId);
    List<MobilePhone> GetMobilePhones();
    MobilePhone? GetMobilePhoneById(int id);
}
