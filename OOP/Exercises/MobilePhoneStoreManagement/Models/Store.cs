using MobilePhoneStoreManagement.Interfaces;

namespace MobilePhoneStoreManagement.Models;

public class Store : IMobilePhoneRepository
{
    private readonly List<MobilePhone> _mobilePhones = new()
    {
        MobilePhone.Create("iPhone 14", "Apple", 50, 1500),
        MobilePhone.Create("iPhone 15", "Apple", 50, 1999),
        MobilePhone.Create("Samsung S22", "Samsung", 100, 1499),
        MobilePhone.Create("Samsung Note 23", "Samsung", 0, 1999)
    };

    public void AddMobilePhone(MobilePhone mobilePhone)
    {
        _mobilePhones.Add(mobilePhone);
    }

    public void DeleteMobilePhone(MobilePhone mobilePhone)
    {
        _mobilePhones.Remove(mobilePhone);
    }

    public MobilePhone? GetMobilePhoneById(int id)
    {
        return _mobilePhones.Find(x => x.Id == id);
    }

    public List<MobilePhone> GetMobilePhones()
    {
        return _mobilePhones.ToList();
    }
}
