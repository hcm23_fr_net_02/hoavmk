namespace MobilePhoneStoreManagement.Models;

public class Purchase
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public DateTime PurchasedDate { get; private set; }

    public List<MobilePhone> MobilePhones { get; private set; }

    public int CustomerId { get; private set; }

    public Customer? Customer { get; private set; }

    private Purchase(
        int id,
        int customerId,
        DateTime purchasedDate,
        List<MobilePhone> mobilePhones)
    {
        Id = id;
        CustomerId = customerId;
        PurchasedDate = purchasedDate;
        MobilePhones = mobilePhones;
    }

    public static Purchase Create(
        int customerId,
        List<MobilePhone> mobilePhones)
    {
        return new Purchase(
            s_identity++,
            customerId,
            DateTime.UtcNow,
            mobilePhones);
    }

    public Purchase WithCustomer(Customer customer)
    {
        Customer = customer;

        return this;
    }
}
