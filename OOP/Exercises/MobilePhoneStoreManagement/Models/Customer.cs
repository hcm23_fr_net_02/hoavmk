namespace MobilePhoneStoreManagement.Models;

public class Customer
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public string FullName { get; private set; }

    public string Address { get; private set; }

    private Customer(int id, string fullName, string address)
    {
        Id = id;
        FullName = fullName;
        Address = address;
    }

    public static Customer Create(
        string fullName,
        string address)
    {
        return new Customer(
            s_identity++,
            fullName,
            address);
    }
}
