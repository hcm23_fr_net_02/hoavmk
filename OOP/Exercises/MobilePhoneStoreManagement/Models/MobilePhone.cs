namespace MobilePhoneStoreManagement.Models;

public class MobilePhone
{
    private static int s_identity = 1;

    public int Id { get; init; }
    
    public string Name { get; private set; }

    public string Manufacturer { get; private set; }

    public int StockQuantity { get; private set; }

    public decimal Price { get; private set; }

    private MobilePhone(
        int id,
        string name,
        string manufacturer,
        int stockQuantity,
        decimal price)
    {
        Id = id;
        Name = name;
        Manufacturer = manufacturer;
        StockQuantity = stockQuantity;
        Price = price;
    }

    public static MobilePhone Create(
        string name,
        string manufacturer,
        int stockQuantity,
        decimal price)
    {
        return new MobilePhone(
            s_identity++,
            name,
            manufacturer,
            stockQuantity,
            price);
    }

    public void UpdateStockQuantity(int stockQuantity)
    {
        if (stockQuantity < 0)
            throw new Exception("Stock quantity must be larger than or equal 0");

        StockQuantity = stockQuantity;
    }

    public override string ToString()
    {
        return $"Id = {Id} | Name = {Name} | Manufacturer = {Manufacturer}"
            + $" | StockQuantity = {StockQuantity} | Price = {Price}";
    }
}
