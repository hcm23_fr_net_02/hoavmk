using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement;

public class MobilePhoneStoreManagementApp
{
    private readonly IMobilePhoneService _mobilePhoneService;
    private readonly ICustomerService _customerService;
    private readonly IPurchaseService _purchaseService;

    public MobilePhoneStoreManagementApp(
        IMobilePhoneService mobilePhoneService,
        ICustomerService customerService,
        IPurchaseService purchaseService)
    {
        _mobilePhoneService = mobilePhoneService;
        _customerService = customerService;
        _purchaseService = purchaseService;
    }

    public void Run()
    {
        while (true)
        {
            Console.WriteLine("************************MobilePhoneStoreManagement************************");
            Console.WriteLine("1. Add mobile phone");
            Console.WriteLine("2. Delete mobile phone");
            Console.WriteLine("3. Show mobile phones");
            Console.WriteLine("4. Purchase mobile phone");
            Console.WriteLine("5. Show purchases");
            Console.WriteLine("6. Exit");
            Console.Write("Select an option: ");

            string? choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Console.Clear();
                    AddMobilePhone();
                    break;

                case "2":
                    Console.Clear();
                    DeleteMobilePhone();
                    break;
                
                case "3":
                    Console.Clear();
                    ShowMobilePhones();
                    break;

                case "4":
                    Console.Clear();
                    PurchaseMobilePhone();
                    break;

                case "5":
                    Console.Clear();
                    ShowPurchases();
                    break;
                
                case "6":
                    Console.Clear();
                    var exited = Exit();

                    if (exited)
                        return;

                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Invalid option!");
                    break;
            }
        }
    }

    private void AddMobilePhone()
    {
        var name = EnterTextField("mobile phone's name");
        var manufacturer = EnterTextField("mobile phone's manufacturer");
        var quantity = EnterStockQuantity();
        var price = EnterPrice();
        var mobilePhone = MobilePhone.Create(name, manufacturer, quantity, price);
        
        _mobilePhoneService.AddMobilePhone(mobilePhone);

        Console.WriteLine("Added new mobile phone.");
    }

    private void DeleteMobilePhone()
    {
        var id = EnterNumberField("mobile phone's id");
        
        try
        {
            _mobilePhoneService.DeleteMobilePhone(id);
            Console.WriteLine($"Deleted mobile phone with id ={id}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private void ShowMobilePhones()
    {
        var mobilePhones = _mobilePhoneService.GetMobilePhones();

        Console.WriteLine("************************Mobile Phones************************");
        
        foreach (var mobilePhone in mobilePhones)
        {
            Console.WriteLine($" - {mobilePhone}");
        }
    }

    private void PurchaseMobilePhone()
    {
        var customerId = EnterNumberField("customer's id");
        var customer = _customerService.GetCustomerById(customerId);

        if (customer is null)
        {
            Console.WriteLine($"The customer with id = {customerId} was not found.");
            
            return;
        }

        var mobilePhoneIdsInput = EnterMobilePhoneIds();
        var listMobilePhoneIds = mobilePhoneIdsInput.Split(", ").ToList();
        var mobilePhoneIds = new List<int>();
        var purchasedMobilePhones = new List<MobilePhone>();

        foreach (var mobilePhoneId in listMobilePhoneIds)
        {
            if (int.TryParse(mobilePhoneId, out var id))
            {
                mobilePhoneIds.Add(id);
            }
            else
            {
                Console.WriteLine("Invalid mobile phone's id.");
                
                return;
            }
        }

        foreach (var mobilePhoneId in mobilePhoneIds)
        {
            var mobilePhone = _mobilePhoneService.GetMobilePhoneById(mobilePhoneId);

            if (mobilePhone is null)
            {
                Console.WriteLine($"The mobile phone with id = {mobilePhoneId} was not found.");

                return;
            }

            if (mobilePhone.StockQuantity == 0)
            {
                Console.WriteLine($"The mobile phone with id = {mobilePhoneId} was out of stock.");

                return;
            }

            purchasedMobilePhones.Add(mobilePhone);
        }

        var purchase = Purchase.Create(customerId, purchasedMobilePhones);

        _purchaseService.AddPurchase(purchase);
        Console.WriteLine("Added new purchase.");
    }


    private void ShowPurchases()
    {
        var purchases = _purchaseService.GetPurchasesWithCustomer();

        Console.WriteLine("************************Purchases************************");

        foreach (var purchase in purchases)
        {
            Console.WriteLine($"- Id = {purchase.Id} | PurchasedDate = {purchase.PurchasedDate}"
                + $" | CustomerId = {purchase.Customer!.Id} | CustomerName = {purchase.Customer!.FullName}");
            
            Console.WriteLine("     Purchased mobile phones:");

            foreach (var mobilePhone in purchase.MobilePhones)
            {
                Console.WriteLine($"           . Id = {mobilePhone.Id} | Name = {mobilePhone.Name}");
            }
        }
    }

    private bool Exit()
    {
        Console.Write("Are you sure to exit (Y/N): ");

        string? choice = Console.ReadLine();
        
        if (choice is "Y" or "y")
            return true;

        return false;
    }

    private void FindMobilePhone()
    {
        var id = EnterNumberField("mobile phone's id");
        var mobilePhone = _mobilePhoneService.GetMobilePhoneById(id);

        if (mobilePhone is null)
        {
            Console.WriteLine($"The mobile phone with id = {id} was not found.");

            return;
        }

        Console.WriteLine($"mobilePhone");
    }

    private int EnterNumberField(string fieldName)
    {
        string? input;
        int number;
        bool parsedNumber;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();
            parsedNumber = int.TryParse(input, out number);

            if (!parsedNumber)
            {
                Console.WriteLine($"Invalid {fieldName}");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedNumber);

        return number;
    }

    private string EnterTextField(string fieldName)
    {
        string? input;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine($"The {fieldName} is required");
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }

    private int EnterStockQuantity()
    {
        string? input;
        int quantity;
        bool parsedQuantity;

        do
        {
            Console.Write("Enter stock quantity: ");
            input = Console.ReadLine();
            parsedQuantity = int.TryParse(input, out quantity);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Stock quantity is required.");
            }
            else if (!parsedQuantity)
            {
                Console.WriteLine("Stock quantity must be a number");
            }
            else if (quantity < 0)
            {
                Console.WriteLine("Stock quantity must be larger than 0.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedQuantity || quantity < 0);

        return quantity;
    }

    private decimal EnterPrice()
    {
        string? input;
        decimal price;
        bool parsedPrice;

        do
        {
            Console.Write("Enter price: ");
            input = Console.ReadLine();
            parsedPrice = decimal.TryParse(input, out price);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Price is required.");
            }
            else if (!parsedPrice)
            {
                Console.WriteLine("Price must be a number");
            }
            else if (price < 0)
            {
                Console.WriteLine("Price must be larger than 0.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedPrice || price < 0);

        return price;
    }

    private string EnterMobilePhoneIds()
    {
        string? input;

        do
        {
            Console.Write("Enter mobile phone ids (separated by ', '): ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Please provide at least one mobile phone's id.");
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }
}
