using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Services;

public class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _customerRepository;

    public CustomerService(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    public void AddCustomer(Customer customer)
    {
        _customerRepository.AddCustomer(customer);
    }

    public Customer? GetCustomerById(int id)
    {
        return _customerRepository.GetCustomerById(id);
    }

    public List<Customer> GetCustomers()
    {
        return _customerRepository.GetCustomers();
    }
}
