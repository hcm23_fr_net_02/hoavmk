using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Services;

public class PurchaseService : IPurchaseService
{
    private readonly IPurchaseRepository _purchaseRepository;
    private readonly ICustomerRepository _customerRepository;

    public PurchaseService(
        IPurchaseRepository purchaseRepository,
        ICustomerRepository customerRepository)
    {
        _purchaseRepository = purchaseRepository;
        _customerRepository = customerRepository;
    }

    public void AddPurchase(Purchase purchase)
    {
        try
        {
            foreach (var mobilePhone in purchase.MobilePhones)
            {
                mobilePhone.UpdateStockQuantity(mobilePhone.StockQuantity - 1);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        _purchaseRepository.AddPurchase(purchase);
    }

    public List<Purchase> GetPurchases()
    {
        return _purchaseRepository.GetPurchases();
    }

    public List<Purchase> GetPurchasesWithCustomer()
    {
        return _purchaseRepository.GetPurchases().Join(
            _customerRepository.GetCustomers(),
            purchase => purchase.CustomerId,
            customer => customer.Id,
            (purchase, customer) => purchase.WithCustomer(customer))
            .ToList();
    }
}
