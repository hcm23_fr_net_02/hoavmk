using MobilePhoneStoreManagement.Interfaces;
using MobilePhoneStoreManagement.Models;

namespace MobilePhoneStoreManagement.Services;

public class MobilePhoneService : IMobilePhoneService
{
    private readonly IMobilePhoneRepository _mobilePhoneRepository;

    public MobilePhoneService(IMobilePhoneRepository mobilePhoneRepository)
    {
        _mobilePhoneRepository = mobilePhoneRepository;
    }

    public void AddMobilePhone(MobilePhone mobilePhone)
    {
        _mobilePhoneRepository.AddMobilePhone(mobilePhone);
    }

    public void DeleteMobilePhone(int mobilePhoneId)
    {
        var mobilePhone = GetMobilePhoneById(mobilePhoneId)
            ?? throw new Exception(
                $"The mobile phone with id = {mobilePhoneId} was not found.");

        _mobilePhoneRepository.DeleteMobilePhone(mobilePhone);
    }

    public MobilePhone? GetMobilePhoneById(int id)
    {
        return _mobilePhoneRepository.GetMobilePhoneById(id);
    }

    public List<MobilePhone> GetMobilePhones()
    {
        return _mobilePhoneRepository.GetMobilePhones();
    }
}
