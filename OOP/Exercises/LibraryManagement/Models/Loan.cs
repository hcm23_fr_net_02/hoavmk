namespace LibraryManagement.Models;

public class Loan
{
    private static int s_identity = 1;
    private readonly List<Book> _borrowedBooks;

    public int Id { get; init; }

    public int CustomerId { get; private set; }

    public DateTime BorrowDate { get; private set; }

    public DateTime DueDate { get; private set; }

    public IReadOnlyList<Book> BorrowedBooks => _borrowedBooks;

    private Loan(
        int id,
        int customerId,
        DateTime borrowDate,
        DateTime dueDate,
        List<Book> borrowedBooks)
    {
        Id = id;
        CustomerId = customerId;
        BorrowDate = borrowDate;
        DueDate = dueDate;
        _borrowedBooks = borrowedBooks;
    }

    public static Loan Create(
        int customerId,
        DateTime dueDate,
        List<Book> borrowedBooks)
    {
        return new Loan(
            s_identity++,
            customerId,
            DateTime.UtcNow,
            dueDate,
            borrowedBooks);
    }
}
