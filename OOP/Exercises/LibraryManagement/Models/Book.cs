namespace LibraryManagement.Models;

public class Book
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public string Title { get; private set; } = default!;

    public string Author { get; private set; } = default!;

    public int StockQuantity { get; private set; }

    public decimal Price { get; private set; }

    private Book(
        int id,
        string title,
        string author,
        int stockQuantity,
        decimal price)
    {
        Id = id;
        Title = title;
        Author = author;
        StockQuantity = stockQuantity;
        Price = price;
    }

    public static Book Create(
        string title,
        string author,
        int stockQuantity,
        decimal price)
    {
        if (string.IsNullOrEmpty(title))
            throw new Exception("Title is required.");

        if (string.IsNullOrEmpty(author))
            throw new Exception("Author is required.");

        if (stockQuantity <= 0)
            throw new Exception("Stock must be larger than 0.");

        if (price <= 0)
            throw new Exception("Price must be larger than 0.");

        return new Book(
            s_identity++,
            title,
            author,
            stockQuantity,
            price);
    }

    public void UpdateStockQuantity(int stockQuantity)
    {
        if (stockQuantity < 0)
            throw new Exception($"The book with id = {Id} has only {StockQuantity} left.");

        StockQuantity = stockQuantity;
    }

    public override string ToString()
    {
        return $"Id = {Id} | Title = {Title} | Author = {Author}"
            + $" | StockQuantity = {StockQuantity} | Price = {Price}";
    }
}
