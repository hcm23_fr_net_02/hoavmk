using LibraryManagement.Interfaces.Repositories;

namespace LibraryManagement.Models;

public class Library : IBookRepository
{
    private readonly List<Book> _books = new()
    {
        Book.Create("C# programming", "Mr. A", 10, 49),
        Book.Create("Book 1", "Mr. B", 5, 20),
        Book.Create("Book 2", "Mr. C", 20, 99),
        Book.Create("Book 3", "Mr. D", 1, 69),
        Book.Create("Book 4", "Mr. E", 2, 49),
        Book.Create("Book 5", "Mr. F", 12, 30),
        Book.Create("Book 6", "Mr. G", 9, 59),
        Book.Create("Book 7", "Mr. H", 7, 49),
    };

    public void AddBook(Book book)
    {
        _books.Add(book);
    }

    public Book? FindBookById(int id)
    {
        return _books.Find(x => x.Id == id);
    }

    public void DeleteBook(Book book)
    {
        _books.Remove(book);
    }

    public List<Book> GetBooks()
    {
        return _books.ToList();
    }
}
