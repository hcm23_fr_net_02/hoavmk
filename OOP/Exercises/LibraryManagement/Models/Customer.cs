namespace LibraryManagement.Models;

public class Customer
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public string Name { get; private set; }

    public string Address { get; private set; }

    private Customer(int id, string name, string address)
    {
        Id = id;
        Name = name;
        Address = address;
    }

    public static Customer Create(string name, string address)
    {
        if (string.IsNullOrEmpty(name))
            throw new Exception("Name is required.");

        if (string.IsNullOrEmpty(address))
            throw new Exception("Address is required.");

        return new Customer(s_identity++, name, address);
    }
}
