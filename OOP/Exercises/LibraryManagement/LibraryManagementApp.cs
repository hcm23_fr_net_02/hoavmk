using LibraryManagement.Interfaces.Services;
using LibraryManagement.Models;

namespace LibraryManagement;

public class LibraryManagementApp
{
    private readonly IBookService _bookService;
    private readonly ICustomerService _customerService;
    private readonly ILoanService _loanService;

    public LibraryManagementApp(
        IBookService bookService,
        ICustomerService customerService,
        ILoanService loanService)
    {
        _bookService = bookService;
        _customerService = customerService;
        _loanService = loanService;
    }

    public void Run()
    {
        while (true)
        {
            Console.WriteLine("****************Library****************");
            Console.WriteLine("1. Add new book");
            Console.WriteLine("2. Delete book");
            Console.WriteLine("3. Show books");
            Console.WriteLine("4. Borrow book");
            Console.WriteLine("5. Show loan books");
            Console.WriteLine("6. Exit");
            Console.Write("Select an option: ");

            var choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Console.Clear();
                    AddBook();
                    break;

                case "2":
                    Console.Clear();
                    DeleteBook();
                    break;

                case "3":
                    Console.Clear();
                    ShowBooks();
                    break;

                case "4":
                    Console.Clear();
                    BorrowBook();
                    break;

                case "5":
                    Console.Clear();
                    ShowLoans();
                    break;

                case "6":
                    return;
                default:
                    Console.Clear();
                    Console.WriteLine("Invalid choice!");
                    break;
            }
        }
    }

    private void AddBook()
    {
        var title = EnterTextField("book's title", "Book's title is required.");
        var author = EnterTextField("book's author", "Book's author is required.");
        var stockQuantity = EnterStockQuantity();
        var price = EnterBookPrice();

        try
        {
            var book = Book.Create(title, author, stockQuantity, price);

            _bookService.AddBook(book);
            Console.WriteLine("Added new book!");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private void ShowBooks()
    {
        var books = _bookService.GetBooks();

        Console.WriteLine("***********Books***********");

        if (books.Count is 0)
        {
            Console.WriteLine("There is no books.");

            return;
        }

        foreach (var book in books)
        {
            Console.WriteLine($" - {book}");
        }
    }

    private void FindBook()
    {
        int id = EnterNumberField("book's id");
        var book = _bookService.GetBookById(id);

        if (book is null)
        {
            Console.WriteLine($"The book with id = {id} is not found.");

            return;
        }

        Console.WriteLine(book);
    }

    private void DeleteBook()
    {
        int id = EnterNumberField("book's id");

        try
        {
            _bookService.DeleteBook(id);
            Console.WriteLine($"Deleted the book with id = {id}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private void BorrowBook()
    {
        int customerId = EnterNumberField("customer's id");
        var customer = _customerService.GetCustomerById(customerId);

        if (customer is null)
        {
            Console.WriteLine($"Customer with id = {customerId} is not found.");

            return;
        }

        var bookIdsInput = EnterTextField("book ids (separated by ' ')", "Book ids are required.");
        var bookIds = bookIdsInput.Split(" ");
        var ids = new int[bookIds.Length];
        var index = 0;

        foreach (var id in bookIds)
        {
            if (int.TryParse(id, out var res))
            {
                ids[index++] = res;
            }
            else
            {
                Console.WriteLine("Invalid book's id.");

                return;
            }
        }

        if (ContainsDuplicate(ids))
        {
            Console.WriteLine("Error: each book's id must be unique.");

            return;
        }

        var books = new List<Book>();

        foreach (var id in ids)
        {
            var book = _bookService.GetBookById(id);

            if (book is null)
            {
                Console.WriteLine($"The book with id = {id} is not found.");

                return;
            }

            books.Add(book);
        }

        var loan = Loan.Create(customerId, DateTime.UtcNow.AddDays(7), books);

        _loanService.BorrowBooks(loan);

        Console.WriteLine("Created new loan.");
    }

    private void ShowLoans()
    {
        var loans = _loanService.GetLoans();

        if (loans.Count is 0)
        {
            Console.WriteLine("There is no loans.");

            return;
        }

        foreach (var loan in loans)
        {
            Console.WriteLine($"Id = {loan.Id} | Customer Id = {loan.CustomerId}"
                + $" | Borrow Date = {loan.BorrowDate} | Due Date = {loan.DueDate}");
            
            Console.WriteLine("     Borrowed books:");

            foreach (var book in loan.BorrowedBooks)
            {
                Console.WriteLine($"         - {book}");
            }
        }
    }

    private string EnterTextField(string fieldName, string alertMessage)
    {
        string? input;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine($"{alertMessage}");
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }

    private int EnterNumberField(string fieldName)
    {
        string? input;
        int number;
        bool parsedNumber;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();
            parsedNumber = int.TryParse(input, out number);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("{filed Name} is required.");
            }
            else if (!parsedNumber)
            {
                Console.WriteLine("Not a number.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedNumber);

        return number;
    }

    private int EnterStockQuantity()
    {
        string? input;
        int quantity;
        bool parsedQuantity;

        do
        {
            Console.Write($"Enter stock quantity: ");
            input = Console.ReadLine();
            parsedQuantity = int.TryParse(input, out quantity);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Stock quantity is required.");
            }
            else if (!parsedQuantity)
            {
                Console.WriteLine("Invalid stock quantity.");
            }
            else if (quantity < 0)
            {
                Console.WriteLine("Stock quantity must be larger than or equal 0.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedQuantity || quantity < 0);

        return quantity;
    }

    private decimal EnterBookPrice()
    {
        string? input;
        decimal price;
        bool parsedPrice;

        do
        {
            Console.Write("Enter room's price: ");
            input = Console.ReadLine();
            parsedPrice = decimal.TryParse(input, out price);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("Price is required.");
            }
            else if (!parsedPrice)
            {
                Console.WriteLine("Invalid price.");
            }
            else if (price <= 0)
            {
                Console.WriteLine("Price must be larger than 0.");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedPrice || price <= 0);

        return price;
    }

    private bool ContainsDuplicate(int[] array)
    {
        var hs = new HashSet<int>();

        foreach (var item in array)
        {
            if (hs.Contains(item))
                return true;

            hs.Add(item);
        }

        return false;
    }
}
