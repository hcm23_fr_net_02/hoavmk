﻿using LibraryManagement;
using LibraryManagement.Models;
using LibraryManagement.Repositories;
using LibraryManagement.Services;

var library = new Library();
var customerRepository = new CustomerRepository();
var loanRepository = new LoanRepository();
var bookService = new BookService(library);
var customerService = new CustomerService(customerRepository);
var loanService = new LoanService(loanRepository);
var libraryManagementApp = new LibraryManagementApp(
    bookService,
    customerService,
    loanService);

libraryManagementApp.Run();
