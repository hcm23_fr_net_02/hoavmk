using LibraryManagement.Interfaces.Repositories;
using LibraryManagement.Models;

namespace LibraryManagement.Repositories;

public class CustomerRepository : ICustomerRepository
{
    private readonly List<Customer> _customers = new()
    {
        Customer.Create("Customer 1", "HCM"),
        Customer.Create("Customer 2", "HCM"),
        Customer.Create("Customer 3", "Dong Nai"),
        Customer.Create("Customer 4", "HCM"),
        Customer.Create("Customer 5", "Da Nang"),
        Customer.Create("Customer 6", "HCM"),
        Customer.Create("Customer 7", "HN"),
    };

    public Customer? GetCustomerById(int id)
    {
        return _customers.Find(x => x.Id == id);
    }

    public List<Customer> GetCustomers()
    {
        return _customers.ToList();
    }
}
