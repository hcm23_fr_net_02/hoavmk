using LibraryManagement.Interfaces.Repositories;
using LibraryManagement.Models;

namespace LibraryManagement.Repositories;

public class LoanRepository : ILoanRepository
{
    private readonly List<Loan> _loans = new();

    public void BorrowBooks(Loan loan)
    {
        foreach (var book in loan.BorrowedBooks)
        {
            book.UpdateStockQuantity(book.StockQuantity - 1);
        }

        _loans.Add(loan);
    }

    public List<Loan> GetLoans()
    {
        return _loans.ToList();
    }
}
