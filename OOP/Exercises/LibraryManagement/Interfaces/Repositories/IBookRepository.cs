using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Repositories;

public interface IBookRepository
{
    void AddBook(Book book);
    void DeleteBook(Book book);
    Book? FindBookById(int id);
    List<Book> GetBooks();
}
