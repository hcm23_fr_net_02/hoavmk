using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Repositories;

public interface ICustomerRepository
{
    Customer? GetCustomerById(int id);
    List<Customer> GetCustomers();
}
