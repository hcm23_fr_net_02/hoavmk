using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Repositories;

public interface ILoanRepository
{
    List<Loan> GetLoans();
    void BorrowBooks(Loan loan);
}
