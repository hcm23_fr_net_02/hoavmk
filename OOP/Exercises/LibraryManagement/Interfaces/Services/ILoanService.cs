using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Services;

public interface ILoanService
{
    List<Loan> GetLoans();
    void BorrowBooks(Loan loan);
}
