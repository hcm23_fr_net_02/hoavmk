using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Services;

public interface IBookService
{
    List<Book> GetBooks();
    void AddBook(Book book);
    void DeleteBook(int bookId);
    Book? GetBookById(int bookId);
}
