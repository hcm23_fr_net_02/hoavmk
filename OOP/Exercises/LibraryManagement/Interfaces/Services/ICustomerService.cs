using LibraryManagement.Models;

namespace LibraryManagement.Interfaces.Services;

public interface ICustomerService
{
    Customer? GetCustomerById(int customerId);
    List<Customer> GetCustomers();
}
