using LibraryManagement.Interfaces.Services;
using LibraryManagement.Interfaces.Repositories;
using LibraryManagement.Models;

namespace LibraryManagement.Services;

public class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _customerRepository;

    public CustomerService(ICustomerRepository customerRepository)
    {
        _customerRepository = customerRepository;
    }

    public Customer? GetCustomerById(int customerId)
    {
        return _customerRepository.GetCustomerById(customerId);
    }

    public List<Customer> GetCustomers()
    {
        return _customerRepository.GetCustomers();
    }
}
