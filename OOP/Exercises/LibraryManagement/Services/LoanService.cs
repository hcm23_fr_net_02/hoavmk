using LibraryManagement.Interfaces.Services;
using LibraryManagement.Interfaces.Repositories;
using LibraryManagement.Models;

namespace LibraryManagement.Services;

public class LoanService : ILoanService
{
    private readonly ILoanRepository _loanRepository;

    public LoanService(ILoanRepository loanRepository)
    {
        _loanRepository = loanRepository;
    }

    public void BorrowBooks(Loan loan)
    {
        _loanRepository.BorrowBooks(loan);
    }

    public List<Loan> GetLoans()
    {
        return _loanRepository.GetLoans();
    }
}
