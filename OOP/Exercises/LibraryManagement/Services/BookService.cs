using LibraryManagement.Interfaces.Services;
using LibraryManagement.Interfaces.Repositories;
using LibraryManagement.Models;

namespace LibraryManagement.Services;

public class BookService : IBookService
{
    private readonly IBookRepository _bookRepository;

    public BookService(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
    }

    public void AddBook(Book book)
    {
        _bookRepository.AddBook(book);
    }

    public void DeleteBook(int bookId)
    {
        var book = _bookRepository.FindBookById(bookId);

        if (book is null)
        {
            Console.WriteLine($"The book with id = {bookId} is not found");
            
            return;
        }

        _bookRepository.DeleteBook(book);
    }

    public Book? GetBookById(int bookId)
    {
        return _bookRepository.FindBookById(bookId);
    }

    public List<Book> GetBooks()
    {
        return _bookRepository.GetBooks();
    }
}
