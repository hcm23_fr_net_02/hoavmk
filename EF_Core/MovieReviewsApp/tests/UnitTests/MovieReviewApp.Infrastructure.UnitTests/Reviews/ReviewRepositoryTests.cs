﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Infrastructure.Persistence;
using MovieReviewApp.Infrastructure.Repositories;
using MovieReviewApp.UnitTesting.Shared.Builders;
using System.Data.Common;

namespace MovieReviewApp.Infrastructure.UnitTests.Reviews;

public class ReviewRepositoryTests : IDisposable
{
    private readonly DbConnection _connection;
    private readonly DbContextOptions<AppDbContext> _contextOptions;
    private Movie _movie1 = new("Movie 1", 2019, "USA", "Mr. A");
    private Review _review1 = new("Test Title 1", "Test Content 1", 7, 1);
    private Review _review2 = new("Test Title 2", "Test Content 2", 5, 1);

    public ReviewRepositoryTests()
    {
        _connection = new SqliteConnection("Filename=:memory:");
        _connection.Open();

        _contextOptions = new DbContextOptionsBuilder<AppDbContext>()
            .UseSqlite(_connection)
            .Options;

        using var context = new AppDbContext(_contextOptions);

        if (context.Database.EnsureCreated())
        {
            context.Movies.Add(_movie1);
            context.Reviews.AddRange(_review1, _review2);
            context.SaveChanges();
        }
    }

    AppDbContext CreateContext() => new(_contextOptions);

    [Fact]
    public async Task GetByMovieId_WhenMovieExists_ShouldReturnReviews()
    {
        // Arrange
        using var context = CreateContext();

        var movieId = 1;
        var reviewRepository = new ReviewRepository(context);

        // Act
        var reviews = await reviewRepository.GetByMovieIdAsync(movieId);

        // Assert
        reviews.Count.Should().BeGreaterThan(0);
    }

    [Fact]
    public async Task GetByMovieId_WhenMovieDoesNotExist_ShouldReturnEmptyList()
    {
        // Arrange
        using var context = CreateContext();

        var movieId = 0;
        var reviewRepository = new ReviewRepository(context);

        // Act
        var reviews = await reviewRepository.GetByMovieIdAsync(movieId);

        // Assert
        reviews.Count.Should().Be(0);
    }

    [Fact]
    public async Task AddReview_ShouldAdd()
    {
        // Arrange
        using var context = CreateContext();

        var review = new ReviewBuiler().Build();
        var reviewRepository = new ReviewRepository(context);

        // Act
        var addedReview = await reviewRepository.AddAsync(review);

        // Assert
        addedReview.Should().NotBeNull();
        addedReview.Title.Should().Be(review.Title);
        addedReview.Content.Should().Be(review.Content);
        addedReview.Rating.Should().Be(review.Rating);
        addedReview.MovieId.Should().Be(review.MovieId);
    }

    [Fact]
    public async Task GetById_WhenReviewExists_ShouldReturnReview()
    {
        // Arrange
        using var context = CreateContext();

        var reviewId = 1;
        var reviewRepository = new ReviewRepository(context);

        // Act
        var review = await reviewRepository.GetByIdAsync(reviewId);

        // Assert
        review.Should().NotBeNull();
    }

    [Fact]
    public async Task GetById_WhenReviewDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        using var context = CreateContext();

        var reviewId = 0;
        var reviewRepository = new ReviewRepository(context);

        // Act
        var review = await reviewRepository.GetByIdAsync(reviewId);

        // Assert
        review.Should().BeNull();
    }

    public void Dispose() => _connection.Dispose();
}
