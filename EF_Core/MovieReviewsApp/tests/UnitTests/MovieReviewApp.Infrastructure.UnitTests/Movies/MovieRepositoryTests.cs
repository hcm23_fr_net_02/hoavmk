﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Infrastructure.Persistence;
using MovieReviewApp.Infrastructure.Repositories;
using MovieReviewApp.UnitTesting.Shared.Builders;
using System.Data.Common;

namespace MovieReviewApp.Infrastructure.UnitTests.Movies;

public class MovieRepositoryTests : IDisposable
{
    private readonly DbConnection _connection;
    private readonly DbContextOptions<AppDbContext> _contextOptions;
    private Movie _movie1 = new("Movie 1", 2019, "USA", "Mr. A");
    private Movie _movie2 = new("Movie 2", 2020, "UK", "Mr. B");
    private Movie _movie3 = new("Movie 3", 2020, "India", "Mr. C");

    public MovieRepositoryTests()
    {
        _connection = new SqliteConnection("Filename=:memory:");
        _connection.Open();

        _contextOptions = new DbContextOptionsBuilder<AppDbContext>()
            .UseSqlite(_connection)
            .Options;

        using var context = new AppDbContext(_contextOptions);

        if (context.Database.EnsureCreated())
        {
            context.Movies.AddRange(_movie1, _movie2, _movie3);
            context.SaveChanges();
        }
    }

    AppDbContext CreateContext() => new(_contextOptions);

    [Fact]
    public async Task GetMovieListPage_ShouldReturnMovieListPage()
    {
        // Arrange
        using var context = CreateContext();

        var page = 1;
        var pageSize = 2;
        var movieRepository = new MovieRepository(context);

        // Act
        PageResult<Movie> result = await movieRepository.GetListPageAsync(page, pageSize);

        // Assert
        result.TotalItems.Should().Be(context.Movies.Count());
        result.Items.Count.Should().BeLessThanOrEqualTo(pageSize);
        result.Page.Should().Be(page);
        result.PageSize.Should().Be(pageSize);
    }

    [Fact]
    public async Task GetMovieById_WhenMovieExists_ShouldReturnMovie()
    {
        // Arrange
        using var context = CreateContext();

        var movieId = 1;
        var movieRepository = new MovieRepository(context);

        // Act
        var movie = await movieRepository.GetByIdAsync(movieId);

        // Assert
        movie.Should().NotBeNull();
        movie!.Id.Should().Be(movieId);
        movie.Name.Should().Be(_movie1.Name);
        movie.ReleaseYear.Should().Be(_movie1.ReleaseYear);
        movie.Director.Should().Be(_movie1.Director);
        movie.Country.Should().Be(_movie1.Country);
    }

    [Fact]
    public async Task GetMovieById_WhenMovieDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        using var context = CreateContext();

        var movieId = 0;
        var movieRepository = new MovieRepository(context);

        // Act
        var movie = await movieRepository.GetByIdAsync(movieId);

        // Assert
        movie.Should().BeNull();
    }

    [Fact]
    public async Task AddMovie_ShouldAdd()
    {
        // Arrange
        using var context = CreateContext();

        var movieRepository = new MovieRepository(context);
        var movie = new MovieBuilder().Build();

        // Act
        var addedMovie = await movieRepository.AddAsync(movie);

        // Assert
        addedMovie.Should().NotBeNull();
        addedMovie.Name.Should().Be(movie.Name);
        addedMovie.ReleaseYear.Should().Be(movie.ReleaseYear);
        addedMovie.Country.Should().Be(movie.Country);
        addedMovie.Director.Should().Be(movie.Director);
    }

    [Fact]
    public async Task DeleteMovie_ShouldDelete()
    {
        // Arrange
        using var context = CreateContext();

        var movieId = 3;
        var movieRepository = new MovieRepository(context);
        var movie = await movieRepository.GetByIdAsync(movieId);

        // Act
        movieRepository.Delete(movie!);

        // Assert
        context.Entry(movie!).State.Should().Be(EntityState.Deleted);
    }

    public void Dispose() => _connection.Dispose();
}
