﻿using MovieReviewApp.Domain.Common.Exceptions.Reviews;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Domain.UnitTests.Reviews;

public class ReviewTests
{
    [Theory]
    [InlineData(Review.MinRating - 1)]
    [InlineData(Review.MaxRating + 1)]
    public void CreateReview_WhenRatingIsInvalid_ShouldThrowException(ushort rating)
    {
        // Arrange
        // Act
        var act = () => new Review(
            ReviewTestData.TestTitle,
            ReviewTestData.TestContent,
            rating,
            ReviewTestData.TestMovieId);

        // Assert
        act.Should().Throw<InvalidRatingRangeException>();
    }

    [Fact]
    public void CreateReview_WhenValid_ShouldCreate()
    {
        // Arrange
        // Act
        var review = new Review(
            ReviewTestData.TestTitle,
            ReviewTestData.TestContent,
            ReviewTestData.TestRating,
            ReviewTestData.TestMovieId);

        // Assert

        review.Title.Should().Be(ReviewTestData.TestTitle);
        review.Content.Should().Be(ReviewTestData.TestContent);
        review.Rating.Should().Be(ReviewTestData.TestRating);
        review.MovieId.Should().Be(ReviewTestData.TestMovieId);
    }
}
