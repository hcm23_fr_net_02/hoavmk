﻿using MovieReviewApp.Domain.Entities;
using MovieReviewApp.UnitTesting.Shared.Builders;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Domain.UnitTests.Movies;

public class MovieTests
{
    [Fact]
    public void CreateMovie_WhenValid_ShouldCreate()
    {
        // Arrange
        // Act
        var movie = new Movie(
            name: MovieTestData.TestMovieName,
            releaseYear: MovieTestData.TestReleaseYear,
            director: MovieTestData.TestDirectorName,
            country: MovieTestData.TestCountryName);

        // Assert
        movie.Name.Should().Be(MovieTestData.TestMovieName);
        movie.ReleaseYear.Should().Be(MovieTestData.TestReleaseYear);
        movie.Director.Should().Be(MovieTestData.TestDirectorName);
        movie.Country.Should().Be(MovieTestData.TestCountryName);
    }

    [Fact]
    public void UpdateMovieName_WhenValid_ShouldUpdate()
    {
        // Arrange
        var name = "updated";
        var movie = new MovieBuilder().Build();

        // Act
        movie.UpdateName(name);

        // Assert
        movie.Name.Should().Be(name);
    }

    [Fact]
    public void UpdateMovieReleaseYear_WhenValid_ShouldUpdate()
    {
        // Arrange
        var releaseYear = 2023;
        var movie = new MovieBuilder().Build();

        // Act
        movie.UpdateReleaseYear(releaseYear);

        // Assert
        movie.ReleaseYear.Should().Be(releaseYear);
    }

    [Fact]
    public void UpdateMovieDirector_WhenValid_ShouldUpdate()
    {
        // Arrange
        var director = "updated";
        var movie = new MovieBuilder().Build();

        // Act
        movie.UpdateDirector(director);

        // Assert
        movie.Director.Should().Be(director);
    }

    [Fact]
    public void UpdateMovieCountry_WhenValid_ShouldUpdate()
    {
        // Arrange
        var country = "updated";
        var movie = new MovieBuilder().Build();

        // Act
        movie.UpdateCountry(country);

        // Assert
        movie.Country.Should().Be(country);
    }
}
