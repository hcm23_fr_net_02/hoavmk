namespace MovieReviewApp.UnitTesting.Shared.TestData;

public static class MovieTestData
{
    public const int TestMovieId = 1;

    public const string TestMovieName = "Test Movie Name";

    public const int TestReleaseYear = 2000;

    public const string TestDirectorName = "Test Director Name";

    public const string TestCountryName = "Test Country Name";
}
