namespace MovieReviewApp.UnitTesting.Shared.TestData;

public static class ReviewTestData
{
    public const string TestTitle = "Test Review Title";

    public const string TestContent = "Test Review Content";

    public const ushort TestRating = 5;

    public const int TestMovieId = 1;
}
