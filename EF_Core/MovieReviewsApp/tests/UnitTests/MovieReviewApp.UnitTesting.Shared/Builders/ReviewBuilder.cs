using MovieReviewApp.Domain.Entities;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.UnitTesting.Shared.Builders;

public class ReviewBuiler
{
    private Review _review;

    public ReviewBuiler()
    {
        WithDefaultValues();
        _review = Build();
    }

    public ReviewBuiler WithDefaultValues()
    {
        _review = new Review(
            ReviewTestData.TestTitle,
            ReviewTestData.TestContent,
            ReviewTestData.TestRating,
            ReviewTestData.TestMovieId);

        return this;
    }

    public Review Build()
        => _review;
}
