﻿using MovieReviewApp.Domain.Common.Contracts;

namespace MovieReviewApp.UnitTesting.Shared.Builders;

public class PageResultBuilder<T> where T : class
{
    private readonly PageResult<T> _pageResult;

    public int Page = 1;

    public int PageSize = 5;

    public PageResultBuilder()
    {
        _pageResult = new PageResult<T>();
    }

    public PageResultBuilder<T> WithEmptyItems()
    {
        _pageResult.Items = new List<T>();
        _pageResult.TotalPages = 0;
        _pageResult.TotalItems = 0;

        return this;
    }

    public PageResultBuilder<T> WithPage(int page)
    {
        _pageResult.Page = page;

        return this;
    }

    public PageResultBuilder<T> WithPageSize(int pageSize)
    {
        _pageResult.PageSize = pageSize;
        _pageResult.TotalPages = CalculateTotalPages(_pageResult.TotalItems, PageSize);

        return this;
    }

    public PageResultBuilder<T> WithItems(List<T> items)
    {
        _pageResult.Items = items.Skip((Page - 1) * PageSize)
            .Take(PageSize)
            .ToList();

        _pageResult.TotalItems = items.Count;
        _pageResult.TotalPages = CalculateTotalPages(items.Count, PageSize);

        return this;
    }

    private static int CalculateTotalPages(int totalItems, int pageSize)
    {
        var totalPages = totalItems / pageSize;

        if (totalPages % pageSize is not 0)
            totalPages++;

        return totalPages;
    }

    public PageResult<T> Build()
        => _pageResult;
}
