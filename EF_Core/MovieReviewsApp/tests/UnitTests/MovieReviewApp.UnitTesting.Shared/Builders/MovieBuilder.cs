using MovieReviewApp.Domain.Entities;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.UnitTesting.Shared.Builders;

public class MovieBuilder
{
    private Movie _movie;

    public MovieBuilder()
    {
        WithDefaultValues();
        _movie = Build();
    }

    public MovieBuilder WithDefaultValues()
    {
        _movie = new Movie(
            MovieTestData.TestMovieName,
            MovieTestData.TestReleaseYear,
            MovieTestData.TestDirectorName,
            MovieTestData.TestCountryName);

        return this;
    }

    public MovieBuilder WithReviews()
    {
        return this;
    }

    public MovieBuilder WithReviews(List<Review> reviews)
    {
        _movie.Reviews.AddRange(reviews);

        return this;
    }

    public Movie Build()
        => _movie;
}
