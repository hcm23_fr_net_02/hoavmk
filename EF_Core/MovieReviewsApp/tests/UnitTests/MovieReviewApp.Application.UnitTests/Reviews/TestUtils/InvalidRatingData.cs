﻿using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Application.UnitTests.Reviews.TestUtils;

public class InvalidRatingData : TheoryData<ushort>
{
    public InvalidRatingData()
    {
        Add(Review.MinRating - 1);
        Add(Review.MaxRating + 1);
    }
}
