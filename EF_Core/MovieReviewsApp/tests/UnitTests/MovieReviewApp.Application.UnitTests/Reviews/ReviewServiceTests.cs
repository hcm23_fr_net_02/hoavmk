﻿using MovieReviewApp.Application.Common.Errors;
using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.Application.Services;
using MovieReviewApp.Application.UnitTests.Reviews.TestUtils;
using MovieReviewApp.Application.UnitTests.TestUtils;
using MovieReviewApp.Domain.Common.Exceptions.Reviews;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.UnitTesting.Shared.Builders;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Application.UnitTests.Reviews;

public class ReviewServiceTests
{
    private readonly Mock<IReviewRepository> _mockReviewRepository;
    private readonly Mock<IMovieRepository> _mockMovieRepository;
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private readonly IReviewService _reviewService;

    public ReviewServiceTests()
    {
        _mockReviewRepository = new Mock<IReviewRepository>();
        _mockMovieRepository = new Mock<IMovieRepository>();
        _mockUnitOfWork = new Mock<IUnitOfWork>();
        _mockUnitOfWork.Setup(m => m.Reviews)
            .Returns(_mockReviewRepository.Object);

        _mockUnitOfWork.Setup(m => m.Movies)
            .Returns(_mockMovieRepository.Object);

        _reviewService = new ReviewService(_mockUnitOfWork.Object);
    }

    [Fact]
    public async Task CreateReview_WhenMovieNotFound_ShouldThrowException()
    {
        // Arrange
        var request = CreateReviewRequestUtils.CreateWithDefaultValues();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(ReviewTestData.TestMovieId))
            .ReturnsAsync((Movie?)null);

        // Act
        Func<Task> act = () => _reviewService.CreateReviewAsync(request);

        // Assert
        await act.Should().ThrowAsync<MovieNotFoundException>();
    }

    [Theory]
    [ClassData(typeof(InvalidRatingData))]
    public async Task CreateReview_WhenRatingInvalid_ShouldThrowException(ushort rating)
    {
        // Arrange
        var request = CreateReviewRequestUtils.Create(
            ReviewTestData.TestMovieId,
            ReviewTestData.TestTitle,
            ReviewTestData.TestContent,
            rating);

        var movie = new MovieBuilder().Build();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(ReviewTestData.TestMovieId))
            .ReturnsAsync(movie);

        // Act
        Func<Task> act = () => _reviewService.CreateReviewAsync(request);

        // Assert
        await act.Should().ThrowAsync<InvalidRatingRangeException>();
    }

    [Fact]
    public async Task CreateReview_WhenValid_ShouldCreate()
    {
        // Arrange
        var request = CreateReviewRequestUtils.CreateWithDefaultValues();
        var movie = new MovieBuilder().Build();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(ReviewTestData.TestMovieId))
            .ReturnsAsync(movie);

        // Act
        await _reviewService.CreateReviewAsync(request);

        // Assert
        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
    }
}
