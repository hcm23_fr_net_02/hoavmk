﻿using MovieReviewApp.Contracts.Movies.Responses;
using MovieReviewApp.Contracts.Reviews.Responses;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Application.UnitTests.TestUtils;

public static class CreateMovieResponseUtils
{
    public static MovieResponse CreateWithDefaultValues()
        => new(
            MovieTestData.TestMovieId,
            MovieTestData.TestMovieName,
            MovieTestData.TestReleaseYear,
            MovieTestData.TestDirectorName,
            MovieTestData.TestCountryName,
            new List<ReviewResponse>());
}
