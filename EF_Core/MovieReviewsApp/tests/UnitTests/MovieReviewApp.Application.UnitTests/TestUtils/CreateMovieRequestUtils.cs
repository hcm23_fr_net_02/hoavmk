using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Application.UnitTests.TestUtils;

public static class CreateMovieRequestUtils
{
    public static CreateMovieRequest CreateWithDefaultValues()
        => new CreateMovieRequest(
            MovieTestData.TestMovieName,
            MovieTestData.TestReleaseYear,
            MovieTestData.TestDirectorName,
            MovieTestData.TestCountryName);
}
