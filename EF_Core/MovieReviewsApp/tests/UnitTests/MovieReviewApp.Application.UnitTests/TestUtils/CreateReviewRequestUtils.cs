﻿using MovieReviewApp.Contracts.Reviews.Requests;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Application.UnitTests.TestUtils;

public static class CreateReviewRequestUtils
{
    public static CreateReviewRequest Create(
        int movieId,
        string title,
        string content,
        ushort rating)
        => new(movieId, title, content, rating);

    public static CreateReviewRequest CreateWithDefaultValues()
        => new(
            ReviewTestData.TestMovieId,
            ReviewTestData.TestTitle,
            ReviewTestData.TestContent,
            ReviewTestData.TestRating);
}
