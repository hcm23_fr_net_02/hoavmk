using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.UnitTesting.Shared.TestData;

namespace MovieReviewApp.Application.UnitTests.TestUtils;

public static class CreateUpdateMovieRequestUtils
{
    public const string MovieNameForUpdate = "Updated";

    public const int ReleaseYearForUpdate = 2020;

    public const string DirectorNameForUpdate = "Updated";

    public const string CountryNameForUpdate = "Updated";

    public static UpdateMovieRequest CreateWithDefaultValues()
        => new(
            MovieTestData.TestMovieId,
            MovieNameForUpdate,
            ReleaseYearForUpdate,
            DirectorNameForUpdate,
            CountryNameForUpdate);
}
