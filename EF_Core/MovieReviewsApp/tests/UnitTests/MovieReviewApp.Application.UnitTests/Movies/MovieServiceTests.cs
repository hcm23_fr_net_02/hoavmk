using MapsterMapper;
using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.Application.Services;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.Application.UnitTests.TestUtils;
using MovieReviewApp.UnitTesting.Shared.Builders;
using MovieReviewApp.Application.Common.Errors;
using MovieReviewApp.UnitTesting.Shared.TestData;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Contracts.Movies.Responses;

namespace MovieReviewApp.Application.UnitTests.Movies;

public class MovieServiceTests
{
    private readonly Mock<IMovieRepository> _mockMovieRepository;
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private readonly Mock<IMapper> _mockMapper;
    private readonly IMovieService _movieService;

    public MovieServiceTests()
    {
        _mockMovieRepository = new Mock<IMovieRepository>();
        _mockUnitOfWork = new Mock<IUnitOfWork>();
        _mockMapper = new Mock<IMapper>();
        _mockUnitOfWork.Setup(m => m.Movies)
            .Returns(_mockMovieRepository.Object);

        _movieService = new MovieService(_mockUnitOfWork.Object, _mockMapper.Object);
    }

    [Fact]
    public async Task CreateMovie_WhenValid_ShouldCreate()
    {
        // Arrange
        var request = CreateMovieRequestUtils.CreateWithDefaultValues();
        var movie = new MovieBuilder().Build();

        _mockMapper.Setup(m => m.Map<Movie>(request)).Returns(movie);

        // Act
        await _movieService.CreateMovieAsync(request);

        // Assert
        _mockUnitOfWork.Verify(m => m.Movies.AddAsync(movie), Times.Once);
        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
    }

    [Fact]
    public async Task DeleteMovie_WhenMovieNotFound_ShouldThrowException()
    {
        // Arrange
        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync((Movie?)null);

        // Act
        Func<Task> act = () => _movieService.DeleteMovieAsync(MovieTestData.TestMovieId);

        // Assert
        await act.Should().ThrowAsync<MovieNotFoundException>();
    }

    [Fact]
    public async Task DeleteMovie_WhenMovieFound_ShouldDelete()
    {
        // Arrange
        var movie = new MovieBuilder().Build();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync(movie);

        // Act
        await _movieService.DeleteMovieAsync(MovieTestData.TestMovieId);

        // Assert
        _mockUnitOfWork.Verify(m =>
            m.Movies.GetByIdAsync(MovieTestData.TestMovieId),
            Times.Once);

        _mockUnitOfWork.Verify(m => m.Movies.Delete(movie), Times.Once);
        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
    }

    [Fact]
    public async Task UpdateMovie_WhenMovieNotFound_ShouldThrowException()
    {
        // Arrange
        var request = CreateUpdateMovieRequestUtils.CreateWithDefaultValues();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync((Movie?)null);

        // Act
        Func<Task> act = () => _movieService.UpdateMovieAsync(request);

        // Assert
        await act.Should().ThrowAsync<MovieNotFoundException>();
    }

    [Fact]
    public async Task UpdateMovie_WhenMovieFound_ShouldUpdate()
    {
        // Arrange
        var movie = new MovieBuilder().Build();
        var request = CreateUpdateMovieRequestUtils.CreateWithDefaultValues();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync(movie);

        // Act
        await _movieService.UpdateMovieAsync(request);

        // Assert
        _mockUnitOfWork.Verify(m =>
            m.Movies.GetByIdAsync(MovieTestData.TestMovieId),
            Times.Once);

        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Once);
    }

    [Fact]
    public async Task GetMovieWithReviews_WhenMovieFound_ShouldReturn()
    {
        // Arrange
        var review = new ReviewBuiler().Build();
        var movie = new MovieBuilder()
            .WithReviews(new List<Review> { review })
            .Build();

        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync(movie);

        // Act
        MovieResponse result = await _movieService.GetMovieWithReviewsByMovieIdAsync(
            MovieTestData.TestMovieId);

        // Assert
        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Never);
        _mockUnitOfWork.Verify(m => m.Movies.GetByIdAsync(
            MovieTestData.TestMovieId),
            Times.Once);
    }

    [Fact]
    public async Task GetMovieWithReviews_WhenMovieNotFound_ShouldThrowException()
    {
        _mockMovieRepository.Setup(m => m.GetByIdAsync(MovieTestData.TestMovieId))
            .ReturnsAsync((Movie?)null);

        // Act
        Func<Task> act = () => _movieService.GetMovieWithReviewsByMovieIdAsync(
            MovieTestData.TestMovieId);

        // Assert
        await act.Should().ThrowAsync<MovieNotFoundException>();
    }

    [Fact]
    public async Task FilterMovies_ReturnMovieListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        var pageResult = new PageResultBuilder<Movie>()
            .WithPage(page)
            .WithPageSize(pageSize)
            .WithEmptyItems()
            .Build();

        // Act
        PageResult<MovieResponse> result = await _movieService.FilterMoviesAsync(
            new Dictionary<string, string>(),
            page,
            pageSize);

        // Arrange
        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Never);
    }

    [Fact]
    public async Task GetMovieListPage_ReturnMovieListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        var movie = new MovieBuilder().Build();
        var movieResponse = CreateMovieResponseUtils.CreateWithDefaultValues();
        var moviePageResult = new PageResultBuilder<Movie>()
            .WithPage(page)
            .WithPageSize(pageSize)
            .WithItems(new List<Movie>() { movie })
            .Build();

        _mockMovieRepository.Setup(m =>
            m.GetListPageAsync(page, pageSize, null, null, string.Empty))
            .ReturnsAsync(moviePageResult);
               
        // Act
        PageResult<MovieResponse> result = await _movieService.GetMovieListPageAsync(
            page,
            pageSize);

        // Assert
        _mockUnitOfWork.Verify(m =>
            m.Movies.GetListPageAsync(page, pageSize, null, null, string.Empty),
            Times.Once);

        _mockMapper.Verify(m =>
            m.Map<PageResult<MovieResponse>>(moviePageResult),
            Times.Once);

        _mockUnitOfWork.Verify(m => m.SaveChangesAsync(), Times.Never);
    }
}
