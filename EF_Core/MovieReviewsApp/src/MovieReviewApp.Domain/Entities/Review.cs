using System.Text.Json.Serialization;
using MovieReviewApp.Domain.Common.Exceptions.Reviews;

namespace MovieReviewApp.Domain.Entities;

public class Review
{
    public const ushort MinRating = 1;

    public const ushort MaxRating = 10;

    public int Id { get; private set; }

    public string Title { get; private set; } = default!;

    public string Content { get; private set; } = default!;

    public ushort Rating { get; private set; }

    public int MovieId { get; private set; }

    public virtual Movie? Movie { get; private set; }

    private Review()
    {
    }

    [JsonConstructor]
    public Review(
        string title,
        string content,
        ushort rating,
        int movieId)
    {
        ValidateRating(rating);

        Title = title;
        Content = content;
        Rating = rating;
        MovieId = movieId;
    }

    private static void ValidateRating(ushort rating)
    {
        if (rating is < MinRating or > MaxRating)
        {
            throw new InvalidRatingRangeException(
                $"Rating must be between {MinRating} and {MaxRating}.");
        }
    }
}
