using System.Text.Json.Serialization;

namespace MovieReviewApp.Domain.Entities;

public class Movie
{
    public const int MinReleaseYear = 1900;

    public static readonly int MaxReleaseYear = DateTime.UtcNow.Year;

    public int Id { get; init; }

    public string Name { get; private set; } = default!;

    public int ReleaseYear  { get; private set; }

    public string Country { get; private set; } = default!;

    public string Director { get; private set; } = default!;

    public virtual List<Review> Reviews { get; private set; } = new();

    private Movie()
    {
    }

    [JsonConstructor]
    public Movie(
        string name,
        int releaseYear,
        string country,
        string director)
    {
        Name = name;
        ReleaseYear = releaseYear;
        Country = country;
        Director = director;
    }

    public void UpdateName(string name)
        => Name = name;

    public void UpdateReleaseYear(int releaseYear)
        => ReleaseYear = releaseYear;

    public void UpdateCountry(string country)
        => Country = country;

    public void UpdateDirector(string director)
        => Director = director;
}
