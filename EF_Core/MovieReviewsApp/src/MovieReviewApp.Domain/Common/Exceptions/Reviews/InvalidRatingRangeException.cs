namespace MovieReviewApp.Domain.Common.Exceptions.Reviews;

public class InvalidRatingRangeException : Exception
{
    public InvalidRatingRangeException()
    {
    }

    public InvalidRatingRangeException(string? message) : base(message)
    {
    }

    public InvalidRatingRangeException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
