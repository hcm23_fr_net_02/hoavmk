namespace MovieReviewApp.Domain.Common.Contracts;

public class PageResult<T> where T : class
{
    public List<T> Items { get; set; } = new();

    public int TotalPages { get; set; }

    public int Page { get; set; }

    public int PageSize { get; set; }

    public int TotalItems { get; set; }
}