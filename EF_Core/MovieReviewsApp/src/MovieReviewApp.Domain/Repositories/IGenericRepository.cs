namespace MovieReviewApp.Domain.Repositories;

public interface IGenericRepository<TEntity>
    where TEntity : class
{
    Task<TEntity> AddAsync(TEntity entity);
    Task<TEntity?> GetByIdAsync(int id);
}
