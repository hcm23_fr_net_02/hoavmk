namespace MovieReviewApp.Domain.Repositories;

public interface IUnitOfWork
{
    IMovieRepository Movies { get; }
    IReviewRepository Reviews { get; }
    Task SaveChangesAsync();
}
