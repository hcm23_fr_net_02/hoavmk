using System.Linq.Expressions;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Domain.Repositories;

public interface IMovieRepository
    : IGenericRepository<Movie>
{
    void Delete(Movie movie);
    Task<PageResult<Movie>> GetListPageAsync(
        int page ,
        int pageSize,
        Expression<Func<Movie, bool>>? filter = null,
        Func<IQueryable<Movie>, IOrderedQueryable<Movie>>? orderBy = null,
        string includeProperties = "");
}
