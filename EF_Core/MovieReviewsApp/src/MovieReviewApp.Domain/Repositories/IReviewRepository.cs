using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Domain.Repositories;

public interface IReviewRepository
    : IGenericRepository<Review>
{
    Task<List<Review>> GetByMovieIdAsync(int movieId);
}
