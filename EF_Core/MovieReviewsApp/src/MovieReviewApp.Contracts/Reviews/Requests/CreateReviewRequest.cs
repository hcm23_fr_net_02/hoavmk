namespace MovieReviewApp.Contracts.Reviews.Requests;

public record CreateReviewRequest(
    int MovieId,
    string Title,
    string Content,
    ushort Rating);
