namespace MovieReviewApp.Contracts.Reviews.Responses;

public record ReviewResponse(
    int Id,
    string Title,
    string Content,
    int Rating,
    int MovieId);
