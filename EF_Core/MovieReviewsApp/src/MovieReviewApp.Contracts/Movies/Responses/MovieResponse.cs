using MovieReviewApp.Contracts.Reviews.Responses;

namespace MovieReviewApp.Contracts.Movies.Responses;

public record MovieResponse(
    int Id,
    string Name,
    int ReleaseYear,
    string Director,
    string Country,
    List<ReviewResponse> Reviews);
