namespace MovieReviewApp.Contracts.Movies.Requests;

public record UpdateMovieRequest(
    int Id,
    string Name,
    int ReleaseYear,
    string Director,
    string Country);
