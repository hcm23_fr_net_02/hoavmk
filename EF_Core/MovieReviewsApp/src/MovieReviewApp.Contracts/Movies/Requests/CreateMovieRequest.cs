namespace MovieReviewApp.Contracts.Movies.Requests;

public record CreateMovieRequest(
    string Name,
    int ReleaseYear,
    string Director,
    string Country);
