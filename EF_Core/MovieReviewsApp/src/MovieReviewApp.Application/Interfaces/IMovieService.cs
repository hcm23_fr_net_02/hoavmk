using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.Contracts.Movies.Responses;
using MovieReviewApp.Domain.Common.Contracts;

namespace MovieReviewApp.Application.Interfaces;

public interface IMovieService
{
    Task<PageResult<MovieResponse>> GetMovieListPageAsync(int page, int pageSize);
    Task CreateMovieAsync(CreateMovieRequest request);
    Task UpdateMovieAsync(UpdateMovieRequest request);
    Task DeleteMovieAsync(int movieId);
    Task<MovieResponse> GetMovieWithReviewsByMovieIdAsync(int movieId);
    Task<PageResult<MovieResponse>> FilterMoviesAsync(
        Dictionary<string, string> criterias,
        int page,
        int pageSize);
}
