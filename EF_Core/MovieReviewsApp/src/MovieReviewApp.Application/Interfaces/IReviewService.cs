using MovieReviewApp.Contracts.Reviews.Requests;

namespace MovieReviewApp.Application.Interfaces;

public interface IReviewService
{
    Task CreateReviewAsync(CreateReviewRequest request);
}
