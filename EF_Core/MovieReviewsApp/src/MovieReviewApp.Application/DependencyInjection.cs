using Microsoft.Extensions.DependencyInjection;
using MovieReviewApp.Application.Common.Mapping;
using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.Application.Services;

namespace MovieReviewApp.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddMapping();
        services.AddScoped<IMovieService, MovieService>();
        services.AddScoped<IReviewService, ReviewService>();

        return services;
    }
}
