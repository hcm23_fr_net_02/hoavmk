using System.Linq.Expressions;
using MapsterMapper;
using MovieReviewApp.Application.Common.Errors;
using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.Contracts.Movies.Responses;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;

namespace MovieReviewApp.Application.Services;

public class MovieService : IMovieService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public MovieService(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public async Task CreateMovieAsync(CreateMovieRequest request)
    {
        var movie = _mapper.Map<Movie>(request);

        await _unitOfWork.Movies.AddAsync(movie);
        await _unitOfWork.SaveChangesAsync();
    }

    public async Task<PageResult<MovieResponse>> GetMovieListPageAsync(int page, int pageSize)
    {
        PageResult<Movie> moviesPageResult = await _unitOfWork.Movies
            .GetListPageAsync(page, pageSize);

        return _mapper.Map<PageResult<MovieResponse>>(moviesPageResult);
    }

    public async Task UpdateMovieAsync(UpdateMovieRequest request)
    {
        Movie? movie = await _unitOfWork.Movies.GetByIdAsync(request.Id);

        if (movie is null)
            throw new MovieNotFoundException(request.Id);

        movie.UpdateDirector(request.Director);
        movie.UpdateName(request.Name);
        movie.UpdateReleaseYear(request.ReleaseYear);
        movie.UpdateCountry(request.Country);

        await _unitOfWork.SaveChangesAsync();
    }

    public async Task DeleteMovieAsync(int movieId)
    {
        Movie? movie = await _unitOfWork.Movies.GetByIdAsync(movieId);

        if (movie is null)
            throw new MovieNotFoundException(movieId);

        _unitOfWork.Movies.Delete(movie);
        await _unitOfWork.SaveChangesAsync();
    }

    public async Task<MovieResponse> GetMovieWithReviewsByMovieIdAsync(int movieId)
    {
        Movie? movie = await _unitOfWork.Movies.GetByIdAsync(movieId);

        if (movie is null)
            throw new MovieNotFoundException(movieId);

        return _mapper.Map<MovieResponse>(movie);
    }

    public async Task<PageResult<MovieResponse>> FilterMoviesAsync(
        Dictionary<string, string> criterias,
        int page,
        int pageSize)
    {
        var name = nameof(Movie.Name);
        var releaseYear = nameof(Movie.ReleaseYear);
        var country = nameof(Movie.Country);
        var director = nameof(Movie.Director);

        Expression<Func<Movie, bool>> filter = (movie) =>
            (!criterias.ContainsKey(name) ||
                movie.Name.ToLower().Contains(criterias[name].ToLower())) &&
            (!criterias.ContainsKey(releaseYear) ||
                movie.ReleaseYear == Convert.ToInt32(criterias[releaseYear])) &&
            (!criterias.ContainsKey(director) ||
                movie.Director.ToLower().Contains(criterias[director].ToLower())) &&
            (!criterias.ContainsKey(country) ||
                movie.Country.ToLower().Contains(criterias[country].ToLower()));

        var pageResult = await _unitOfWork.Movies.GetListPageAsync(
            page: page,
            pageSize: pageSize,
            filter: filter);

        return _mapper.Map<PageResult<MovieResponse>>(pageResult);
    }
}
