using MovieReviewApp.Application.Common.Errors;
using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.Contracts.Reviews.Requests;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;

namespace MovieReviewApp.Application.Services;

public class ReviewService : IReviewService
{
    private readonly IUnitOfWork _unitOfWork;

    public ReviewService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task CreateReviewAsync(CreateReviewRequest request)
    {
        Movie? movie = await _unitOfWork.Movies.GetByIdAsync(request.MovieId);

        if (movie is null)
        {
            throw new MovieNotFoundException();
        }

        var review = new Review(
            title: request.Title,
            content: request.Content,
            rating: request.Rating,
            movieId: request.MovieId);

        await _unitOfWork.Reviews.AddAsync(review);
        await _unitOfWork.SaveChangesAsync();
    }
}
