using Mapster;
using MovieReviewApp.Contracts.Reviews.Requests;
using MovieReviewApp.Contracts.Reviews.Responses;
using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Application.Common.Mapping;

public class ReviewMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<Review, ReviewResponse>();
        config.NewConfig<CreateReviewRequest, Review>();
    }
}
