using Mapster;
using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.Contracts.Movies.Responses;
using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Application.Common.Mappings;

public class MovieMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<Movie, MovieResponse>()
            .Map(
                dest => dest.Reviews,
                src => src.Reviews);

        config.NewConfig<CreateMovieRequest, Movie>();
    }
}
