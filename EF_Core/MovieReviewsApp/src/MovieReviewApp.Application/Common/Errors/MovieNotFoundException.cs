namespace MovieReviewApp.Application.Common.Errors;

public class MovieNotFoundException : Exception
{
    public MovieNotFoundException()
    {
    }

    public MovieNotFoundException(int movieId)
        : base($"The movie with id = {movieId} was not found.")
    {
    }

    public MovieNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
