using MovieReviewApp.Application.Interfaces;
using MovieReviewApp.ConsoleApp.Constants;
using MovieReviewApp.ConsoleApp.Helpers;
using MovieReviewApp.Contracts.Movies.Requests;
using MovieReviewApp.Contracts.Movies.Responses;
using MovieReviewApp.Contracts.Reviews.Requests;
using MovieReviewApp.Contracts.Reviews.Responses;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.ConsoleApp;

public class MovieReviewsApp
{
    private readonly IMovieService _movieService;
    private readonly IReviewService _reviewService;

    public MovieReviewsApp(
        IMovieService movieService,
        IReviewService reviewService)
    {
        _movieService = movieService;
        _reviewService = reviewService;
    }

    public async Task RunAsync()
    {
        var exit = false;

        while (!exit)
        {
            try
            {
                Console.WriteLine(MenuOptions.AppTitle);
                Console.WriteLine(MenuOptions.ViewMovieListLabel);
                Console.WriteLine(MenuOptions.AddNewMovieLabel);
                Console.WriteLine(MenuOptions.FilterMoviesLabel);
                Console.WriteLine(MenuOptions.ExitLabel);
                Console.Write(MenuOptions.SelectMenuOptionTitle);

                string? choice = Console.ReadLine();

                switch (choice)
                {
                    case MenuOptions.ViewMovieListOption:
                        await ShowMovieListPageAsync();
                        break;

                    case MenuOptions.AddMovieOption:
                        await AddNewMovieAsync();
                        break;

                    case MenuOptions.FilterMoviesOption:
                        await FilterMoviesAsync();
                        break;

                    case MenuOptions.ExitOption:

                        return;

                    default:
                        Console.WriteLine(MenuOptions.InvalidOption);
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    private async Task ShowMovieListPageAsync()
    {
        var page = 1;
        var exit = false;

        while (!exit)
        {
            Console.WriteLine(MenuHelper.GetPageTitle(nameof(Movie), page));

            PageResult<MovieResponse> pageResult = await _movieService.GetMovieListPageAsync(
                page,
                PageOptions.PageSize);

            var printFormat = "{0, -4} {1, -70} {2, -16} {3, -20}";

            Console.WriteLine(
                printFormat,
                nameof(Movie.Id),
                nameof(Movie.Name),
                nameof(Movie.ReleaseYear),
                nameof(Movie.Country));

            Console.WriteLine(MenuOptions.HorizontalLine);

            if (pageResult.Items.Count is 0)
            {
                Console.WriteLine(AlertMessages.EmptyMovieList);
            }
            else
            {
                foreach (MovieResponse movie in pageResult.Items)
                {
                    Console.WriteLine(
                        printFormat,
                        movie.Id,
                        movie.Name,
                        movie.ReleaseYear,
                        movie.Director);
                }
            }
            
            var hasNextPage = pageResult.Page < pageResult.TotalPages;
            var hasPreviousPage = pageResult.Page > 1;

            Console.WriteLine();

            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPageTitle);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPageTitle);
            }

            Console.WriteLine(PageOptions.BackTitle);
            Console.Write(MenuOptions.SelectReviewListOptionTitle(
                hasNextPage,
                hasPreviousPage));

            string? choice = Console.ReadLine();


            if (choice is PageOptions.NextPage1stOption or
                PageOptions.NextPage2ndOption)
            {
                if (hasNextPage)
                    page++;
            }
            else if (choice is PageOptions.PreviousPage1stOption or
                PageOptions.PreviousPage2ndOption)
            {
                if (hasPreviousPage)
                    page--;
            }
            else if (choice is PageOptions.Back1stOption or
                PageOptions.Back2ndOption)
            {
                return;
            }
            else if (int.TryParse(choice, out var movieId))
            {
                await ViewMovieDetailsAsync(movieId);
            }
            else
            {
                Console.WriteLine(MenuOptions.InvalidOption);
            }
        }
    }

    private async Task AddNewMovieAsync()
    {
        Console.WriteLine(MenuOptions.AddNewMovieTitle);

        string name = InputHelper.EnterTextField(
            nameof(Movie.Name),
            AlertMessages.RequiredMovieName);

        int releaseYear = EnterRelaseYear();
        string director = InputHelper.EnterTextField(
            nameof(Movie.Director),
            AlertMessages.RequiredDirector);

        string country = InputHelper.EnterTextField(
            nameof(Movie.Country),
            AlertMessages.RequiredCountry);

        var request = new CreateMovieRequest(
            name,
            releaseYear,
            director,
            country);

        await _movieService.CreateMovieAsync(request);
        Console.WriteLine(AlertMessages.AddedMovie);
    }

    private async Task FilterMoviesAsync()
    {
        Console.WriteLine(MenuOptions.FilterMoviesTitle);
        string? name = InputHelper.EnterOptionalTextField(nameof(Movie.Name));
        int? releaseYear = EnterReleaseYearForFilter();
        string? director = InputHelper.EnterOptionalTextField(nameof(Movie.Director));
        string? country = InputHelper.EnterOptionalTextField(nameof(Movie.Country));

        Dictionary<string, string> filterCriterias = new();

        if (!string.IsNullOrEmpty(name))
        {
            filterCriterias.Add(
                nameof(Movie.Name),
                name);
        }

        if (releaseYear is not null)
        {
            filterCriterias.Add(
                nameof(Movie.ReleaseYear),
                releaseYear.ToString()!);
        }

        if (!string.IsNullOrEmpty(director))
        {
            filterCriterias.Add(
                nameof(Movie.Director),
                director);
        }

        if (!string.IsNullOrEmpty(country))
        {
            filterCriterias.Add(nameof(Movie.Country), country);
        }

        var page = 1;
        var exit = false;

        while (!exit)
        {
            Console.WriteLine(MenuOptions.FilterResultTitle);

            PageResult<MovieResponse> pageResult = await _movieService.FilterMoviesAsync(
                filterCriterias,
                page,
                PageOptions.PageSize);

            bool hasNextPage = pageResult.Page < pageResult.TotalPages;
            bool hasPreviousPage = pageResult.Page > 1;

            var printFormat = "{0, -4} {1, -70} {2, -16} {3, -20}";

            Console.WriteLine(
                printFormat,
                nameof(Movie.Id),
                nameof(Movie.Name),
                nameof(Movie.ReleaseYear),
                nameof(Movie.Country));

            Console.WriteLine(MenuOptions.HorizontalLine);

            if (pageResult.Items.Count is 0)
            {
                Console.WriteLine(AlertMessages.EmptyFilterResult);
            }
            else
            {
                foreach (MovieResponse movie in pageResult.Items)
                {
                    Console.WriteLine(
                        printFormat,
                        movie.Id,
                        movie.Name,
                        movie.ReleaseYear,
                        movie.Director);
                }

                if (hasNextPage)
                {
                    Console.WriteLine(PageOptions.NextPageTitle);
                }

                if (hasPreviousPage)
                {
                    Console.WriteLine(PageOptions.PreviousPageTitle);
                }
            }

            Console.WriteLine(MenuOptions.BackTitle);
            Console.Write(MenuOptions.SelectListPageOptionTitle(hasNextPage, hasPreviousPage));

            string? choice = Console.ReadLine();

            if (choice is PageOptions.NextPage1stOption or
                PageOptions.NextPage2ndOption)
            {
                if (hasNextPage)
                    page++;
            }
            else if (choice is PageOptions.PreviousPage1stOption or
                PageOptions.PreviousPage2ndOption)
            {
                if (hasPreviousPage)
                    page--;
            }
            else if (choice is PageOptions.Back1stOption or
                PageOptions.Back2ndOption)
            {
                return;
            }
        }
    }

    private async Task ViewMovieDetailsAsync(int movieId)
    {
        try
        {
            MovieResponse movie = await _movieService.GetMovieWithReviewsByMovieIdAsync(movieId);

            Console.WriteLine($"{nameof(Movie.Id)}: {movie.Id}");
            Console.WriteLine($"{nameof(Movie.Name)}: {movie.Name}");
            Console.WriteLine($"{nameof(Movie.ReleaseYear)}: {movie.ReleaseYear}");
            Console.WriteLine($"{nameof(Movie.Director)}: {movie.Director}");
            Console.WriteLine($"{nameof(Movie.Country)}: {movie.Country}");
            Console.WriteLine();
            Console.WriteLine(MenuOptions.EditMovieLabel);
            Console.WriteLine(MenuOptions.DeleteMovieLabel);
            Console.WriteLine(MenuOptions.AddReviewLabel);
            Console.WriteLine(MenuOptions.ViewReviewsLabel);
            Console.WriteLine(MenuOptions.BackTitle);
            Console.Write(MenuOptions.SelectMovieDetailsOption);

            string? choice = Console.ReadLine();

            switch (choice)
            {
                case MenuOptions.EditMovie1stOption:
                case MenuOptions.EditMovie2ndOption:
                    await EditMovieAsync(movieId);
                    break;

                case MenuOptions.DeleteMovie1stOption:
                case MenuOptions.DeleteMovie2ndOption:
                    await DeleteMovieAsync(movieId);
                    break;

                case MenuOptions.AddReview1stOption:
                case MenuOptions.AddReview2ndOption:
                    await AddReviewAsync(movieId);
                    break;

                case MenuOptions.ViewReviews1stOption:
                case MenuOptions.ViewReviews2ndOption:
                    await ViewReviewsAsync(movieId);
                    break;

                case MenuOptions.Back1stOption:
                case MenuOptions.Back2ndOption:

                    return;

                default:
                    Console.WriteLine(MenuOptions.InvalidOption);
                    break;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    private async Task EditMovieAsync(int movieId)
    {
        Console.WriteLine(MenuOptions.EditMovieTitle);
        
        string name = InputHelper.EnterTextField(
            nameof(Movie.Name),
            AlertMessages.RequiredMovieName);

        int releaseYear = EnterRelaseYear();
        string director = InputHelper.EnterTextField(
            nameof(Movie.Director),
            AlertMessages.RequiredDirector);

        string country = InputHelper.EnterTextField(
            nameof(Movie.Country),
            AlertMessages.RequiredCountry);

        var request = new UpdateMovieRequest(
            movieId,
            name,
            releaseYear,
            director,
            country);

        await _movieService.UpdateMovieAsync(request);
        Console.WriteLine(
            AlertMessages.GetUpdateMovieSuccessMessage(movieId));
    }

    private async Task DeleteMovieAsync(int movieId)
    {
        Console.WriteLine(MenuOptions.DeleteMovieTitle);
        Console.Write(AlertMessages.ConfirmDeleteMovieMessage);

        string? choice = Console.ReadLine();

        switch (choice)
        {
            case MenuOptions.Yes1stOption:
            case MenuOptions.Yes2ndOption:
                await _movieService.DeleteMovieAsync(movieId);
                Console.WriteLine(
                    AlertMessages.GetDeleteMovieSuccessMessage(movieId));
                break;
            
            default:
                return;
        }
    }

    private async Task AddReviewAsync(int movieId)
    {
        Console.Clear();
        Console.WriteLine(MenuOptions.AddReviewTitle);

        string title = InputHelper.EnterTextField(
            nameof(Review.Title),
            AlertMessages.RequiredTitle);

        string content = InputHelper.EnterTextField(
            nameof(Review.Content),
            AlertMessages.RequiredContent);

        ushort rating = EnterRating();
        var request = new CreateReviewRequest(
            movieId,
            title,
            content,
            rating);

        await _reviewService.CreateReviewAsync(request);
        Console.WriteLine(AlertMessages.AddedReview);
        Console.WriteLine(MenuOptions.ContinueApp);
        Console.ReadKey();
    }

    private async Task ViewReviewsAsync(int movieId)
    {
        Console.Clear();

        MovieResponse movie = await _movieService.GetMovieWithReviewsByMovieIdAsync(movieId);
        var printFormat = "{0, -4} {1, -10} {2, -30} {3, -70}";

        Console.WriteLine(MenuOptions.ViewReviewsTitle);
        Console.WriteLine(
            printFormat,
            nameof(Review.Id),
            nameof(Review.Rating),
            nameof(Review.Title),
            nameof(Review.Content));

        Console.WriteLine(MenuOptions.HorizontalLine);

        if (movie.Reviews.Count is 0)
        {
            Console.WriteLine(AlertMessages.MovieHadNoReviews);

            return;
        }

        foreach (ReviewResponse review in movie.Reviews)
        {
            Console.WriteLine(
                printFormat,
                review.Id,
                review.Rating,
                review.Title,
                review.Content);
        }

        Console.WriteLine(MenuOptions.ContinueApp);
        Console.ReadKey();
    }

    private static int EnterRelaseYear()
    {
        string? input;
        int releaseYear;
        bool parsedReleaseYear;

        do
        {
            Console.Write("Enter release year: ");
            input = Console.ReadLine();
            parsedReleaseYear = int.TryParse(input, out releaseYear);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(AlertMessages.RequiredReleaseYear);
            }
            else if (!parsedReleaseYear)
            {
                Console.WriteLine(AlertMessages.InvalidReleaseYear);
            }
            else if (releaseYear < Movie.MinReleaseYear
                || releaseYear > Movie.MaxReleaseYear)
            {
                Console.WriteLine(AlertMessages.InvalidRelaseYearRange);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedReleaseYear
        || releaseYear < Movie.MinReleaseYear
        || releaseYear > Movie.MaxReleaseYear);

        return releaseYear;
    }

    private static int? EnterReleaseYearForFilter()
    {
        int releaseYear;
        bool parsedReleaseYear;
        string? input;

        do
        {
            Console.Write("Enter release year: ");
            input = Console.ReadLine();
            parsedReleaseYear = int.TryParse(input, out releaseYear);

            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            else if (!parsedReleaseYear)
            {
                Console.WriteLine(AlertMessages.RequiredReleaseYear);
            }
            else  if (releaseYear < Movie.MinReleaseYear
                    || releaseYear > Movie.MaxReleaseYear)
            {
                Console.WriteLine(AlertMessages.InvalidRelaseYearRange);
            }
        }
        while (!parsedReleaseYear
        || releaseYear < Movie.MinReleaseYear
        || releaseYear > Movie.MaxReleaseYear);

        return releaseYear;
    }


    private static ushort EnterRating()
    {
        string? input;
        ushort rating;
        bool parsedRating;

        do
        {
            Console.Write("Enter rating: ");
            input = Console.ReadLine();
            parsedRating = ushort.TryParse(input, out rating);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(AlertMessages.RequiredRating);
            }
            else if (!parsedRating)
            {
                Console.WriteLine(AlertMessages.InvalidRating);
            }
            else if (rating < Review.MinRating || rating > Review.MaxRating)
            {
                Console.WriteLine(AlertMessages.InvalidRaingRange);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedRating
        || rating < Review.MinRating
        || rating > Review.MaxRating);

        return rating;
    }
}
