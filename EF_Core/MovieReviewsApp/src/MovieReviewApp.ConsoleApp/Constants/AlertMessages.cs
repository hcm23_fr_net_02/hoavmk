using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.ConsoleApp.Constants;

public static class AlertMessages
{
    public const string EmptyMovieList = "Movies are empty.";

    public const string RequiredMovieName = "Name is required.";

    public const string RequiredDirector = "Director is required.";

    public const string RequiredCountry = "Country is required";

    public const string RequiredReleaseYear = "Release year is required";

    public const string RequiredTitle = "Title is required";

    public const string RequiredContent = "Content is required";

    public const string RequiredRating = "Rating is required";

    public const string AddedMovie = "Added new movie.";

    public const string AddedReview = "Added new review.";

    public const string EmptyFilterResult = "No movie found.";

    public const string InvalidReleaseYear = "Invalid release year.";

    public const string InvalidRating = "Invalid rating.";

    public const string MovieHadNoReviews = "This review doesn't have any review.";

    public const string ConfirmDeleteMovieMessage = "Are you sure to delete this movie? (Y/N): ";

    public static string GetUpdateMovieSuccessMessage(int movieId)
        => $"Updated the movie with id = {movieId}.";

    public static string GetDeleteMovieSuccessMessage(int movieId)
        => $"Deleted the movie with id = {movieId}";

    public static readonly string InvalidRelaseYearRange = $"Release year must be between"
        + $" {Movie.MinReleaseYear} and {Movie.MaxReleaseYear}";

    public static readonly string InvalidRaingRange = $"Rating must be between"
        + $" {Review.MinRating} and {Review.MaxRating}";
}
