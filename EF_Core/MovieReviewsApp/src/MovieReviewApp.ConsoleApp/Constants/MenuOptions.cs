namespace MovieReviewApp.ConsoleApp.Constants;

public static class MenuOptions
{
    public const string AppTitle = "*******************MoviewReviewsApp*******************";

    public const string AddNewMovieTitle = "*******************Add Movie*******************";

    public const string EditMovieTitle = "*******************Edit Movie*******************";

    public const string DeleteMovieTitle = "*******************Edit Movie*******************";

    public const string AddReviewTitle = "*******************Add Review*******************";

    public const string ViewReviewsTitle = "*******************Review List*******************";

    public const string FilterMoviesTitle = "*******************Filter Movies*******************";

    public const string FilterResultTitle = "*******************Filter Result*******************";

    public const string ViewMovieListLabel = "1. View movies";

    public const string AddNewMovieLabel = "2. Add new movie";

    public const string FilterMoviesLabel = "3. Filter movies";

    public const string ExitLabel = "4. Exit";

    public const string EditMovieLabel = "E. Edit movie";

    public const string DeleteMovieLabel = "D. Delete movie";

    public const string AddReviewLabel = "A. Add review";

    public const string ViewReviewsLabel = "L. View movie's reviews";

    public const string BackTitle = "B. Back";

    public static readonly string HorizontalLine = new('-', 120);

    public const string SelectMovieDetailsOption = "Select an option (E/D/A/L/B): ";

    public const string SelectMenuOptionTitle = "Select an option (1/2/3/4): ";

    public static string SelectListPageOptionTitle(bool hasNextPage, bool hasPreviousPage)
    {
        string nextPage = hasNextPage ? "N/" : string.Empty;
        string previousPage = hasPreviousPage ? "P/" : string.Empty;

        return "Select an option ("
            + nextPage
            + previousPage
            + "B): ";
    }

    public const string ContinueApp = "Press any key to continue.";

    public static string SelectReviewListOptionTitle(bool hasNextPage, bool hasPreviousPage)
    {
        string nextPage = hasNextPage ? "N/" : string.Empty;
        string previousPage = hasPreviousPage ? "P/" : string.Empty;

        return "Select an option ("
            + nextPage
            + previousPage
            + "B) or enter a review's Id to view movie's details: ";
    }

    public const string ViewMovieListOption = "1";

    public const string AddMovieOption = "2";

    public const string FilterMoviesOption = "3";

    public const string ExitOption = "4";

    public const string EditMovie1stOption = "E";

    public const string EditMovie2ndOption = "e";

    public const string DeleteMovie1stOption = "D";

    public const string DeleteMovie2ndOption = "d";

    public const string AddReview1stOption = "A";

    public const string AddReview2ndOption = "a";

    public const string ViewReviews1stOption = "L";

    public const string ViewReviews2ndOption = "l";

    public const string Back1stOption = "B";

    public const string Back2ndOption = "b";

    public const string Yes1stOption = "Y";

    public const string Yes2ndOption = "y";

    public const string No1stOption = "N";

    public const string No2ndOption = "n";

    public const string InvalidOption = "Invalid option!";
}
