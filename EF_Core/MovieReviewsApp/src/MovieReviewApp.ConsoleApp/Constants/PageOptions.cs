namespace MovieReviewApp.ConsoleApp.Constants;

public static class PageOptions
{
    public const int PageSize = 10;

    public const string NextPageTitle = "N. Next page";

    public const string PreviousPageTitle = "P. Previous page";

    public const string BackTitle = "B. Back";

    public const string NextPage1stOption = "N";

    public const string NextPage2ndOption = "n";

    public const string PreviousPage1stOption = "P";

    public const string PreviousPage2ndOption = "p";

    public const string Back1stOption = "B";

    public const string Back2ndOption = "b";
}
