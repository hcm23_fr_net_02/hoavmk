﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MovieReviewApp.Application;
using MovieReviewApp.ConsoleApp;
using MovieReviewApp.Infrastructure;
using MovieReviewApp.Infrastructure.Persistence;
using MovieReviewApp.Infrastructure.Persistence.Seeding;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

builder.Services.AddPresentation();
builder.Services.AddApplication();
builder.Services.AddInfrastructure(builder.Configuration);

using IHost host = builder.Build();

using (var scope = host.Services.CreateScope())
{
    var scopedProvider = scope.ServiceProvider;

    try
    {
        var appDbContext = scopedProvider.GetRequiredService<AppDbContext>();

        await AppDbContextSeed.SeedAsync(appDbContext);
    }
    catch (Exception e)
    {
        Console.WriteLine(e.Message);
    }
}

var app = host.Services.GetRequiredService<MovieReviewsApp>();

await app.RunAsync();
