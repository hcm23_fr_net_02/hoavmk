using Microsoft.Extensions.DependencyInjection;

namespace MovieReviewApp.ConsoleApp;

public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services.AddScoped<MovieReviewsApp>();

        return services;
    }
}
