namespace MovieReviewApp.ConsoleApp.Helpers;

public static class MenuHelper
{
    public static string GetPageTitle(string field, int page)
        => $"*******************{field} Page {page}*******************";

}
