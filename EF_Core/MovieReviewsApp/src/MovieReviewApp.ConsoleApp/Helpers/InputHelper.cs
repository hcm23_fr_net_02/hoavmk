namespace MovieReviewApp.ConsoleApp.Helpers;

public static class InputHelper
{
    public static string? EnterOptionalTextField(string fieldName)
    {
        Console.Write($"Enter {fieldName}: ");
        return Console.ReadLine();
    }

    public static string EnterTextField(string fieldName, string alertMessage)
    {
        string? input;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(alertMessage);
            }
        }
        while (string.IsNullOrEmpty(input));

        return input;
    }

    public static int EnterNumberField(string fieldName)
    {
        string? input;
        int number;
        bool parsedNumber;

        do
        {
            Console.Write($"Enter {fieldName}: ");
            input = Console.ReadLine();
            parsedNumber = int.TryParse(input, out number);

            if (string.IsNullOrEmpty(fieldName))
            {
                Console.WriteLine($"{fieldName} is required.");
            }
            else if (!parsedNumber)
            {
                Console.WriteLine($"Invalid {fieldName}");
            }
        }
        while (string.IsNullOrEmpty(input) || !parsedNumber);

        return number;
    }
}
