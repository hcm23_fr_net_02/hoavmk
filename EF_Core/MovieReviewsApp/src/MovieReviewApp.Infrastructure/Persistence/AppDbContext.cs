using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Entities;

namespace MovieReviewApp.Infrastructure.Persistence;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(
            typeof(AppDbContext).Assembly);
    }

    public DbSet<Movie> Movies { get; set; } = default!;

    public DbSet<Review> Reviews { get; set; } = default!;
}
