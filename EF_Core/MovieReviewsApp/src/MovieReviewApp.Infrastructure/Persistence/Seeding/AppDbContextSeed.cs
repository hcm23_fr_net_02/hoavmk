using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Infrastructure.Constants;

namespace MovieReviewApp.Infrastructure.Persistence.Seeding;

public static class AppDbContextSeed
{
    private static readonly JsonSerializerOptions s_jsonSerializerOptions = new()
    {
        PropertyNameCaseInsensitive = true
    };

    public async static Task SeedAsync(AppDbContext dbContext)
    {
        if (dbContext.Database.IsSqlite())
        {
            dbContext.Database.Migrate();
        }

        if (await dbContext.Movies.CountAsync() is 0)
        {
            List<Movie>? movies = await GetDataFromJsonFileAsync<Movie>(
                DataPaths.MovieDataFilePath);
            
            if (movies is not null)
            {
                await dbContext.Movies.AddRangeAsync(movies);
                await dbContext.SaveChangesAsync();
            }
        }
        
        if (await dbContext.Reviews.CountAsync() is 0)
        {
            List<Review>? reviews = await GetDataFromJsonFileAsync<Review>(
                DataPaths.ReviewDataFilePath);
            
            if (reviews is not null)
            {
                await dbContext.Reviews.AddRangeAsync(reviews);
                await dbContext.SaveChangesAsync();
            }
        }  
    }

    private static async Task<List<TData>?> GetDataFromJsonFileAsync<TData>(string dataPath)
    {
        try
        {
            using FileStream openStream = File.OpenRead(dataPath);

            return await JsonSerializer.DeserializeAsync<List<TData>>(
                openStream,
                s_jsonSerializerOptions);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);

            return null;
        }
    }
}
