using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.Infrastructure.Persistence;

namespace MovieReviewApp.Infrastructure.Repositories;

public class GenericRepository<TEntity> :
    IGenericRepository<TEntity>
    where TEntity : class
{
    private readonly DbSet<TEntity> _dbSet;

    public GenericRepository(AppDbContext appDbContext)
    {
        _dbSet = appDbContext.Set<TEntity>();
    }

    public virtual async Task<TEntity> AddAsync(TEntity entity)
    {
        EntityEntry<TEntity> result = await _dbSet.AddAsync(entity);

        return result.Entity;
    }

    public virtual async Task<TEntity?> GetByIdAsync(int id)
    {
        return await _dbSet.FindAsync(id);
    }
}
