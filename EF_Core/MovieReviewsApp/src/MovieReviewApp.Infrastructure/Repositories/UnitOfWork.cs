using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.Infrastructure.Persistence;

namespace MovieReviewApp.Infrastructure.Repositories;

public class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly AppDbContext _appDbContext;
    private IMovieRepository _movieRepository = default!;
    private IReviewRepository _reviewRepository = default!;

    public IMovieRepository Movies
    {
        get
        {
            _movieRepository ??= new MovieRepository(_appDbContext);

            return _movieRepository;
        }
    }

    public IReviewRepository Reviews
    {
        get
        {
            _reviewRepository ??= new ReviewRepository(_appDbContext);

            return _reviewRepository;
        }
    }

    public UnitOfWork(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public async Task SaveChangesAsync()
    {
        await _appDbContext.SaveChangesAsync();
    }

    private bool disposed = false;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposed)
        {
            if (disposing)
            {
                _appDbContext.Dispose();
            }

            disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
