using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.Infrastructure.Persistence;

namespace MovieReviewApp.Infrastructure.Repositories;

public class ReviewRepository :
    GenericRepository<Review>,
    IReviewRepository
{
    private readonly AppDbContext _appDbContext;

    public ReviewRepository(AppDbContext appDbContext)
        : base(appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public async Task<List<Review>> GetByMovieIdAsync(int movieId)
    {
        return await _appDbContext.Reviews
            .Where(r => r.MovieId == movieId)
            .ToListAsync();
    }
}
