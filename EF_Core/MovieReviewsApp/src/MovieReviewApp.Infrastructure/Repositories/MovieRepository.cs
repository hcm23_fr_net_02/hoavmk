using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MovieReviewApp.Domain.Common.Contracts;
using MovieReviewApp.Domain.Entities;
using MovieReviewApp.Domain.Repositories;
using MovieReviewApp.Infrastructure.Persistence;

namespace MovieReviewApp.Infrastructure.Repositories;

public class MovieRepository :
    GenericRepository<Movie>,
    IMovieRepository
{
    private readonly AppDbContext _appDbContext;

    public MovieRepository(AppDbContext appDbContext)
        : base(appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public override async Task<Movie?> GetByIdAsync(int id)
    {
        return await _appDbContext.Movies
            .Where(movie => movie.Id == id)
            .Include(movie => movie.Reviews)
            .FirstOrDefaultAsync();
    }

    public void Delete(Movie movie)
    {
        _appDbContext.Movies.Remove(movie);
    }

    public async Task<PageResult<Movie>> GetListPageAsync(
        int page,
        int pageSize,
        Expression<Func<Movie, bool>>? filter = null,
        Func<IQueryable<Movie>, IOrderedQueryable<Movie>>? orderBy = null,
        string includeProperties = "")
    {
        IQueryable<Movie> query = _appDbContext.Movies;

        if (filter is not null)
        {
            query = query.Where(filter);
        }

        if (orderBy is not null)
        {
            query = orderBy(query);
        }
        else
        {
            query = query.OrderBy(x => x.Id);
        }

        foreach (var includeProperty in includeProperties.Split(
            new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        {
            query = query.Include(includeProperty);
        }

        int totalItems = await query.CountAsync();
        var totalPages = totalItems / pageSize;

        if (totalItems % pageSize is not 0)
            totalPages++;

        var movies = await query.Skip((page - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        return new PageResult<Movie>
        {
            Items = movies,
            TotalPages = totalPages,
            Page = page,
            PageSize = pageSize,
            TotalItems = totalItems
        };
    }
}
