namespace MovieReviewApp.Infrastructure.Constants;

public static class DataPaths
{
    public const string MovieDataFilePath = "Data/movies.json";

    public const string ReviewDataFilePath = "Data/reviews.json";

    public const string DirectorDataFilePath = "Data/directors.json";
}
