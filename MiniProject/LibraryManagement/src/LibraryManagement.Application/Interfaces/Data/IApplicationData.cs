using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Interfaces.Data;

public interface IApplicationData
{
    List<Book> Books { get; set; }
    List<BookRating> BookRatings { get; set; }
    List<Reader> Readers { get; set; }
    List<Loan> Loans { get; set; }
}
