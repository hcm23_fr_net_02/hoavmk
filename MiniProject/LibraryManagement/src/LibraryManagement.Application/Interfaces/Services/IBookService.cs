using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Contracts.Books;
using ErrorOr;

namespace LibraryManagement.Application.Interfaces.Services;

public interface IBookService
{
    ErrorOr<Book> GetBookWithRatingsByBookId(int bookId);
    PageResult<Book> GetBooksListPage(int page, int pageSize);
    ErrorOr<Book> GetBookById(int bookId);
    ErrorOr<Created> AddBook(BookRequest request);
    ErrorOr<Updated> EditBook(int bookId, BookRequest request);
    ErrorOr<Deleted> DeleteBook(int bookId);
    PageResult<Book> FilterBooks(FilterBookRequest request);
    ErrorOr<Created> BorrowBook(LoanRequest request);
}
