using ErrorOr;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Interfaces.Services;

public interface ILoanService
{
    PageResult<Loan> GetLoansListPage(int page, int pageSize);
    PageResult<Loan> GetLoansByCustomerName(
        string? customerName,
        int page,
        int pageSize);

    PageResult<Loan> GetLoansByBookTitle(
        string? bookTitle,
        int page,
        int pageSize);
    ErrorOr<Success> ReturnBook(int loanId);
}
