using ErrorOr;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.Interfaces.Services;

public interface IReaderService
{
    ErrorOr<Created> AddReader(
        string fullName,
        string phoneNumber,
        string address);

    ErrorOr<Deleted> DeleteReader(int ReaderId);    
    ErrorOr<Updated> UpdateReader(
        int id,
        string fullName,
        string phoneNumber,
        string address);

    PageResult<Reader> GetReadersListPage(int page, int pageSize);
}
