using ErrorOr;
using LibraryManagement.Contracts.BookRatings;

namespace LibraryManagement.Application.Interfaces.Services;

public interface IBookRatingService
{
    ErrorOr<Created> AddBookRating(BookRatingRequest request);
}
