using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Contracts.Books;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;

namespace LibraryManagement.Application.Services;

public class BookService : IBookService
{
    private readonly IBookRepository _bookRepository;
    private readonly IBookRatingRepository _bookRatingRepository;
    private readonly IReaderRepository _readerRepository;
    private readonly ILoanRepository _loanRepository;

    public BookService(
        IBookRepository bookRepository,
        IBookRatingRepository bookRatingRepository,
        IReaderRepository readerRepository,
        ILoanRepository loanRepository)
    {
        _bookRepository = bookRepository;
        _bookRatingRepository = bookRatingRepository;
        _readerRepository = readerRepository;
        _loanRepository = loanRepository;
    }

    public ErrorOr<Created> AddBook(BookRequest request)
    {
        var createBookResult = Book.Create(
            request.Title,
            request.Author,
            request.PublicationYear,
            request.Quantity,
            request.Genre,
            request.Description);
        
        if (createBookResult.IsError)
        {
            return createBookResult.Errors;
        }

        _bookRepository.Add(createBookResult.Value);

        return Result.Created;
    }

    public ErrorOr<Deleted> DeleteBook(int bookId)
    {
        var book = _bookRepository.GetById(bookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(bookId);
        }

        bool isAnyLoanWithBook = _loanRepository.IsAnyLoansWithBook(bookId);

        if (isAnyLoanWithBook)
        {
            return Errors.Book.HadLoans(bookId);
        }

        _bookRepository.Delete(book);

        return Result.Deleted;
    }

    public ErrorOr<Updated> EditBook(int bookId, BookRequest request)
    {
        var book = _bookRepository.GetById(bookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(bookId);
        }

        var errors = new List<Error>();

        var updatePublicationYearResult = book.UpdatePublicationYear(request.PublicationYear);
        var updateQuantityResult = book.UpdateQuantity(request.Quantity);

        if (updatePublicationYearResult.IsError)
        {
            errors.Add(updatePublicationYearResult.FirstError);
        }

        if (updateQuantityResult.IsError)
        {
            errors.Add(updateQuantityResult.FirstError);
        }

        if (errors.Count > 0)
        {
            return errors;
        }

        book.UpdateAuthor(request.Author);
        book.UpdateTitle(request.Title);
        book.UpdateGenre(request.Genre);
        book.UpdateDescription(request.Description);

        return Result.Updated;
    }

    public PageResult<Book> FilterBooks(FilterBookRequest request)
    {
        return _bookRepository.FilterBooks(
            request.Title,
            request.Author,
            request.PublicationYear,
            request.Genre,
            request.Page,
            request.PageSize);
    }

    public ErrorOr<Book> GetBookById(int bookId)
    {
        var book = _bookRepository.GetById(bookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(bookId);
        }

        return book;
    }

    public PageResult<Book> GetBooksListPage(int page, int pageSize)
    {
        return _bookRepository.GetBooksListPage(page, pageSize);
    }

    public ErrorOr<Book> GetBookWithRatingsByBookId(int bookId)
    {
        var book = _bookRepository.GetById(bookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(bookId);
        }

        var ratings = _bookRatingRepository.GetBookRatingsByBookId(bookId);
        var clonedBook = (Book)book.Clone();

        return clonedBook.WithRatings(ratings);
    }

    public ErrorOr<Created> BorrowBook(LoanRequest request)
    {
        var book = _bookRepository.GetById(request.BookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(request.BookId);
        }

        if (book.Quantity is 0)
        {
            return Errors.Book.OutOfStock(request.BookId);
        }

        var reader = _readerRepository.GetById(request.ReaderId);

        if (reader is null)
        {
            return Errors.Reader.NotFoundValidation(request.ReaderId);
        }

        var createLoanResult = Loan.Create(
            request.BorrowedDate,
            request.DueDate,
            request.BookId,
            request.ReaderId);

        if (createLoanResult.IsError)
        {
            return createLoanResult.FirstError;
        }

        var loan = createLoanResult.Value;

        var updateBookQuantityResult = book.UpdateQuantity(book.Quantity - 1);

        if (updateBookQuantityResult.IsError)
        {
            return updateBookQuantityResult.FirstError;
        }

        _loanRepository.Add(loan);

        return Result.Created;
    }
}
