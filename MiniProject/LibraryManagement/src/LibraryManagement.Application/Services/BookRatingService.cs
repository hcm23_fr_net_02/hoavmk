using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Contracts.BookRatings;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;

namespace LibraryManagement.Application.Services;

public class BookRatingService : IBookRatingService
{
    private readonly IBookRatingRepository _bookRatingRepository;
    private readonly IBookRepository _bookRepository;

    public BookRatingService(
        IBookRatingRepository bookRatingRepository,
        IBookRepository bookRepository)
    {
        _bookRatingRepository = bookRatingRepository;
        _bookRepository = bookRepository;
    }

    public ErrorOr<Created> AddBookRating(BookRatingRequest request)
    {
        var book = _bookRepository.GetById(request.BookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(request.BookId);
        }

        var createBookRatingResult = BookRating.Create(
            request.Title,
            request.Content,
            request.Rating,
            request.BookId);

        if (createBookRatingResult.IsError)
        {
            return createBookRatingResult.FirstError;
        }

        var bookRating = createBookRatingResult.Value;

        _bookRatingRepository.Add(bookRating);

        return Result.Created;
    }
}
