using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;

namespace LibraryManagement.Application.Services;

public class ReaderService : IReaderService
{
    private readonly IReaderRepository _readerRepository;
    private readonly ILoanRepository _loanRepository;

    public ReaderService(
        IReaderRepository readerRepository,
        ILoanRepository loanRepository)
    {
        _readerRepository = readerRepository;
        _loanRepository = loanRepository;
    }

    public ErrorOr<Created> AddReader(
        string fullName,
        string phoneNumber,
        string address)
    {
        var reader = Reader.Create(fullName, phoneNumber, address);

        _readerRepository.Add(reader);

        return Result.Created;
    }

    public ErrorOr<Deleted> DeleteReader(int readerId)
    {
        var reader = _readerRepository.GetById(readerId);

        if (reader is null)
        {
            return Errors.Reader.NotFoundValidation(readerId);
        }

        bool readerHadLoans = _loanRepository.IsAnyLoansWithReader(readerId);

        if (readerHadLoans)
        {
            return Errors.Reader.HadLoans(readerId);
        }

        _readerRepository.Delete(reader);

        return Result.Deleted;
    }

    public PageResult<Reader> GetReadersListPage(int page, int pageSize)
    {
        return _readerRepository.GetReadersListPage(page, pageSize);
    }
    
    public ErrorOr<Updated> UpdateReader(
        int id,
        string fullName,
        string phoneNumber,
        string address)
    {
        var reader = _readerRepository.GetById(id);

        if (reader is null)
        {
            return Errors.Reader.NotFoundValidation(id);
        }

        reader.UpdateFullName(fullName);
        reader.UpdatePhoneNumber(phoneNumber);
        reader.UpdateAddress(address);

        return Result.Updated;
    }
}
