using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;

namespace LibraryManagement.Application.Services;

public class LoanService : ILoanService
{
    private readonly ILoanRepository _loanRepository;
    private readonly IBookRepository _bookRepository;

    public LoanService(
        ILoanRepository loanRepository,
        IBookRepository bookRepository)
    {
        _loanRepository = loanRepository;
        _bookRepository = bookRepository;
    }

    public ErrorOr<Success> ReturnBook(int loanId)
    {
        var loan = _loanRepository.GetLoanById(loanId);

        if (loan is null)
        {
            return Errors.Loan.NotFoundValidation(loanId);
        }

        if (loan.ReturnDate is not null)
        {
            return Errors.Loan.AlreadyReturned(loanId);
        }

        var book = _bookRepository.GetById(loan.BookId);

        if (book is null)
        {
            return Errors.Book.NotFoundValidation(loan.BookId);
        }

        var updateBookQuantityResult = book.UpdateQuantity(book.Quantity + 1);

        if (updateBookQuantityResult.IsError)
        {
            return updateBookQuantityResult.FirstError;
        }

        loan.ReturnBook();

        return Result.Success;
    }

    public PageResult<Loan> GetLoansByBookTitle(
        string? bookTitle,
        int page,
        int pageSize)
    {
        return _loanRepository.GetLoansByBookTitle(
            bookTitle,
            page,
            pageSize);
    }

    public PageResult<Loan> GetLoansByCustomerName(
        string? customerName,
        int page,
        int pageSize)
    {
        return _loanRepository.GetLoansByCustomerName(
            customerName,
            page,
            pageSize);
    }

    public PageResult<Loan> GetLoansListPage(int page, int pageSize)
    {
        return _loanRepository.GetLoansWithBookAndCustomerListPage(page, pageSize);
    }
}
