namespace LibraryManagement.Contracts.BookRatings;

public record BookRatingRequest(
    string Title,
    string Content,
    int Rating,
    int BookId);
