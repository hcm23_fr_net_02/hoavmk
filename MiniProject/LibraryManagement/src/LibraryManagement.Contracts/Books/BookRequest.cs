namespace LibraryManagement.Contracts.Books;

public record BookRequest(
    string Title,
    string Author,
    int PublicationYear,
    int Quantity,
    string Genre,
    string Description);
