namespace LibraryManagement.Contracts.Books;

public record LoanRequest(
    DateTime BorrowedDate,
    DateTime DueDate,
    int BookId,
    int ReaderId);
