namespace LibraryManagement.Contracts.Books;

public record FilterBookRequest(
    string? Title,
    string? Author,
    int? PublicationYear,
    string? Genre,
    int Page,
    int PageSize);
