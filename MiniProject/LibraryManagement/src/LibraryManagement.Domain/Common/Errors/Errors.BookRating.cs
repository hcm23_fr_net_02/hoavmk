using ErrorOr;

namespace LibraryManagement.Domain.Common.Errors;

public static partial class Errors
{
    public static class BookRating
    {
        public static Error InvalidRatingRange => Error.Validation(
            code: "BookRating.InvalidRatingRange",
            description: $"Rating must be between {Entities.BookRating.MinRating}"
                + $" and {Entities.BookRating.MaxRating}.");
    }
}
