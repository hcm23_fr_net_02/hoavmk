using ErrorOr;

namespace LibraryManagement.Domain.Common.Errors;

public static partial class Errors
{
    public static class Reader
    {
        public static Error NotFoundValidation(int readerId) => Error.Validation(
            code: "Reader.NotFoundValidation",
            description: $"The reader with id = {readerId} was not found.");

        public static Error HadLoans(int readerId) => Error.Validation(
            code: "Reader.NotFoundValidation",
            description: $"The reader with id = {readerId} already borrowed books.");
    }
}
