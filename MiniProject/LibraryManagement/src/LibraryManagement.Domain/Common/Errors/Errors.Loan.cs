using ErrorOr;

namespace LibraryManagement.Domain.Common.Errors;

public static partial class Errors
{
    public static class Loan
    {
        public static Error NotFoundValidation(int loanId) => Error.Validation(
            code: "Loan.NotFoundValidation",
            description: $"The loan with id = {loanId} was not found.");

        public static Error InvalidDueDate => Error.Validation(
            code: "Loan.InvalidDueDate",
            description: "DueDate must be larger than BorrowedDate.");

        public static Error AlreadyReturned(int loanId) => Error.Validation(
            code: "Loan.AlreadyReturned",
            description: $"The loan with id = {loanId} already returned.");
    }
}
