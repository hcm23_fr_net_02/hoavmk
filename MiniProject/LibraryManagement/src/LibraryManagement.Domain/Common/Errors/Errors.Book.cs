using ErrorOr;

namespace LibraryManagement.Domain.Common.Errors;

public static partial class Errors
{
    public static class Book
    {
        public static Error NotFoundValidation(int bookId) => Error.Validation(
            code: "Book.NotFoundValidation",
            description: $"The book with id = {bookId} was not found.");

        public static Error OutOfStock(int bookId) => Error.Validation(
            code: "Book.OutOfStock",
            description: $"The book with id = {bookId} was out of stock.");

        public static Error InvalidQuantity => Error.Validation(
            code: "Book.InvalidQuantity",
            description: $"Quantity must be larger than or equal "
            + $"{Entities.Book.MinQuantity}.");

        public static Error InvalidPublicationYear => Error.Validation(
            code: "Book.InvalidPublicationYear",
            description: $"PublicationYear must be "
                + $"{Entities.Book.MinPublicationYear} and "
                + $"{Entities.Book.MaxPublicationYear}");

        public static Error HadLoans(int bookId) => Error.Validation(
            code: "Book.HadLoans",
            description: $"The book with id = {bookId} is referenced by another loan.");
    }
}
