namespace LibraryManagement.Domain.Common.Contracts;

public class PageResult<T> where T : class
{
    public List<T> Items { get; set; }

    public int CurrentPage { get; set; }

    public int PageSize { get; set; }

    public int TotalPages { get; set; }

    public PageResult(
        List<T> items,
        int currentPage,
        int pageSize,
        int totalPages)
    {
        Items = items;
        CurrentPage = currentPage;
        PageSize = pageSize;
        TotalPages = totalPages;
    }
}
