using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Domain.Repositories;

public interface IReaderRepository
{
    PageResult<Reader> GetReadersListPage(int page, int pageSize);
    Reader? GetById(int id);
    void Add(Reader Reader);
    void Delete(Reader Reader);
}
