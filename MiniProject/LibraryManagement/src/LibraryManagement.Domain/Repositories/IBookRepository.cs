using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Domain.Repositories;

public interface IBookRepository
{
    PageResult<Book> GetBooksListPage(int page, int pageSize);
    PageResult<Book> FilterBooks(
        string? title,
        string? author,
        int? publicationYear,
        string? genre,
        int page,
        int pageSize);

    Book? GetById(int id);
    void Add(Book book);
    void Delete(Book book);
}
