using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Domain.Repositories;

public interface ILoanRepository
{
    Loan? GetLoanById(int LoanId);
    bool IsAnyLoansWithReader(int readerId);
    bool IsAnyLoansWithBook(int bookId);
    void Add(Loan Loan);
    PageResult<Loan> GetLoansByCustomerName(
        string? customerName,
        int page,
        int pageSize);
    
    PageResult<Loan> GetLoansByBookTitle(
        string? bookTitle,
        int page,
        int pageSize);
        
    PageResult<Loan> GetLoansWithBookAndCustomerListPage(int page, int pageSize);
}
