using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Domain.Repositories;

public interface IBookRatingRepository
{
    List<BookRating> GetBookRatings();
    List<BookRating> GetBookRatingsByBookId(int bookId);
    BookRating? GetById(int id);
    void Add(BookRating bookRating);
    void Delete(BookRating bookRating);
}
