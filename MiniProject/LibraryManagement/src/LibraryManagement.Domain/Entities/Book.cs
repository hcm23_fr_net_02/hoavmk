using System.Text.Json.Serialization;
using LibraryManagement.Domain.Common.Errors;
using ErrorOr;

namespace LibraryManagement.Domain.Entities;

public class Book : ICloneable
{
    private static int s_identity = 1;
    private List<BookRating> _ratings = new();

    public const int MinPublicationYear = 1000;

    public static readonly int MaxPublicationYear = DateTime.UtcNow.Year;

    public const int MinQuantity = 0;

    public const int MaxQuantity = int.MaxValue;

    public int Id { get; init; }

    public string Title { get; private set; }

    public string Author { get; private set; }

    public int PublicationYear { get; private set; }

    public int Quantity { get; private set; }

    public string Genre { get; private set; }

    public string Description { get; private set; }

    public List<BookRating> Ratings => _ratings.ToList();

    private Book(
        int id,
        string title,
        string author,
        int publicationYear,
        int quantity,
        string genre,
        string description)
    {
        Id = id;
        Title = title;
        Author = author;
        PublicationYear = publicationYear;
        Quantity = quantity;
        Genre = genre;
        Description = description;
    }

    [JsonConstructor]
    public Book(
        string title,
        string author,
        int publicationYear,
        int quantity,
        string genre,
        string description)
    {
        Id = s_identity++;
        Title = title;
        Author = author;
        PublicationYear = publicationYear;
        Quantity = quantity;
        Genre = genre;
        Description = description;
    }

    public static ErrorOr<Book> Create(
        string title,
        string author,
        int publicationYear,
        int quantity,
        string genre,
        string description)
    {
        var errors = new List<Error>();
        var validatePublicationYearResult = ValidatePublicationYear(publicationYear);
        var validateQuantity = ValidateQuantity(quantity);

        if (validatePublicationYearResult.IsError)
        {
            errors.Add(validatePublicationYearResult.FirstError);
        }

        if (validateQuantity.IsError)
        {
            errors.Add(validateQuantity.FirstError);
        }

        if (errors.Count > 0)
        {
            return errors;
        }

        return new Book(
            s_identity++,
            title,
            author,
            publicationYear,
            quantity,
            genre,
            description);
    }

    public object Clone()
    {
        return new Book(
            Id,
            Title,
            Author,
            PublicationYear,
            Quantity,
            Genre,
            Description);
    }

    public Book WithRatings(List<BookRating> bookRatings)
    {
        _ratings = bookRatings;

        return this;
    }
    public void UpdateTitle(string title)
    {
        Title = title;
    }

    public void UpdateAuthor(string author)
    {
        Author = author;
    }

    public ErrorOr<Success> UpdatePublicationYear(int publicationYear)
    {
        var validatePublicationYearResult = ValidatePublicationYear(publicationYear);

        if (validatePublicationYearResult.IsError)
        {
            return validatePublicationYearResult;
        }

        PublicationYear = publicationYear;

        return Result.Success;
    }

    public ErrorOr<Success> UpdateQuantity(int quantity)
    {
        var validateQuantityResult = ValidateQuantity(quantity);

        if (validateQuantityResult.IsError)
        {
            return validateQuantityResult;
        }

        Quantity = quantity;

        return Result.Success;
    }

    public void UpdateGenre(string genre)
    {
        Genre = genre;
    }

    public void UpdateDescription(string description)
    {
        Description = description;
    }

    public override string ToString()
    {
        return $"Id = {Id} | Title = {Title} | Author = {Author} | "
            + $"PublicationYear = {PublicationYear} | Quantity = {Quantity}"
            + $"Genre = {Genre} | Description = {Description}";
    }

    private static ErrorOr<Success> ValidatePublicationYear(int publicationYear)
    {
        if (publicationYear < MinPublicationYear || publicationYear > MaxPublicationYear)
        {
            return Errors.Book.InvalidPublicationYear;
        }

        return Result.Success;
    }

    private static ErrorOr<Success> ValidateQuantity(int quantity)
    {
        if (quantity < MinQuantity)
        {
            return Errors.Book.InvalidQuantity;
        }

        return Result.Success;
    }
}
