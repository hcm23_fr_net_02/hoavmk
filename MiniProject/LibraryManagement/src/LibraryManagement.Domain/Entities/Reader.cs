using System.Text.Json.Serialization;

namespace LibraryManagement.Domain.Entities;

public class Reader
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public string FullName { get; private set; }

    public string PhoneNumber { get; private set; }

    public string Address { get; private set; }

    private Reader(
        int id,
        string fullName,
        string phoneNumber,
        string address)
    {
        Id = id;
        FullName = fullName;
        PhoneNumber = phoneNumber;
        Address = address;
    }

    [JsonConstructor]
    public Reader(
        string fullName,
        string phoneNumber,
        string address)
    {
        Id = s_identity++;
        FullName = fullName;
        PhoneNumber = phoneNumber;
        Address = address;
    }

    public static Reader Create(
        string fullName,
        string phoneNumber,
        string address)
    {
        return new(s_identity++, fullName, phoneNumber, address);
    }

    public void UpdateFullName(string fullName)
    {
        FullName = fullName;
    }

    public void UpdatePhoneNumber(string phoneNumber)
    {
        PhoneNumber = phoneNumber;
    }

    public void UpdateAddress(string address)
    {
        Address = address;
    }

    public override string ToString()
    {
        return $"Id = {Id} | FullName = {FullName} | "
            + $"PhoneNumber = {PhoneNumber} | Address = {Address}";
    }
}
