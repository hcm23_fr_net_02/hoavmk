using System.Text.Json.Serialization;
using ErrorOr;
using LibraryManagement.Domain.Common.Errors;

namespace LibraryManagement.Domain.Entities;

public class BookRating
{
    private static int s_identity = 1;

    public const int MinRating = 1;

    public const int MaxRating = 10;

    public int Id { get; init; }

    public string Title { get; private set; }

    public string Content { get; private set; }

    public int Rating { get; private set; }

    public int BookId { get; private set; }

    private BookRating(
        int id,
        string title,
        string content,
        int rating,
        int bookId)
    {
        Id = id;
        Title = title;
        Content = content;
        Rating = rating;
        BookId = bookId;
    }

    [JsonConstructor]
    public BookRating(
        string title,
        string content,
        int rating,
        int bookId)
    {
        Id = s_identity++;
        Title = title;
        Content = content;
        Rating = rating;
        BookId = bookId;
    }

    public static ErrorOr<BookRating> Create(
        string title,
        string content,
        int rating,
        int bookId)
    {
        var validateRatingResult = ValidateRating(rating);

        if (validateRatingResult.IsError)
        {
            return validateRatingResult.FirstError;
        }

        return new BookRating(
            s_identity++,
            title,
            content,
            rating,
            bookId);
    }

    private static ErrorOr<Success> ValidateRating(int rating)
    {
        if (rating < MinRating || rating > MaxRating)
        {
            return Errors.BookRating.InvalidRatingRange;
        }

        return Result.Success;
    }
}
