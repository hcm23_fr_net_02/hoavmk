using System.Text.Json.Serialization;
using ErrorOr;
using LibraryManagement.Domain.Common.Errors;

namespace LibraryManagement.Domain.Entities;

public class Loan
{
    private static int s_identity = 1;

    public int Id { get; init; }

    public DateTime BorrowedDate { get; private set; }

    public DateTime DueDate { get; private set; }

    public DateTime? ReturnDate { get; private set; } = null;

    public int BookId { get; private set; }

    public Book? Book { get; private set; }

    public int ReaderId { get; private set; }

    public Reader? Reader { get; private set; }

    private Loan(
        int id,
        DateTime borrowedDate,
        DateTime dueDate,
        int bookId,
        int readerId)
    {
        Id = id;
        BorrowedDate = borrowedDate;
        DueDate = dueDate;
        BookId = bookId;
        ReaderId = readerId;
    }

    [JsonConstructor]
    public Loan(
        DateTime borrowedDate,
        DateTime dueDate,
        int bookId,
        int readerId)
    {
        Id = s_identity++;
        BorrowedDate = borrowedDate;
        DueDate = dueDate;
        BookId = bookId;
        ReaderId = readerId;
    }

    public static ErrorOr<Loan> Create(
        DateTime borrowedDate,
        DateTime dueDate,
        int bookId,
        int readerId)
    {
        var validateDueDateResult = ValidateDueDate(dueDate, borrowedDate);

        if (validateDueDateResult.IsError)
        {
            return validateDueDateResult.FirstError;
        }

        return new Loan(
            s_identity++,
            borrowedDate,
            dueDate,
            bookId,
            readerId);
    }

    public void ReturnBook()
    {
        ReturnDate = DateTime.UtcNow;
    }

    public Loan WithBook(Book book)
    {
        Book = book;

        return this;
    }

    public Loan WithReader(Reader reader)
    {
        Reader = reader;

        return this;
    }

    private static ErrorOr<Success> ValidateDueDate(DateTime dueDate, DateTime borrowedDate)
    {
        if (dueDate < borrowedDate)
        {
            return Errors.Loan.InvalidDueDate;
        }

        return Result.Success;
    }
}
