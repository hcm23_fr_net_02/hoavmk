namespace LibraryManagement.Infrastructure.Constants;

public static class DataPaths
{
    public const string BookDataPath = "Mocks/books.json";

    public const string ReaderDataPath = "Mocks/readers.json";

    public const string BookRatingDataPath = "Mocks/book-ratings.json";

    public const string LoanDataPath = "Mocks/loans.json";
}
