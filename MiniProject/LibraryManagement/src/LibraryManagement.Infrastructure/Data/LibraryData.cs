using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Infrastructure.Data;

public sealed class LibraryData : IApplicationData
{
    private readonly static LibraryData s_instance = new();

    public List<Book> Books { get; set; } = new();

    public List<Reader> Readers { get; set; } = new();

    public List<BookRating> BookRatings { get; set; } = new();

    public List<Loan> Loans { get; set; } = new();

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static LibraryData()
    {
    }

    private LibraryData()
    {
    }

    public static LibraryData Instance => s_instance;
}
