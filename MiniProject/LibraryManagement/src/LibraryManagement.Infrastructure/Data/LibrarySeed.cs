using System.Reflection;
using System.Text.Json;
using LibraryManagement.Data.JsonConverters;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Infrastructure.Constants;

namespace LibraryManagement.Infrastructure.Data;

public static class LibrarySeed
{
    private static readonly JsonSerializerOptions s_jsonSerializerOptions = new()
    {
        PropertyNameCaseInsensitive = true
    };

    public static async Task SeedAsync()
    {
        var libraryData = LibraryData.Instance;

        s_jsonSerializerOptions.Converters.Add(new DateTimeJsonConverter());
        
        if (libraryData.Books.Count is 0)
        {
            var books = await GetPreConfiguredBooksAsync();

            if (books is not null)
            {
                libraryData.Books = books;
            }
        }
        else
        {
            Console.WriteLine("Books already contained data.");
        }

        if (libraryData.Readers.Count is 0)
        {
            var readers = await GetPreConfiguredReadersAsync();

            if (readers is not null)
            {
                libraryData.Readers = readers;
            }
        }
        else
        {
            Console.WriteLine("Readers already contained data.");
        }

        if (libraryData.BookRatings.Count is 0)
        {
            var bookRatings = await GetPreConfiguredBookRatingAsync();

            if (bookRatings is not null)
            {
                libraryData.BookRatings = bookRatings;
            }
        }
        else
        {
            Console.WriteLine("BookRatings already contained data.");
        }

        if (libraryData.Loans.Count is 0)
        {
            var loans = await GetPreConfiguredLoansAsync();

            if (loans is not null)
            {
                libraryData.Loans = loans;
            }
        }
        else
        {
            Console.WriteLine("Loans already contained data.");
        }
    }

    private static async Task<List<Book>?> GetPreConfiguredBooksAsync()
    {
        try
        {
            using FileStream openStream = File.OpenRead(DataPaths.BookDataPath);


            return await JsonSerializer.DeserializeAsync<List<Book>>(
                openStream, s_jsonSerializerOptions);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);

            return null;
        }
    }

    private static async Task<List<Reader>?> GetPreConfiguredReadersAsync()
    {
        try
        {
            using FileStream openStream = File.OpenRead(DataPaths.ReaderDataPath);

            return await JsonSerializer.DeserializeAsync<List<Reader>>(
                openStream, s_jsonSerializerOptions);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);

            return null;
        }
    }

    public static async Task<List<BookRating>?> GetPreConfiguredBookRatingAsync()
    {
        try
        {
            using FileStream openStream = File.OpenRead(DataPaths.BookRatingDataPath);

            return await JsonSerializer.DeserializeAsync<List<BookRating>>(
                openStream, s_jsonSerializerOptions);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);

            return null;
        }
    }

    public static async Task<List<Loan>?> GetPreConfiguredLoansAsync()
    {
        try
        {
            using FileStream openStream = File.OpenRead(DataPaths.LoanDataPath);

            return await JsonSerializer.DeserializeAsync<List<Loan>>(
                openStream, s_jsonSerializerOptions);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);

            return null;
        }
    }
}
