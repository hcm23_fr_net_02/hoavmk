using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Data;
using LibraryManagement.Infrastructure.Helpers;

namespace LibraryManagement.Infrastructure.Repositories;

public class ReaderRepository : IReaderRepository
{
    private readonly IApplicationData _applicationData;

    public ReaderRepository(IApplicationData libraryData)
    {
        _applicationData = libraryData;
    }

    public void Add(Reader reader)
    {
        _applicationData.Readers.Add(reader);
    }

    public void Delete(Reader reader)
    {
        _applicationData.Readers.Remove(reader);
    }

    public Reader? GetById(int id)
    {
        return _applicationData.Readers.Find(x => x.Id == id);
    }

    public PageResult<Reader> GetReadersListPage(int page, int pageSize)
    {
        return Paging<Reader>.GetListPage(_applicationData.Readers, page, pageSize);
    }
}
