using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Data;
using LibraryManagement.Infrastructure.Helpers;

namespace LibraryManagement.Infrastructure.Repositories;

public class LoanRepository : ILoanRepository
{
    private readonly IApplicationData _applicationData;

    public LoanRepository(IApplicationData applicationData)
    {
        _applicationData = applicationData;
    }

    public void Add(Loan loan)
    {
        _applicationData.Loans.Add(loan);
    }

    public bool IsAnyLoansWithReader(int readerId)
    {
        return _applicationData.Loans.Any(x => x.ReaderId == readerId);
    }

    public bool IsAnyLoansWithBook(int bookId)
    {
        return _applicationData.Loans.Any(x => x.BookId == bookId);
    }

    public Loan? GetLoanById(int LoanId)
    {
        return _applicationData.Loans.Find(x => x.Id == LoanId);
    }

    public PageResult<Loan> GetLoansByBookTitle(string? bookTitle, int page, int pageSize)
    {
        var loans = _applicationData.Loans
            .Join(
                _applicationData.Readers,
                loan => loan.ReaderId,
                reader => reader.Id,
                (loan, reader) => loan.WithReader(reader))
            .Join(
                _applicationData.Books,
                loan => loan.BookId,
                book => book.Id,
                (loan, book) => loan.WithBook(book));
        
        var filteredLoans = loans.Where(Loan =>
            bookTitle is null
            || Loan.Book!.Title.ToLower()
                .Contains(bookTitle.ToLower()));
        
        return Paging<Loan>.GetListPage(filteredLoans, page, pageSize);
    }

    public PageResult<Loan> GetLoansByCustomerName(
        string? readerName,
        int page,
        int pageSize)
    {
        var loans = _applicationData.Loans
            .Join(
                _applicationData.Readers,
                loan => loan.ReaderId,
                reader => reader.Id,
                (loan, reader) => loan.WithReader(reader))
            .Join(
                _applicationData.Books,
                loan => loan.BookId,
                book => book.Id,
                (loan, book) => loan.WithBook(book));
        
        var filteredLoans = loans.Where(loan =>
            readerName is null
            || loan.Reader!.FullName.ToLower()
                .Contains(readerName.ToLower()));
        
        return Paging<Loan>.GetListPage(filteredLoans, page, pageSize);
    }

    public PageResult<Loan> GetLoansWithBookAndCustomerListPage(
        int page,
        int pageSize)
    {
        var loans = _applicationData.Loans
            .Join(
                _applicationData.Books,
                loan => loan.BookId,
                book => book.Id,
                (loan, book) => loan.WithBook(book))
            .Join(
                _applicationData.Readers,
                loan => loan.ReaderId,
                reader => reader.Id,
                (loan, reader) => loan.WithReader(reader))
            .ToList();

        return Paging<Loan>.GetListPage(loans, page, pageSize);
    }
}
