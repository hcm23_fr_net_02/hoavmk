using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Data;

namespace LibraryManagement.Infrastructure.Repositories;

public class BookRatingRepository : IBookRatingRepository
{
    private readonly IApplicationData _applicationData;

    public BookRatingRepository(IApplicationData applicationData)
    {
        _applicationData = applicationData;
    }

    public void Add(BookRating bookRating)
    {
        _applicationData.BookRatings.Add(bookRating);
    }

    public void Delete(BookRating bookRating)
    {
        _applicationData.BookRatings.Remove(bookRating);
    }

    public List<BookRating> GetBookRatings()
    {
        return _applicationData.BookRatings.ToList();
    }

    public List<BookRating> GetBookRatingsByBookId(int bookId)
    {
        return _applicationData.BookRatings
            .Where(r => r.BookId == bookId)
            .ToList();
    }

    public BookRating? GetById(int id)
    {
        return _applicationData.BookRatings.Find(r => r.Id == id);
    }
}
