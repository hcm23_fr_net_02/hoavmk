using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Data;
using LibraryManagement.Infrastructure.Helpers;

namespace LibraryManagement.Infrastructure.Repositories;

public class BookRepository : IBookRepository
{
    private readonly IApplicationData _applicationData;

    public BookRepository(IApplicationData applicationData)
    {
        _applicationData = applicationData;
    }

    public void Add(Book book)
    {
        _applicationData.Books.Add(book);
    }

    public void Delete(Book book)
    {
        _applicationData.Books.Remove(book);
    }

    public PageResult<Book> FilterBooks(
        string? title,
        string? author,
        int? publicationYear,
        string? genre,
        int page,
        int pageSize)
    {
        IEnumerable<Book> query = _applicationData.Books
            .Where(book =>
                title is null
                || book.Title.ToLower().Contains(title.ToLower()))
            .Where(book =>
                author is null
                || book.Author.ToLower().Contains(author.ToLower()))
            .Where(book =>
                publicationYear is null
                || book.PublicationYear == publicationYear)
            .Where(book =>
                genre is null
                || book.Genre.ToLower().Contains(genre.ToLower()));

        return Paging<Book>.GetListPage(query, page, pageSize);
    }

    public PageResult<Book> GetBooksListPage(int page, int pageSize)
    {
        return Paging<Book>.GetListPage(_applicationData.Books, page, pageSize);
    }

    public Book? GetById(int id)
    {
        return _applicationData.Books.Find(x => x.Id == id);
    }
}
