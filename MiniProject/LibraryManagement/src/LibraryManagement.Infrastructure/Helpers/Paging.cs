using LibraryManagement.Domain.Common.Contracts;

namespace LibraryManagement.Infrastructure.Helpers;

public static class Paging<T> where T : class
{
    public static PageResult<T> GetListPage(IEnumerable<T> items, int page, int pageSize)
    {
        var totalItems = items.Count();
        var totalPages = totalItems / pageSize;

        if (totalItems % pageSize is not 0)
        {
            totalPages++;
        }

        var pagedItems = items.Skip((page - 1) * pageSize)
            .Take(pageSize)
            .ToList();

        return new PageResult<T>(pagedItems, page, pageSize, totalPages);
    }
}
