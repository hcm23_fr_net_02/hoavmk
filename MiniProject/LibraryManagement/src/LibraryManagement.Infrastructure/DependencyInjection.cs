using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Data;
using LibraryManagement.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryManagement.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        services.AddSingleton<IApplicationData>(LibraryData.Instance);

        services.AddScoped<IBookRepository, BookRepository>();
        services.AddScoped<IBookRatingRepository, BookRatingRepository>();
        services.AddScoped<IReaderRepository, ReaderRepository>();
        services.AddScoped<ILoanRepository, LoanRepository>();

        LibrarySeed.SeedAsync().GetAwaiter().GetResult();

        return services;
    }
}
