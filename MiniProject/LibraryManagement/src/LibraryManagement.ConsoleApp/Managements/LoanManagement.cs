using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.ConsoleApp.Helpers;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.ConsoleApp.Common.Constants;
using LibraryManagement.ConsoleApp.Common.Messages;

namespace LibraryManagement.ConsoleApp.Managements;

public class LoanManagement
{
    private readonly ILoanService _loanService;

    public LoanManagement(ILoanService loanService)
    {
        _loanService = loanService;
    }

    public void ManageLoans()
    {
        string? choice;
        var currentPage = 1;
        var exit = false;

        while (!exit)
        {
            Console.WriteLine(MenuHelper.GetPageTitle(typeof(Loan).Name, currentPage));

            var pageResult = _loanService.GetLoansListPage(
                currentPage,
                PageOptions.PageSize);
            
            var printFormat = "{0, -4} {1, -7} {2, -30} {3, -12} {4, -30}";

            Console.WriteLine(
                printFormat,
                LoanManagementMenu.IdColumn,
                LoanManagementMenu.BookIdColumn,
                LoanManagementMenu.BookTitleColumn,
                LoanManagementMenu.ReaderIdColumn,
                LoanManagementMenu.ReaderNameColumn);
                
            Console.WriteLine(new string('-', 100));

            Console.WriteLine(pageResult.Items.Count);

            foreach (var loan in pageResult.Items)
            {
                Console.WriteLine(printFormat, loan.Id, loan.BookId,
                    loan.Book!.Title.Length > 20
                    ? string.Concat(loan.Book.Title.AsSpan(0, 20), "...")
                    : loan.Book!.Title,
                    loan.Reader!.Id, loan.Reader!.FullName);
            }

            var hasNextPage = pageResult.CurrentPage < pageResult.TotalPages;
            var hasPreviousPage = pageResult.CurrentPage > 1;

            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPage);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPage);
            }

            Console.WriteLine(LoanManagementMenu.FindBorrowedBook);
            Console.WriteLine(CommonOptions.Back);
            Console.Write(LoanManagementMenu.SelectOption);

            choice = Console.ReadLine();

            if (choice is PageOptions.FirstNextPageOption
                or PageOptions.SecondNextPageOption)
            {
                if (hasNextPage)
                {
                    currentPage++;
                }
            }
            else if (choice is PageOptions.FirstPreviousPageOption
                or PageOptions.SecondPreviousPageOption)
            {
                if (hasPreviousPage)
                {
                    currentPage--;
                }
            }
            else if (choice is LoanManagementMenu.FirstFindBorrowedBookOption
                or LoanManagementMenu.SecondFindBorrowedBookOption)
            {
                FindLoans();
            }
            else if (choice is CommonOptions.FirstBackOption
                or CommonOptions.SecondBackOption)
            {
                return;
            }
            else if (int.TryParse(choice, out var borrowedId))
            {
                var returnResult = _loanService.ReturnBook(borrowedId);

                if (returnResult.IsError)
                {
                    Console.WriteLine(returnResult.FirstError.Description);
                }
                else
                {
                    Console.WriteLine($"Returned borrowed book with id = {borrowedId}");
                }
            }
            else
            {
                Console.WriteLine(CommonOptions.InvalidOption);
            }
        }
    }

    private void FindLoans()
    {
        Console.Clear();

        while (true)
        {    
            Console.WriteLine(LoanManagementMenu.FindLoans);
            Console.WriteLine(LoanManagementMenu.FindByReaderName);
            Console.WriteLine(LoanManagementMenu.FindByBookTitle);
            Console.WriteLine(CommonOptions.Back);
            Console.Write(LoanManagementMenu.SelectFindOption);

            string? choice = Console.ReadLine();

            switch (choice)
            {
                case LoanManagementMenu.FirstFindByReaderNameOption:
                case LoanManagementMenu.SecondFindByReaderNameOption:
                    FindLoansBy(
                        LoanManagementMenu.ReaderIdColumn,
                        _loanService.GetLoansByCustomerName);

                    break;

                case LoanManagementMenu.FirstFindByBookTitleOption:
                case LoanManagementMenu.SecondFindByBookTitleOption:
                    FindLoansBy(
                        LoanManagementMenu.BookTitle,
                        _loanService.GetLoansByBookTitle);

                    break;

                case CommonOptions.FirstBackOption:
                case CommonOptions.SecondBackOption:
                    return;

                default:
                    Console.WriteLine(CommonOptions.InvalidOption);
                    break;
            }
        }
    }

    private void FindLoansBy(
        string fieldName,
        Func<string?, int, int, PageResult<Loan>> filter)
    {
        var fieldValue = InputHelper.EnterOptionalTextField(fieldName);
        var currentPage = 1;

        while (true)
        {
            Console.WriteLine(MenuHelper.GetPageTitle(LoanManagementMenu.FilteredLoanTitle, currentPage));
            var pageResult = filter(fieldValue, currentPage, PageOptions.PageSize);
            
            if (pageResult.Items.Count is 0)
            {
                Console.WriteLine(LoanMessages.LoanNotFound(fieldName, fieldValue));
                
                return;
            }

            var printFormat = "{0, -4} {1, -7} {2, -30} {3, -12} {4, -30}";

            Console.WriteLine(
                printFormat,
                LoanManagementMenu.IdColumn,
                LoanManagementMenu.BookIdColumn,
                LoanManagementMenu.BookTitleColumn,
                LoanManagementMenu.ReaderIdColumn,
                LoanManagementMenu.ReaderNameColumn);

            foreach (var loan in pageResult.Items)
            {
                Console.WriteLine(printFormat, loan.Id, loan.BookId,
                    loan.Book!.Title.Length > 20
                    ? string.Concat(loan.Book.Title.AsSpan(0, 20), "...")
                    : loan.Book!.Title,
                    loan.Reader!.Id, loan.Reader!.FullName);
            }

            var hasNextPage = pageResult.CurrentPage < pageResult.TotalPages;
            var hasPreviousPage = pageResult.CurrentPage > 1;

            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPage);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPage);
            }

            Console.WriteLine(CommonOptions.Back);
            Console.Write(LoanManagementMenu.SelectPageOption);

            string? choice = Console.ReadLine();

            switch (choice)
            {
                case PageOptions.FirstNextPageOption:
                case PageOptions.SecondNextPageOption:
                    if (hasNextPage)
                    {
                        currentPage++;
                    }

                    break;

                case PageOptions.FirstPreviousPageOption:
                case PageOptions.SecondPreviousPageOption:
                    if (hasPreviousPage)
                    {
                        currentPage--;
                    }

                    break;
                case CommonOptions.FirstBackOption:
                case CommonOptions.SecondBackOption:
                    return;

                default:
                    Console.WriteLine(CommonOptions.InvalidOption);
                    break;
            }
        }
    }
}
