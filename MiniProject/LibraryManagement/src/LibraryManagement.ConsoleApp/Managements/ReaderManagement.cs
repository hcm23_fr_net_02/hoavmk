using LibraryManagement.ConsoleApp.Helpers;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Domain.Entities;
using LibraryManagement.ConsoleApp.Common.Constants;

namespace LibraryManagement.ConsoleApp.Managements;

public class ReaderManagement
{
    private readonly IReaderService _readerService;

    public ReaderManagement(IReaderService readerService)
    {
        _readerService = readerService;
    }

    public void ManageReaders()
    {
        string? choice;
        var currentPage = 1;
        var exit = false;

        while (!exit)
        {
            Console.Clear();
            Console.WriteLine(MenuHelper.GetPageTitle(typeof(Reader).Name, currentPage));

            var pageResult = _readerService.GetReadersListPage(
                currentPage,
                PageOptions.PageSize);
            
            var printFormat = "{0, -4} {1, -30} {2, -20} {3, -20}";

            Console.WriteLine(
                printFormat,
                ReaderFields.Id,
                ReaderFields.FullName,
                ReaderFields.PhoneNumber,
                ReaderFields.Address);

            Console.WriteLine(new string('-', 100));

            foreach (var reader in pageResult.Items)
            {
                Console.WriteLine(printFormat, reader.Id, reader.FullName,
                    reader.PhoneNumber, reader.Address);
            }

            var hasNextPage = pageResult.CurrentPage < pageResult.TotalPages;
            var hasPreviousPage = pageResult.CurrentPage > 1;

            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPage);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPage);
            }

            Console.WriteLine(ReaderManagementMenu.AddReader);
            Console.WriteLine(ReaderManagementMenu.DeleteReader);
            Console.WriteLine(ReaderManagementMenu.EditReader);
            Console.WriteLine(CommonOptions.Back);
            Console.Write(ReaderManagementMenu.SelectOption);

            choice = Console.ReadLine();

            switch (choice)
            {
                case PageOptions.FirstNextPageOption:
                case PageOptions.SecondNextPageOption:
                    if (hasNextPage)
                    {
                        currentPage++;
                    }

                    break;

                case PageOptions.FirstPreviousPageOption:
                case PageOptions.SecondPreviousPageOption:
                    if (hasPreviousPage)
                    {
                        currentPage--;
                    }

                    break;

                case ReaderManagementMenu.FirstAddReaderOption:
                case ReaderManagementMenu.SecondAddReaderOption:
                    var added = AddReader();

                    if (added)
                    {
                        currentPage = 1;
                    }

                    break;

                case ReaderManagementMenu.FirstEditReaderOption:
                case ReaderManagementMenu.SecondEditReaderOption:
                    var edited = EditReader();

                    if (edited)
                    {
                        currentPage = 1;
                    }

                    break;

                case ReaderManagementMenu.FirstDeleteReaderOption:
                case ReaderManagementMenu.SecondDeleteReaderOption:
                    var deleted = DeleteReader();

                    if (deleted)
                    {
                        currentPage = 1;
                    }

                    break;

                case CommonOptions.FirstBackOption:
                case CommonOptions.SecondBackOption:
                    return;

                default:
                    Console.WriteLine(CommonOptions.InvalidOption);
                    break;
            }
        }
    }

    private bool AddReader()
    {
        Console.WriteLine(ReaderManagementMenu.AddReaderTitle);

        var fullName = InputHelper.EnterTextField(
            ReaderFields.FullName,
            MenuHelper.GetRequiredMessage(ReaderFields.FullName));

        var phoneNumber = InputHelper.EnterTextField(
            ReaderFields.PhoneNumber,
            MenuHelper.GetRequiredMessage(ReaderFields.PhoneNumber));

        var address = InputHelper.EnterTextField(
            ReaderFields.Address,
            MenuHelper.GetRequiredMessage(ReaderFields.Address));
            
        var addResult = _readerService.AddReader(fullName, phoneNumber, address);

        if (addResult.IsError)
        {
            Console.WriteLine(addResult.FirstError.Description);

            return false;
        }

        return true;
    }

    private bool EditReader()
    {
        Console.WriteLine(ReaderManagementMenu.EditReader);

        var readerId = InputHelper.EnterNumberField(ReaderFields.ReaderId);
        var fullName = InputHelper.EnterTextField(
            ReaderFields.FullName,
            MenuHelper.GetRequiredMessage(ReaderFields.FullName));

        var phoneNumber = InputHelper.EnterTextField(
            ReaderFields.PhoneNumber,
            MenuHelper.GetRequiredMessage(ReaderFields.PhoneNumber));

        var address = InputHelper.EnterTextField(
            ReaderFields.Address,
            MenuHelper.GetRequiredMessage(ReaderFields.Address));

        var editResult = _readerService.UpdateReader(
            readerId,
            fullName,
            phoneNumber,
            address);
        
        if (editResult.IsError)
        {
            Console.WriteLine(editResult.FirstError.Description);

            return false;
        }

        return true;
    }

    private bool DeleteReader()
    {
        Console.WriteLine(ReaderManagementMenu.DeleteReader);

        var readerId = InputHelper.EnterNumberField(ReaderFields.ReaderId);
        var deleteResult = _readerService.DeleteReader(readerId);

        if (deleteResult.IsError)
        {
            Console.WriteLine(deleteResult.FirstError.Description);

            return false;
        }

        return true;
    }
}
