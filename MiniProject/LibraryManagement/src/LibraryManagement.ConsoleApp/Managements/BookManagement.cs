using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.ConsoleApp.Common.Constants;
using LibraryManagement.ConsoleApp.Common.ErrorMessages;
using LibraryManagement.ConsoleApp.Helpers;
using LibraryManagement.Contracts.BookRatings;
using LibraryManagement.Contracts.Books;
using LibraryManagement.Domain.Entities;

namespace LibraryManagement.ConsoleApp.Managements;

public class BookManagement
{
    private readonly IBookService _bookService;
    private readonly IBookRatingService _bookRatingService;

    public BookManagement(
        IBookService bookService,
        IBookRatingService bookRatingService)
    {
        _bookService = bookService;
        _bookRatingService = bookRatingService;
    }

    public void ManageBooks()
    {
        string? choice;
        var currentPage = 1;

        while (true)
        {
            Console.Clear();
            Console.WriteLine(MenuHelper.GetPageTitle(typeof(Book).Name, currentPage));

            var pageResult = _bookService.GetBooksListPage(
                currentPage,
                PageOptions.PageSize);

            var printFormat = "{0, -4} {1, -30}";

            Console.WriteLine(printFormat, BookFields.Id, BookFields.Title);
            Console.WriteLine(new string('-', 100));

            foreach (var book in pageResult.Items)
            {
                Console.WriteLine(printFormat, book.Id, book.Title);
            }

            var hasNextPage = pageResult.CurrentPage < pageResult.TotalPages;
            var hasPreviousPage = pageResult.CurrentPage > 1;

            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPage);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPage);
            }

            Console.WriteLine(CommonOptions.Back);
            Console.Write(MenuHelper.GetSelectBookMenuOptionMessage(
                hasNextPage,
                hasPreviousPage));

            choice = Console.ReadLine();

            if (choice is PageOptions.FirstNextPageOption
                or PageOptions.SecondNextPageOption)
            {
                if (hasNextPage)
                {
                    currentPage++;
                }
            }
            else if (choice is PageOptions.FirstPreviousPageOption
                or PageOptions.SecondPreviousPageOption)
            {
                if (hasPreviousPage)
                {
                    currentPage--;
                }
            }
            else if (choice is CommonOptions.FirstBackOption
                or CommonOptions.SecondBackOption)
            {
                return;
            }
            else
            {
                if (int.TryParse(choice, out var bookId))
                {
                    Console.Clear();
                    var getBookResult = _bookService.GetBookById(bookId);

                    if (getBookResult.IsError)
                    {
                        Console.WriteLine(getBookResult.FirstError.Description);
                        continue;
                    }

                    var book = getBookResult.Value;
                    
                    Console.WriteLine(BookManagementMenu.BookDetailTitle);
                    Console.WriteLine($"{BookFields.Id}: {book.Id}");
                    Console.WriteLine($"{BookFields.Title}: {book.Title}");
                    Console.WriteLine($"{BookFields.Author}: {book.Author}");
                    Console.WriteLine($"{BookFields.PublicationYear}: {book.PublicationYear}");
                    Console.WriteLine($"{BookFields.Quantity}: {book.Quantity}");
                    Console.WriteLine($"{BookFields.Genre}: {book.Genre}");
                    Console.WriteLine($"{BookFields.Description}: {book.Description}");

                    string? bookDetailsChoice;

                    do
                    {
                        Console.WriteLine();
                        Console.WriteLine(BookManagementMenu.EditBook);
                        Console.WriteLine(BookManagementMenu.DeleteBook);
                        Console.WriteLine(BookManagementMenu.AddRating);
                        Console.WriteLine(BookManagementMenu.ViewRatings);
                        Console.WriteLine(BookManagementMenu.BorrowBook);
                        Console.WriteLine(CommonOptions.Back);
                        Console.Write(BookManagementMenu.OptionSelection);
                        bookDetailsChoice = Console.ReadLine();

                        switch (bookDetailsChoice)
                        {
                            case BookManagementMenu.FirstEditBookOption:
                            case BookManagementMenu.SecondEditBookOption:
                                EditBook(book.Id);
                                break;

                            case BookManagementMenu.FirstDeleteBookOption:
                            case BookManagementMenu.SecondDeleteBookOption:
                                DeleteBook();
                                break;

                            case BookManagementMenu.FirstAddRatingOption:
                            case BookManagementMenu.SecondAddRatingOption:
                                AddRating(book.Id);
                                break;

                            case BookManagementMenu.FirstViewRatingsOption:
                            case BookManagementMenu.SecondViewRatingsOption:
                                ShowBookRatings(book.Id);
                                break;

                            case BookManagementMenu.FirstBorrowBookOption:
                            case BookManagementMenu.SecondBorrowBookOption:
                                BorrowBook(book.Id);
                                break;

                            case CommonOptions.FirstBackOption:
                            case CommonOptions.SecondBackOption:
                                return;

                            default:
                                Console.WriteLine(CommonOptions.InvalidOption);
                                break;
                        }
                    }
                    while (bookDetailsChoice is not CommonOptions.FirstBackOption
                        or CommonOptions.SecondBackOption);
                }
                else
                {
                    Console.WriteLine(CommonOptions.InvalidOption);
                }
            }
        }
    }

    private void AddRating(int id)
    {
        Console.Clear();
        Console.WriteLine(BookManagementMenu.AddRatingTitle);

        var title = InputHelper.EnterTextField(
            BookRatingFields.Title,
            MenuHelper.GetRequiredMessage(
                BookRatingFields.Title));

        var content = InputHelper.EnterTextField(
            BookRatingFields.Content,
            MenuHelper.GetRequiredMessage(
                BookRatingFields.Content));

        int rating = EnterRatingNumber();
        var ratingRequest = new BookRatingRequest(
            Title: title,
            Content: content,
            Rating: rating,
            BookId: id);

        var addResult = _bookRatingService.AddBookRating(ratingRequest);

        if (addResult.IsError)
        {
            foreach (var error in addResult.Errors)
            {
                Console.WriteLine(error.Description);
            }

            return;
        }

        Console.WriteLine(BookMessages.CannotAddRating);
    }

    public void FilterBooks()
    {
        Console.Clear();
        Console.WriteLine(BookManagementMenu.FilterBooksTitle);

        var title = InputHelper.EnterOptionalTextField(BookFields.Title);
        var author = InputHelper.EnterOptionalTextField(BookFields.Author);
        var publicationYear = EnterPublicationYearForFilter();
        var genre = InputHelper.EnterOptionalTextField(BookFields.Genre);
        int currentPage = 1;
        var exit = false;

        while (!exit)
        {
            var filterRequest = new FilterBookRequest(
                title,
                author,
                publicationYear,
                genre,
                currentPage,
                PageOptions.PageSize);

            var result = _bookService.FilterBooks(filterRequest);
            var printFormat = "{0, -4} {1, -80} {2, -25} {3, -20} {4, -25}";

            Console.WriteLine(BookManagementMenu.FilterBooksResultTitle);
            Console.WriteLine(
                printFormat,
                BookFields.Id,
                BookFields.Title,
                BookFields.Author,
                BookFields.PublicationYear,
                BookFields.Genre);

            foreach (var book in result.Items)
            {
                Console.WriteLine(
                    printFormat,
                    book.Id,
                    book.Title,
                    book.Author,
                    book.PublicationYear,
                    book.Genre);
            }

            Console.WriteLine();

            bool hasNextPage = result.CurrentPage < result.TotalPages;
            bool hasPreviousPage = result.CurrentPage > 1;
            
            if (hasNextPage)
            {
                Console.WriteLine(PageOptions.NextPage);
            }

            if (hasPreviousPage)
            {
                Console.WriteLine(PageOptions.PreviousPage);
            }

            Console.WriteLine(CommonOptions.Back);
            Console.Write(BookManagementMenu.SelectionFilterResultOption);
            
            string? choice = Console.ReadLine();

            switch (choice)
            {
                case PageOptions.FirstNextPageOption:
                case PageOptions.SecondNextPageOption:
                    if (hasNextPage)
                    {
                        currentPage++;
                    }
                    
                    break;

                case PageOptions.FirstPreviousPageOption:
                case PageOptions.SecondPreviousPageOption:
                    if (hasPreviousPage)
                    {
                        currentPage--;
                    }

                    break;

                case CommonOptions.FirstBackOption:
                case CommonOptions.SecondBackOption:
                    return;
                
                default:
                    Console.WriteLine(CommonOptions.InvalidOption);
                    break;
            }
        }
    }

    public void AddBook()
    {
        Console.WriteLine(BookManagementMenu.AddBookTitle);

        var title = InputHelper.EnterTextField(
            BookFields.Title,
            MenuHelper.GetRequiredMessage(BookFields.Title));

        var author = InputHelper.EnterTextField(
            BookFields.Author,
            MenuHelper.GetRequiredMessage(BookFields.Author));

        int publicationYear = EnterPublicationYear();
        int quantity = EnterStockQuantity();
        var genre = InputHelper.EnterTextField(
            BookFields.Genre,
            MenuHelper.GetRequiredMessage(BookFields.Genre));

        var description = InputHelper.EnterTextField(
            BookFields.Description,
            MenuHelper.GetRequiredMessage(BookFields.Description));

        var bookRequest = new BookRequest(
            title,
            author,
            publicationYear,
            quantity,
            genre,
            description);

        var addResult = _bookService.AddBook(bookRequest);

        if (addResult.IsError)
        {
            foreach (var error in addResult.Errors)
            {
                Console.WriteLine(error.Description);
            }

            return;
        }

        Console.WriteLine(BookMessages.CannotAddBook);
    }

    private void EditBook(int id)
    {
        Console.Clear();
        Console.WriteLine(BookManagementMenu.EditBookTitle);

        var title = InputHelper.EnterTextField(
            BookFields.Title,
            MenuHelper.GetRequiredMessage(BookFields.Title));

        var author = InputHelper.EnterTextField(
            BookFields.Author,
            MenuHelper.GetRequiredMessage(BookFields.Author));

        int publicationYear = EnterPublicationYear();
        int quantity = EnterStockQuantity();
        var genre = InputHelper.EnterTextField(
            BookFields.Genre,
            MenuHelper.GetRequiredMessage(BookFields.Genre));

        var description = InputHelper.EnterTextField(
            BookFields.Description,
            MenuHelper.GetRequiredMessage(BookFields.Description));

        var bookRequest = new BookRequest(
            title,
            author,
            publicationYear,
            quantity,
            genre,
            description);

        var editResult = _bookService.EditBook(id, bookRequest);

        if (editResult.IsError)
        {
            foreach (var error in editResult.Errors)
            {
                Console.WriteLine(error.Description);
            }

            return;
        }

        Console.WriteLine(BookMessages.CannotEditBook(id));
    }

    private void ShowBookRatings(int bookId)
    {
        Console.WriteLine(BookManagementMenu.BookRatingsTitle);

        var getBookResult = _bookService.GetBookWithRatingsByBookId(bookId);

        if (getBookResult.IsError)
        {
            Console.WriteLine(getBookResult.FirstError.Description);

            return;
        }

        var book = getBookResult.Value;

        if (book.Ratings.Count is 0)
        {
            Console.WriteLine(BookMessages.NoRatings);

            return;
        }

        var printFormat = "{0, -4} {1, -16} {2, -50} {3, -100}";

        Console.WriteLine(
            printFormat,
            BookRatingFields.Id,
            BookRatingFields.Rating,
            BookRatingFields.Title,
            BookRatingFields.Content);

        foreach (var rating in book.Ratings)
        {
            Console.WriteLine(
                printFormat,
                rating.Id,
                rating.Rating,
                rating.Title,
                rating.Content);
        }

        Console.WriteLine();
        Console.WriteLine(CommonOptions.Back);
        
        string? input;

        while (true)
        {
            Console.Write(CommonOptions.SelectionBack);
            input = Console.ReadLine();

            if (input is CommonOptions.FirstBackOption or CommonOptions.SecondBackOption)
            {
                return;
            }
        }
    }

    private void DeleteBook()
    {
        Console.Clear();
        Console.WriteLine(BookManagementMenu.DeleteBookTitle);

        var bookId = InputHelper.EnterNumberField(BookFields.BookId);
        var getBookResult = _bookService.GetBookById(bookId);

        if (getBookResult.IsError)
        {
            Console.WriteLine(getBookResult.FirstError.Description);
            
            return;
        }

        string? answer;

        do
        {
            Console.Write(MenuHelper.GetConfirmDeleteBookMessage(bookId));;
            answer = Console.ReadLine();

            if (answer is CommonOptions.FirstYesOption or CommonOptions.SecondYesOption)
            {
                var deleteResult = _bookService.DeleteBook(bookId);

                if (deleteResult.IsError)
                {
                    Console.WriteLine(deleteResult.FirstError.Description);
                }
                else
                {
                    Console.WriteLine(BookMessages.DeletedBook(bookId));
                }

                return;
            }

            if (answer is CommonOptions.FirstNoOption or CommonOptions.SecondNoOption)
            {
                return;
            }
        }
        while (answer is not CommonOptions.FirstYesOption
            or CommonOptions.SecondYesOption
            or CommonOptions.FirstNoOption
            or CommonOptions.SecondNoOption);
    }

    private void BorrowBook(int bookId)
    {
        Console.Clear();
        Console.WriteLine(BookManagementMenu.BorrowBookTitle);

        var borrowedDate = EnterBorrowedDate();
        var dueDate = EnterDueDate(borrowedDate);
        var customerId = InputHelper.EnterNumberField(ReaderFields.ReaderId);
        var loanRequest = new LoanRequest(
            borrowedDate,
            dueDate,
            bookId,
            customerId);

        var borrowResult = _bookService.BorrowBook(loanRequest);

        if (borrowResult.IsError)
        {
            Console.WriteLine(borrowResult.FirstError.Description);

            return;
        }

        Console.WriteLine(BookMessages.CannotBorrowBook(bookId));
    }

    private static int EnterStockQuantity()
    {
        string? input;
        int quantity;
        bool parsedQuantity;

        do
        {
            Console.Write(BookMessages.EnterQuantity);
            input = Console.ReadLine();
            parsedQuantity = int.TryParse(input, out quantity);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(BookMessages.QuantityIsRequired);
            }
            else if (!parsedQuantity)
            {
                Console.WriteLine(BookMessages.InvalidQuantity);
            }
            else if (quantity < Book.MinQuantity)
            {
                Console.WriteLine(BookMessages.QuantityBelowMinimumValue);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedQuantity
        || quantity < Book.MinQuantity);

        return quantity;
    }

    private static int EnterPublicationYear()
    {
        string? input;
        int publicationYear;
        bool parsedPublicationYear;

        do
        {
            Console.Write(BookMessages.EnterPublicationYear);
            input = Console.ReadLine();
            parsedPublicationYear = int.TryParse(input, out publicationYear);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(BookMessages.RequiredPublicationYear);
            }
            else if (!parsedPublicationYear)
            {
                Console.WriteLine(BookMessages.InvalidPublicationYear);
            }
            else if (publicationYear < Book.MinPublicationYear
                || publicationYear > Book.MaxPublicationYear)
            {
                Console.WriteLine(BookMessages.InvalidPublicationYearRange);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedPublicationYear
        || publicationYear < Book.MinPublicationYear
        || publicationYear > Book.MaxPublicationYear);

        return publicationYear;
    }

    private static int EnterRatingNumber()
    {
        string? input;
        int rating;
        bool parsedRating;

        do
        {
            Console.Write(BookMessages.EnterRating);
            input = Console.ReadLine();
            parsedRating = int.TryParse(input, out rating);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(BookMessages.RequiredRating);
            }
            else if (!parsedRating)
            {
                Console.WriteLine(BookMessages.InvalidRating);
            }
            else if (rating < BookRating.MinRating || rating > BookRating.MaxRating)
            {
                Console.WriteLine(BookMessages.InvalidRatingRange);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedRating
        || rating < BookRating.MinRating
        || rating > BookRating.MaxRating);

        return rating;
    }

    private static DateTime EnterBorrowedDate()
    {
        string? input;
        DateTime borrowedDate;
        bool parsedBorrowedDate;

        do
        {
            Console.Write(BookMessages.EnterBorrowedDate);
            input = Console.ReadLine();
            parsedBorrowedDate = DateTime.TryParse(input, out borrowedDate);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(BookMessages.RequiredBorrowedDate);
            }
            else if (!parsedBorrowedDate)
            {
                Console.WriteLine(BookMessages.InvalidBorrowedDate);
            }
            else if (borrowedDate < DateTime.UtcNow)
            {
                Console.WriteLine(BookMessages.BorrowedLessThanCurrently);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedBorrowedDate
        || borrowedDate < DateTime.UtcNow);

        return borrowedDate;
    }

    private static int? EnterPublicationYearForFilter()
    {
        int publicationYear;
        bool parsedPublicationYear;
        string? input;

        do
        {
            Console.Write(BookMessages.EnterPublicationYear);
            input = Console.ReadLine();
            parsedPublicationYear = int.TryParse(input, out publicationYear);

            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            else if (!parsedPublicationYear)
            {
                Console.WriteLine(BookMessages.InvalidPublicationYear);
            }
            else  if (publicationYear < Book.MinPublicationYear
                    || publicationYear > Book.MaxPublicationYear)
            {
                Console.WriteLine(BookMessages.InvalidPublicationYearRange);
            }
        }
        while (!parsedPublicationYear
        || publicationYear < Book.MinPublicationYear
        || publicationYear > Book.MaxPublicationYear);

        return publicationYear;
    }

    private static DateTime EnterDueDate(DateTime borrowedDate)
    {
        string? input;
        DateTime dueDate;
        bool parsedDueDate;

        do
        {
            Console.Write(BookMessages.EnterDueDate);
            input = Console.ReadLine();
            parsedDueDate = DateTime.TryParse(input, out dueDate);

            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine(BookMessages.RequiredDueDate);
            }
            else if (!parsedDueDate)
            {
                Console.WriteLine(BookMessages.InvalidDueDate);
            }
            else if (dueDate <= borrowedDate)
            {
                Console.WriteLine(BookMessages.DueDateLessThanBorrowedDate);
            }
        }
        while (string.IsNullOrEmpty(input)
        || !parsedDueDate
        || dueDate <= borrowedDate);

        return dueDate;
    }
}
