namespace LibraryManagement.ConsoleApp.Helpers;

public static class MenuHelper
{
    public static string GetPageTitle(string title, int page)
        => $"**************{title} Page {page}**************";

    public static string GetSelectBookMenuOptionMessage(bool hasNextPage, bool hasPreviousPage)
        => $"Select an option (" + (hasNextPage ? "N/" : "")
                + (hasPreviousPage ? "P/" : "") + "B) or enter book's id to view book details: ";

    public static string GetRequiredMessage(string field)
        => $"{field} is required.";

    public static string GetConfirmDeleteBookMessage(int bookId)
        => $"Are you sure to delete the book with id = {bookId}? (Y/N): ";
}
