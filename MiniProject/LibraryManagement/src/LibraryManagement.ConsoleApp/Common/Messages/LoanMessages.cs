namespace LibraryManagement.ConsoleApp.Common.Messages;

public static class LoanMessages
{
    public static string LoanNotFound(string field, string? value)
        => $"There is no loan with {field} = {value}";
}
