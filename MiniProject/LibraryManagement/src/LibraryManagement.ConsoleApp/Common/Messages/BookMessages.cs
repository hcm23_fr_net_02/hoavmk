using LibraryManagement.Domain.Entities;

namespace LibraryManagement.ConsoleApp.Common.ErrorMessages;

public static class BookMessages
{
    public const string CannotAddRating = "Cannot add rating.";

    public const string CannotAddBook = "Cannot add book.";

    public static string CannotEditBook(int bookId)
        => $"Cannot edit the book with id = {bookId}";

    public const string NoRatings = "This book does not have any ratings.";

    public static string DeletedBook(int bookId)
        => $"Deleted the book with id = {bookId}.";

    public static string CannotBorrowBook(int bookId)
        => $"Cannot borrow the book with id = {bookId}.";

    public const string EnterQuantity = "Enter quantity: ";

    public const string QuantityIsRequired = "Quantity is required.";

    public const string InvalidQuantity = "Invalid quantity.";

    public static readonly string QuantityBelowMinimumValue = $"Quantity must be larger than or equal {Book.MinQuantity}";

    public static readonly string EnterPublicationYear = $"Enter publication year ({Book.MinPublicationYear} - {Book.MaxPublicationYear}): ";

    public const string RequiredPublicationYear = "Publication year is required.";

    public const string InvalidPublicationYear = "Invalid publication year.";

    public static readonly string InvalidPublicationYearRange = $"Publication year must be between "
                    + $"{Book.MinPublicationYear} and {Book.MaxPublicationYear}";

    public const string EnterRating = $"Enter rating: ";

    public const string RequiredRating = "Rating is required.";

    public const string InvalidRating = "Invalid rating.";

    public static readonly string InvalidRatingRange = $"Rating must be between {BookRating.MinRating} and {BookRating.MaxRating}";

    public const string EnterBorrowedDate = "Enter borrowed date (MM/dd/yyyy): ";

    public const string RequiredBorrowedDate = "Borrowed date is required.";

    public const string InvalidBorrowedDate = "Invalid borrowed date.";

    public const string BorrowedLessThanCurrently = $"Borrowed date must be larger than current date.";

    public const string EnterDueDate = "Enter due date (MM/dd/yyyy): ";

    public const string RequiredDueDate = "Due date is required.";

    public const string InvalidDueDate = "Invalid due date.";

    public const string DueDateLessThanBorrowedDate = $"Due date must be larger than borrowed date.";
}
