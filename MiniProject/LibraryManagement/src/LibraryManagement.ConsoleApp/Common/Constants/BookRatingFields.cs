namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class BookRatingFields
{
    public const string Id = "Id";

    public const string Title = "Title";

    public const string Content = "Content";

    public const string Rating = "Rating";

    public const string BookId = "BookId";
}
