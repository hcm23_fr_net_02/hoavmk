namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class AppMenu
{
    public const string Title = "******************Library Management App******************";

    public const string ViewBooks = "1. View books";

    public const string AddBook = "2. Add book";

    public const string FilterBooks = "3. Filter books";

    public const string ViewLoans = "4. View loans";

    public const string ManageReaders = "5. Manage readers";

    public const string Exit = "6. Exit";

    public const string OptionSelection = "Select an option (1/2/3/4/5/6): ";

    public const string ViewBooksOption = "1";

    public const string AddBookOption = "2";

    public const string FilterBooksOption = "3";

    public const string ManageBorrowedBooksOption = "4";

    public const string ManageReadersOption = "5";

    public const string ExitOption = "6";

    public const string InvalidOption = "Invalid Option!";
}

