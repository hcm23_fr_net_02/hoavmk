namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class BookFields
{
    public const string Id = "Id";

    public const string BookId = "book's Id";

    public const string Title = "Title";

    public const string Author = "Author";

    public const string PublicationYear = "PublicationYear";

    public const string Quantity = "Quantity";
    
    public const string Genre = "Genre";

    public const string Description = "Description";
}
