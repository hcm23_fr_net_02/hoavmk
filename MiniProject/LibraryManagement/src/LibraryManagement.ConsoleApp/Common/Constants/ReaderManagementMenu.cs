namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class ReaderManagementMenu
{
    public const string AddReaderTitle = "**************Add Reader**************";

    public const string EditReaderTitle = "**************Edit Reader**************";

    public const string DeleteReaderTitle = "**************Delete Reader**************";

    public const string AddReader = "A. Add new reader";

    public const string DeleteReader = "D. Delete reader";

    public const string EditReader = "E. Edit reader";

    public const string SelectOption = "Select an option (N/P/A/D/E/B): ";

    public const string FirstAddReaderOption = "A";

    public const string SecondAddReaderOption = "a";

    public const string FirstDeleteReaderOption = "D";

    public const string SecondDeleteReaderOption = "d";

    public const string FirstEditReaderOption = "E";

    public const string SecondEditReaderOption = "e";
}
