namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class BookManagementMenu
{
    public const string BookDetailTitle = "**************Book Details**************";

    public const string AddRatingTitle = "**************Add Rating**************";

    public const string FilterBooksTitle = "**************Filter Book**************";

    public const string FilterBooksResultTitle = "**************Filter Book**************";

    public const string AddBookTitle = "**************Add Book**************";

    public const string EditBookTitle = "**************Edit Book**************";

    public const string BookRatingsTitle = "**************Book Ratings**************";

    public const string DeleteBookTitle = "**************Delete Book**************";

    public const string BorrowBookTitle = "**************Borrow Book**************";

    public const string EditBook = "E. Edit book";

    public const string DeleteBook = "D. Delete book";

    public const string AddRating = "A. Add rating";

    public const string ViewRatings = "L. View book's ratings";

    public const string BorrowBook = "R. Borrow book";

    public const string Back = "B. Back";

    public const string OptionSelection = "Select an option (E/D/A/L/R/B): ";

    public const string FirstEditBookOption = "E";

    public const string SecondEditBookOption = "e";

    public const string FirstDeleteBookOption = "D";

    public const string SecondDeleteBookOption = "d";

    public const string FirstAddRatingOption = "A";

    public const string SecondAddRatingOption = "a";

    public const string FirstViewRatingsOption = "L";

    public const string SecondViewRatingsOption = "l";

    public const string FirstBorrowBookOption = "R";
    
    public const string SecondBorrowBookOption = "r";

    public const string SelectionFilterResultOption = "Select an option (N/P/B): ";
}
