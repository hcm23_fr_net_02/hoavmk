namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class ReaderFields
{
    public const string Id = "Id";

    public const string ReaderId = "reader's Id";

    public const string FullName = "Full Name";
    
    public const string PhoneNumber = "Phone Number";

    public const string Address = "Address";
}
