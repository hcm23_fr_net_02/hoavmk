namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class CommonOptions
{
    public const string InvalidOption = "Invalid option!";

    public const string Back = "B. Back";

    public const string SelectionBack = "Enter \"B\" to back: ";

    public const string FirstBackOption = "B";

    public const string SecondBackOption = "b";

    public const string FirstYesOption = "Y";

    public const string SecondYesOption = "y";

    public const string FirstNoOption = "N";

    public const string SecondNoOption = "n";
}
