namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class LoanManagementMenu
{
    public const string FindLoans = $"**************Find Loans**************";

    public const string FindBorrowedBook = "F. Find borrowed book";

    public const string SelectOption = "Select an option (N/P/F/B) or enter loan's id to return the book: ";

    public const string FirstFindBorrowedBookOption = "F";

    public const string SecondFindBorrowedBookOption = "f";

    public const string FindByReaderName = "R. Find by reader's name";

    public const string FindByBookTitle = "T. Find by book's title";

    public const string SelectFindOption = "Select an option (R/T/B): ";

    public const string FirstFindByReaderNameOption = "R";

    public const string SecondFindByReaderNameOption = "r";

    public const string FirstFindByBookTitleOption = "T";

    public const string SecondFindByBookTitleOption = "t";

    public const string IdColumn = "Id";

    public const string BookIdColumn = "BookId";

    public const string BookTitleColumn = "BookTitle";

    public const string ReaderIdColumn = "ReaderId";

    public const string ReaderNameColumn = "ReaderName";

    public const string SelectPageOption = "Select an option (N/P/B): ";

    public const string FilteredLoanTitle = "Filtered Loan";

    public const string ReaderName = "reader's name";

    public const string BookTitle = "book's title";
}
