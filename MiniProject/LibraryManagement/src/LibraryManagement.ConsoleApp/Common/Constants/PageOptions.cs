namespace LibraryManagement.ConsoleApp.Common.Constants;

public static class PageOptions
{
    public const int PageSize = 5;

    public const string NextPage = "N. Next page";

    public const string PreviousPage = "P. Previous page";

    public const string FirstNextPageOption = "N";

    public const string SecondNextPageOption = "n";

    public const string FirstPreviousPageOption = "P";

    public const string SecondPreviousPageOption = "p";
}
