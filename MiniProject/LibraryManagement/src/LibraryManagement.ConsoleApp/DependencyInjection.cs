using Microsoft.Extensions.DependencyInjection;

namespace LibraryManagement.ConsoleApp;

public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services.AddSingleton<LibraryManagementApp>();

        return services;
    }
}
