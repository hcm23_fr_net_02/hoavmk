﻿using LibraryManagement.Application;
using LibraryManagement.ConsoleApp;
using LibraryManagement.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);

builder.Services.AddPresentation();
builder.Services.AddApplication();
builder.Services.AddInfrastructure();

using IHost host = builder.Build();

var app = host.Services.GetRequiredService<LibraryManagementApp>();

app.Run();

