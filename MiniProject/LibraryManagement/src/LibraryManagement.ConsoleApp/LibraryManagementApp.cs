using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.ConsoleApp.Common.Constants;
using LibraryManagement.ConsoleApp.Managements;

namespace LibraryManagement.ConsoleApp;

public class LibraryManagementApp
{
    private readonly IReaderService _readerService;
    private readonly IBookService _bookService;
    private readonly IBookRatingService _bookRatingService;
    private readonly ILoanService _loanService;
    private readonly BookManagement _bookManagement;
    private readonly ReaderManagement _readerManagement;
    private readonly LoanManagement _loanManagement;

    public LibraryManagementApp(
        IReaderService readerService,
        IBookService bookService,
        IBookRatingService bookRatingService,
        ILoanService loanService)
    {
        _readerService = readerService;
        _bookService = bookService;
        _bookRatingService = bookRatingService;
        _loanService = loanService;

        _bookManagement = new(_bookService, _bookRatingService);
        _readerManagement = new(_readerService);
        _loanManagement = new(_loanService);
    }

    public void Run()
    {
        string? choice;
        var exit = false;

        while (!exit)
        {
            Console.WriteLine(AppMenu.Title);
            Console.WriteLine(AppMenu.ViewBooks);
            Console.WriteLine(AppMenu.AddBook);
            Console.WriteLine(AppMenu.FilterBooks);
            Console.WriteLine(AppMenu.ViewLoans);
            Console.WriteLine(AppMenu.ManageReaders);
            Console.WriteLine(AppMenu.Exit);
            Console.Write(AppMenu.OptionSelection);
            choice = Console.ReadLine();

            switch (choice)
            {
                case AppMenu.ViewBooksOption:
                    ManageBooks();
                    break;

                case AppMenu.AddBookOption:
                    AddBook();
                    break;

                case AppMenu.FilterBooksOption:
                    FilterBooks();
                    break;
                
                case AppMenu.ManageBorrowedBooksOption:
                    ManageBorrowedBooks();
                    break;

                case AppMenu.ManageReadersOption:
                    ManageReaders();
                    break;
                
                case AppMenu.ExitOption:
                    return;
                
                default:
                    Console.WriteLine(AppMenu.InvalidOption);
                    break;
            }
        }
    }

    public void ManageBooks()
    {
        _bookManagement.ManageBooks();
    }

    public void AddBook()
    {
        _bookManagement.AddBook();
    }

    public void FilterBooks()
    {
        _bookManagement.FilterBooks();
    }

    public void ManageBorrowedBooks()
    {
        _loanManagement.ManageLoans();
    }

    public void ManageReaders()
    {
        _readerManagement.ManageReaders();
    }
}
