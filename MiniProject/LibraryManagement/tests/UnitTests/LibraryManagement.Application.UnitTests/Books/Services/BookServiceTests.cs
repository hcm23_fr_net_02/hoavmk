using System.Collections.Generic;
using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Application.Services;
using LibraryManagement.Application.UnitTests.Books.TestUtils;
using LibraryManagement.Contracts.Books;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Application.UnitTests;

public class BookServiceTests
{
    private readonly IBookService _bookService;
    private readonly Mock<IBookRepository> _mockBookRepository;
    private readonly Mock<IBookRatingRepository> _mockBookRatingRepository;
    private readonly Mock<IReaderRepository> _mockReaderRepository;
    private readonly Mock<ILoanRepository> _mockLoanRepository;

    public BookServiceTests()
    {
        _mockBookRepository = new();
        _mockBookRatingRepository = new();
        _mockReaderRepository = new();
        _mockLoanRepository = new();
        _bookService = new BookService(
            _mockBookRepository.Object,
            _mockBookRatingRepository.Object,
            _mockReaderRepository.Object,
            _mockLoanRepository.Object);
    }

    [Fact]
    public void AddBook_WhenBookIsValid_ShouldAdd()
    {
        // Arrange
        BookRequest bookRequest = CreateBookRequestUtils.CreateDefaultBookRequest();

        // Act
        ErrorOr<Created> result = _bookService.AddBook(bookRequest);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Created);
    }

    [Theory]
    [ClassData(typeof(InvalidPublicationYearData))]
    public void AddBook_WhenPublicationYearIsInvalid_ShouldReturnError(int publicationYear)
    {
        // Arrange
        BookRequest bookRequest = CreateBookRequestUtils.CreateBookRequest(
            TestingConstants.Book.Title,
            TestingConstants.Book.Genre,
            publicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        // Act
        ErrorOr<Created> result = _bookService.AddBook(bookRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidPublicationYear);
    }

    [Theory]
    [ClassData(typeof(InvalidQuantityData))]
    public void AddBook_WhenQuantityIsInvalid_ShouldReturnError(int quantity)
    {
        // Arrange
        BookRequest bookRequest = CreateBookRequestUtils.CreateBookRequest(
            TestingConstants.Book.Title,
            TestingConstants.Book.Genre,
            TestingConstants.Book.PublicationYear,
            quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        // Act
        ErrorOr<Created> result = _bookService.AddBook(bookRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidQuantity);
    }

    [Fact]
    public void BorrowBook_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        var bookId = 1;
        LoanRequest loanRequest = CreateLoanRequestUtils.CreateDefaultLoanRequest();
        _mockBookRepository.Setup(m => m.GetById(bookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Created> result = _bookService.BorrowBook(loanRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookId));
    }

    [Fact]
    public void BorrowBook_WhenBookOutOfStock_ShouldReturnError()
    {
        // Arrange
        Book book = CreateBookUtils.CreateBook(
            title: TestingConstants.Book.Title,
            author: TestingConstants.Book.Author,
            publicationYear: TestingConstants.Book.PublicationYear,
            quantity: 0,
            genre: TestingConstants.Book.Genre,
            description: TestingConstants.Book.Description);

        LoanRequest loanRequest = CreateLoanRequestUtils.CreateLoanRequest(
            borrowedDate: TestingConstants.Loan.BorrowedDate,
            dueDate: TestingConstants.Loan.DueDate,
            booId: book.Id,
            readerId: TestingConstants.Loan.ReaderId);

         _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        // Act
        ErrorOr<Created> result = _bookService.BorrowBook(loanRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.OutOfStock(book.Id));
    }

    [Fact]
    public void BorrowBook_WhenReaderNotFound_ShouldReturnError()
    {
        // Arrange
        int readerId = TestingConstants.Loan.ReaderId;
        Book book = CreateBookUtils.CreateBook(
            title: TestingConstants.Book.Title,
            author: TestingConstants.Book.Author,
            publicationYear: TestingConstants.Book.PublicationYear,
            quantity: TestingConstants.Book.Quantity,
            genre: TestingConstants.Book.Genre,
            description: TestingConstants.Book.Description);

        LoanRequest loanRequest = CreateLoanRequestUtils.CreateLoanRequest(
            borrowedDate: TestingConstants.Loan.BorrowedDate,
            dueDate: TestingConstants.Loan.DueDate,
            booId: book.Id,
            readerId: readerId);

        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockReaderRepository.Setup(m => m.GetById(readerId))
            .Returns<Reader?>(null!);

        // Act
        ErrorOr<Created> result = _bookService.BorrowBook(loanRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Reader.NotFoundValidation(readerId));
    }

    [Fact]
    public void BorrowBook_WhenDueDateIsInvalid_ShouldReturnError()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        LoanRequest loanRequest = CreateLoanRequestUtils.CreateLoanRequest(
            borrowedDate: TestingConstants.Loan.BorrowedDate,
            dueDate: TestingConstants.Loan.BorrowedDate.AddDays(-1),
            booId: book.Id,
            readerId: reader.Id);

        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockReaderRepository.Setup(m => m.GetById(reader.Id))
            .Returns(reader);

        // Act
        ErrorOr<Created> result = _bookService.BorrowBook(loanRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Loan.InvalidDueDate);
    }

    [Fact]
    public void BorrowBook_WhenValid_ShouldBorrow()
    {
         // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        LoanRequest loanRequest = CreateLoanRequestUtils.CreateLoanRequest(
            borrowedDate: TestingConstants.Loan.BorrowedDate,
            dueDate: TestingConstants.Loan.DueDate,
            booId: book.Id,
            readerId: reader.Id);

        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockReaderRepository.Setup(m => m.GetById(reader.Id))
            .Returns(reader);

        // Act
        ErrorOr<Created> result = _bookService.BorrowBook(loanRequest);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Created);
    }

    [Fact]
    public void DeleteBook_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        var bookId = 1;
        _mockBookRepository.Setup(m => m.GetById(bookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Deleted> result = _bookService.DeleteBook(bookId);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookId));
    }

    [Fact]
    public void DeleteBook_WhenBookHasLoans_ShouldReturnError()
    {
        // Arrange
        var book = CreateBookUtils.CreateDefaultBook();
        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockLoanRepository.Setup(m => m.IsAnyLoansWithBook(book.Id))
            .Returns(true);

        // Act
        ErrorOr<Deleted> result = _bookService.DeleteBook(book.Id);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.HadLoans(book.Id));
    }

    [Fact]
    public void DeleteBook_WhenValid_ShouldDelete()
    {
        // Arrange
        var book = CreateBookUtils.CreateDefaultBook();
        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockLoanRepository.Setup(m => m.IsAnyLoansWithBook(book.Id))
            .Returns(false);

        // Act
        ErrorOr<Deleted> result = _bookService.DeleteBook(book.Id);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Deleted);
    }

    [Fact]
    public void EditBook_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        var bookId = 1;
        BookRequest bookRequest = CreateBookRequestUtils.CreateDefaultBookRequest();
        _mockBookRepository.Setup(m => m.GetById(bookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Updated> result = _bookService.EditBook(bookId, bookRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookId));
    }

    [Theory]
    [ClassData((typeof(InvalidPublicationYearData)))]
    public void EditBook_PublicationYearIsInvalid_ShouldReturnError(int publicationYear)
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        BookRequest bookRequest = CreateBookRequestUtils.CreateBookRequest(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            publicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);
        
        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        // Act
        ErrorOr<Updated> result = _bookService.EditBook(book.Id, bookRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidPublicationYear);
    }

    [Theory]
    [ClassData(typeof(InvalidQuantityData))]
    public void EditBook_WhenQuantityIsInvalid_ShouldReturnError(int quantity)
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        BookRequest bookRequest = CreateBookRequestUtils.CreateBookRequest(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            TestingConstants.Book.PublicationYear,
            quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        // Act
        ErrorOr<Updated> result = _bookService.EditBook(book.Id, bookRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidQuantity);
    }

    [Fact]
    public void EditBook_WhenUpdateDataIsValid_ShouldUpdate()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        BookRequest bookRequest = CreateBookRequestUtils.CreateDefaultBookRequest();

        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        // Act
        ErrorOr<Updated> result = _bookService.EditBook(book.Id, bookRequest);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Updated);
        book.Title.Should().Be(bookRequest.Title);
        book.Author.Should().Be(bookRequest.Author);
        book.PublicationYear.Should().Be(bookRequest.PublicationYear);
        book.Quantity.Should().Be(bookRequest.Quantity);
        book.Genre.Should().Be(bookRequest.Genre);
        book.Description.Should().Be(bookRequest.Description);
    }
    
    [Fact]
    public void GetBookById_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        var bookId = 1;
        _mockBookRepository.Setup(m => m.GetById(bookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Book> result = _bookService.GetBookById(bookId);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookId));
    }

    [Fact]
    public void GetBookById_WhenFound_ShouldReturnBook()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        // Act
        ErrorOr<Book> result = _bookService.GetBookById(book.Id);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(book);
    }
     [Fact]
    public void GetBookWithRatingsByBookId_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        var bookId = 1;
        _mockBookRepository.Setup(m => m.GetById(bookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Book> result = _bookService.GetBookWithRatingsByBookId(bookId);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookId));
    }

    [Fact]
    public void GetBookWithRatingsByBookId_WhenFound_ShouldReturnBook()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        List<BookRating> bookRatings = CreateBookRatingUtils.CreateBookRatings();
        _mockBookRepository.Setup(m => m.GetById(book.Id))
            .Returns(book);

        _mockBookRatingRepository.Setup(m => m.GetBookRatingsByBookId(book.Id))
            .Returns(bookRatings);

        // Act
        ErrorOr<Book> result = _bookService.GetBookWithRatingsByBookId(book.Id);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Ratings.Count
            .Should()
            .Be(bookRatings.Count);
    }
}
