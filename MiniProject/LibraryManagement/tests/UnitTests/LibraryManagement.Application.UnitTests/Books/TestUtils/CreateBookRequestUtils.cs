using LibraryManagement.Contracts.Books;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Application.UnitTests.Books.TestUtils;

public static class CreateBookRequestUtils
{
    public static BookRequest CreateBookRequest(
        string title,
        string author,
        int publicationYear,
        int quantity,
        string genre,
        string description)
    {
        return new BookRequest(
            title,
            author,
            publicationYear,
            quantity,
            genre,
            description);
    }

    public static BookRequest CreateDefaultBookRequest()
    {
        return new BookRequest(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            TestingConstants.Book.PublicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);
    }
}
