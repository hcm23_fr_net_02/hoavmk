using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.UnitTests.Books.TestUtils;

public class InvalidPublicationYearData : TheoryData<int>
{
    public InvalidPublicationYearData()
    {
        Add(Book.MinPublicationYear - 1);
        Add(Book.MaxPublicationYear + 1);
    }
}
