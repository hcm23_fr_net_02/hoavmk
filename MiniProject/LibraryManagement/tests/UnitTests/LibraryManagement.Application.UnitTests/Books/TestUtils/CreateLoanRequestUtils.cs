using System;
using LibraryManagement.Contracts.Books;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Application.UnitTests.Books.TestUtils;

public static class CreateLoanRequestUtils
{
    public static LoanRequest CreateLoanRequest(
        DateTime borrowedDate,
        DateTime dueDate,
        int booId,
        int readerId)
    {
        return new LoanRequest(
            borrowedDate,
            dueDate,
            booId,
            readerId);
    }

    public static LoanRequest CreateDefaultLoanRequest()
    {
        return new LoanRequest(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            TestingConstants.Loan.BookId,
            TestingConstants.Loan.ReaderId);
    }
}
