using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Application.Services;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Application.UnitTests.Readers.Services;

public class ReaderServiceTests
{
    private readonly IReaderService _readerService;
    private readonly Mock<IReaderRepository> _mockReaderRepository;
    private readonly Mock<ILoanRepository> _mockLoanRepository;

    public ReaderServiceTests()
    {
        _mockReaderRepository = new();
        _mockLoanRepository = new();
        _readerService = new ReaderService(
            _mockReaderRepository.Object,
            _mockLoanRepository.Object);
    }

    [Fact]
    public void AddReader_WhenReaderValid_ShouldAdd()
    {
        // Arrange
        // Act
        ErrorOr<Created> result = _readerService.AddReader(
            TestingConstants.Reader.FullName,
            TestingConstants.Reader.PhoneNumber,
            TestingConstants.Reader.Address);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Created);
    }

    [Fact]
    public void DeleteReader_WhenReaderNotFound_ShouldReturnError()
    {
        // Arrange
        var readerId = 1;

        _mockReaderRepository.Setup(m => m.GetById(readerId))
            .Returns<Reader?>(null!);

        // Act
        ErrorOr<Deleted> result = _readerService.DeleteReader(readerId);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Reader.NotFoundValidation(readerId));
    }

    [Fact]
    public void DeleteReader_WhenReaderHasLoans_ShouldReturnError()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        _mockReaderRepository.Setup(m => m.GetById(reader.Id))
            .Returns(reader);

        _mockLoanRepository.Setup(m => m.IsAnyLoansWithReader(reader.Id))
            .Returns(true);

        // Act
        ErrorOr<Deleted> result = _readerService.DeleteReader(reader.Id);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Reader.HadLoans(reader.Id));
    }

    [Fact]
    public void DeleteReader_WhenValid_ShouldDelete()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        _mockReaderRepository.Setup(m => m.GetById(reader.Id))
            .Returns(reader);

        _mockLoanRepository.Setup(m => m.IsAnyLoansWithReader(reader.Id))
            .Returns(false);

         // Act
        ErrorOr<Deleted> result = _readerService.DeleteReader(reader.Id);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Deleted);
    }

    [Fact]
    public void UpdateReader_WhenReaderNotFound_ShouldReturnError()
    {
        // Arrange
        var readerId = 1;

        _mockReaderRepository.Setup(m => m.GetById(readerId))
            .Returns<Reader?>(null!);

        // Act
        ErrorOr<Updated> result = _readerService.UpdateReader(
            readerId,
            TestingConstants.Reader.FullName2,
            TestingConstants.Reader.PhoneNumber2,
            TestingConstants.Reader.Address2);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Reader.NotFoundValidation(readerId));
    }

    [Fact]
    public void UpdateReader_WhenUpdateDataIsValid_ShouldUpdate()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        
        _mockReaderRepository.Setup(m => m.GetById(reader.Id))
            .Returns(reader);

        // Act
        ErrorOr<Updated> result = _readerService.UpdateReader(
            reader.Id,
            TestingConstants.Reader.FullName2,
            TestingConstants.Reader.PhoneNumber2,
            TestingConstants.Reader.Address2);

        // Assert
        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Updated);
        reader.FullName.Should().Be( TestingConstants.Reader.FullName2);
        reader.PhoneNumber.Should().Be( TestingConstants.Reader.PhoneNumber2);
        reader.Address.Should().Be( TestingConstants.Reader.Address2);
    }
}
