using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Application.UnitTests.BookRatings.TestUtils;

public class InvalidRatingData : TheoryData<int>
{
    public InvalidRatingData()
    {
        Add(BookRating.MinRating - 1);
        Add(BookRating.MaxRating + 1);
    }
}
