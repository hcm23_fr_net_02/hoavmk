using LibraryManagement.Contracts.BookRatings;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Application.UnitTests.BookRatings.TestUtils;

public static class CreateBookRatingRequestUtils
{
    public static BookRatingRequest CreateBookRatingRequest(
        string title,
        string content,
        int rating,
        int bookId)
    {
        return new BookRatingRequest(
            title,
            content,
            rating,
            bookId);
    }

    public static BookRatingRequest CreateDefaultBookRatingRequest()
        => new(
            TestingConstants.BookRating.Title,
            TestingConstants.BookRating.Content,
            TestingConstants.BookRating.Rating,
            TestingConstants.BookRating.BookId);
}
