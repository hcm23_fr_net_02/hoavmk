using ErrorOr;
using LibraryManagement.Application.Services;
using LibraryManagement.Application.UnitTests.BookRatings.TestUtils;
using LibraryManagement.Contracts.BookRatings;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Application.UnitTests.BookRatings.Services;

public class BookRatingServiceTests
{
    private readonly BookRatingService _bookRatingService;
    private readonly Mock<IBookRatingRepository> _mockBookRatingRepository;
    private readonly Mock<IBookRepository> _mockBookRepository;

    public BookRatingServiceTests()
    {
        _mockBookRatingRepository = new();
        _mockBookRepository = new();
        _bookRatingService = new BookRatingService(
            _mockBookRatingRepository.Object,
            _mockBookRepository.Object);
    }

    [Fact]
    public void AddBookRating_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        BookRatingRequest bookRatingRequest = CreateBookRatingRequestUtils.CreateDefaultBookRatingRequest();

        _mockBookRepository.Setup(m => m.GetById(bookRatingRequest.BookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Created> result = _bookRatingService.AddBookRating(bookRatingRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(bookRatingRequest.BookId));
    }

    [Theory]
    [ClassData(typeof(InvalidRatingData))]
    public void AddBookRating_WhenRatingIsInvalid_ShouldReturnError(int rating)
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();

        BookRatingRequest bookRatingRequest = CreateBookRatingRequestUtils.CreateBookRatingRequest(
            title: TestingConstants.BookRating.Title,
            content: TestingConstants.BookRating.Content,
            rating: rating,
            bookId: book.Id);

        _mockBookRepository.Setup(m => m.GetById(bookRatingRequest.BookId))
            .Returns(book);

        // Act
        ErrorOr<Created> result = _bookRatingService.AddBookRating(bookRatingRequest);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.BookRating.InvalidRatingRange);
    }

    [Fact]
    public void AddBookRating_WhenValid_ShouldAdd()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        BookRatingRequest bookRatingRequest = CreateBookRatingRequestUtils.CreateBookRatingRequest(
            title: TestingConstants.BookRating.Title,
            content: TestingConstants.BookRating.Content,
            rating: TestingConstants.BookRating.Rating,
            bookId: book.Id);

        _mockBookRepository.Setup(m => m.GetById(bookRatingRequest.BookId))
            .Returns(book);

        // Act
        ErrorOr<Created> result = _bookRatingService.AddBookRating(bookRatingRequest);

        // Assert
        result.Value.Should().Be(Result.Created);
    }
}
