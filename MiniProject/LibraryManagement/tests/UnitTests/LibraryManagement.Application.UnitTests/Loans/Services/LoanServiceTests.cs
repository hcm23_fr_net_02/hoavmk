using ErrorOr;
using LibraryManagement.Application.Interfaces.Services;
using LibraryManagement.Application.Services;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Application.UnitTests.Loans.Services;

public class LoanServiceTests
{
    private readonly ILoanService _loanService;
    private readonly Mock<ILoanRepository> _mockLoanRepository;
    private readonly Mock<IBookRepository> _mockBookRepository;

    public LoanServiceTests()
    {
        _mockLoanRepository = new();
        _mockBookRepository = new();
        _loanService = new LoanService(
            _mockLoanRepository.Object,
            _mockBookRepository.Object);
    }

    [Fact]
    public void ReturnBook_WhenLoanNotFound_ShouldReturnError()
    {
        // Arrange
        var loanId = 1;

        _mockLoanRepository.Setup(m => m.GetLoanById(loanId))
            .Returns<Loan?>(null!);

        // Act
        ErrorOr<Success> result = _loanService.ReturnBook(loanId);

        // Assert
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Loan.NotFoundValidation(loanId));
    }

    [Fact]
    public void ReturnBook_WhenBookReturned_ShouldReturnError()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();
        
        loan.ReturnBook();

        _mockLoanRepository.Setup(m => m.GetLoanById(loan.Id))
            .Returns(loan);

        // Act
        ErrorOr<Success> result = _loanService.ReturnBook(loan.Id);

        // Arrange
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Loan.AlreadyReturned(loan.Id));
    }

    [Fact]
    public void ReturnBook_WhenBookNotFound_ShouldReturnError()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();
        
        _mockLoanRepository.Setup(m => m.GetLoanById(loan.Id))
            .Returns(loan);

        _mockBookRepository.Setup(m => m.GetById(loan.BookId))
            .Returns<Book?>(null!);

        // Act
        ErrorOr<Success> result = _loanService.ReturnBook(loan.Id);

        // Arrange
        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.NotFoundValidation(loan.BookId));
    }

    [Fact]
    public void ReturnBook_WhenValid_ShouldReturn()
    {
         // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            TestingConstants.Loan.ReaderId);

        int bookQuantity = book.Quantity;
        
        _mockLoanRepository.Setup(m => m.GetLoanById(loan.Id))
            .Returns(loan);

        _mockBookRepository.Setup(m => m.GetById(loan.BookId))
            .Returns(book);

        // Act
        ErrorOr<Success> result = _loanService.ReturnBook(loan.Id);

        // Arrange
        result.Value.Should().Be(Result.Success);
        loan.ReturnDate.Should().NotBeNull();
        book.Quantity.Should().Be(bookQuantity + 1);
    }
}
