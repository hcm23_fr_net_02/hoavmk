namespace LibraryManagement.UnitTesting.Shared.Constants;

public static partial class TestingConstants
{
    public static class BookRating
    {
        public const string Title = "BookRating";

        public const string Content = "BookRating";

        public const int Rating = 5;

        public const int BookId = 1;
    }
}
