namespace LibraryManagement.UnitTesting.Shared.Constants;

public static partial class TestingConstants
{
    public static class Reader
    {
        public const string FullName = "Reader";

        public const string FullName2 = "FullName";

        public const string FullNameForUpdate = "New FullName";

        public const string PhoneNumber = "Reader";

        public const string PhoneNumber2 = "PhoneNumber";

        public const string PhoneNumberForUpdate = "New PhoneNumber";

        public const string Address = "Redder";

        public const string Address2 = "Address";

        public const string AddressForUpdate = "New Address";
    }
}
