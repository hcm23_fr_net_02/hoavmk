namespace LibraryManagement.UnitTesting.Shared.Constants;

public static partial class TestingConstants
{
    public static class Book
    {
        public const string Title = "book";

        public const string Title2 = "....";

        public const string TitleForUpdate = "title";

        public const string Author = "book";

        public const string AuthorForUpdate = "New author";

        public const int Quantity = 1;

        public const int QuantityForUpdate = 10;

        public const int PublicationYear = 1999;

        public const int PublicationYearForUpdate = 2010;

        public const string Genre = "book";

        public const string GenreForUpdate = "New genre";

        public const string Description = "book";

        public const string DescriptionForUpdate = "New description";
    }
}
