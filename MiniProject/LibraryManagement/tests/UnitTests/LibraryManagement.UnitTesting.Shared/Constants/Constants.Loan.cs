using System;

namespace LibraryManagement.UnitTesting.Shared.Constants;

public static partial class TestingConstants
{
    public static class Loan
    {
        public static readonly DateTime BorrowedDate = DateTime.UtcNow.AddDays(-7);

        public static readonly DateTime DueDate = DateTime.UtcNow.AddDays(2);

        public const int BookId = 1;

        public const int ReaderId = 1;
    }
}
