using System;
using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.UnitTesting.Shared.TestUtils;

public static class CreateLoanUtils
{
    public static Loan CreateLoan(
        DateTime borrowedDate,
        DateTime dueDate,
        int bookId,
        int readerId)
    {
        return Loan.Create(
            borrowedDate,
            dueDate,
            bookId,
            readerId)
            .Value;
    }

    public static Loan CreateDefaultLoan()
        => Loan.Create(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            TestingConstants.Loan.BookId,
            TestingConstants.Loan.ReaderId)
            .Value;
}
