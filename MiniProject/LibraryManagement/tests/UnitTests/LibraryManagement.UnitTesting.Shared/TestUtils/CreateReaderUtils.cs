using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.UnitTesting.Shared.TestUtils;

public static class CreateReaderUtils
{
    public static Reader CreateReader(
        string fullName,
        string phoneNumber,
        string address)
    {
        return Reader.Create(fullName, phoneNumber, address);
    }

    public static Reader CreateDefaultReader()
    => Reader.Create(
            TestingConstants.Reader.FullName,
            TestingConstants.Reader.PhoneNumber,
            TestingConstants.Reader.Address);
}
