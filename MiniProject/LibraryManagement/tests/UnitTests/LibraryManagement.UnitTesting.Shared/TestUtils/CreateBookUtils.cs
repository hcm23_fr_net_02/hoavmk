using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.UnitTesting.Shared.TestUtils;

public static class CreateBookUtils
{
    public static Book CreateBook(
        string title,
        string author,
        int publicationYear,
        int quantity,
        string genre,
        string description)
    {   
        return Book.Create(
        title,
        author,
        publicationYear,
        quantity,
        genre,
        description)
        .Value;
    }

    public static Book CreateDefaultBook()
    {
        return Book.Create(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            TestingConstants.Book.PublicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description)
            .Value;
    }
}
