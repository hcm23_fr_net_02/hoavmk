using System.Collections.Generic;
using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.UnitTesting.Shared.TestUtils;

public static class CreateBookRatingUtils
{
    public static List<BookRating> CreateBookRatings()
    {
        var bookRatings = new List<BookRating>();

        for (var i = 0; i <= 10; i++)
        {
            BookRating bookRating = BookRating.Create(
                title: $"Title {i}",
                content: $"Content {i}",
                rating: 5,
                bookId: 1)
                .Value;

            bookRatings.Add(bookRating);
        }

        return bookRatings;
    }

    public static BookRating CreateBookRating(
        string title,
        string content,
        int rating,
        int bookId)
        => BookRating.Create(
            title,
            content,
            rating,
            bookId)
            .Value;

    public static BookRating CreateDefaultBookRating()
        => BookRating.Create(
            TestingConstants.BookRating.Title,
            TestingConstants.BookRating.Content,
            TestingConstants.BookRating.Rating,
            TestingConstants.BookRating.BookId)
            .Value;
}
