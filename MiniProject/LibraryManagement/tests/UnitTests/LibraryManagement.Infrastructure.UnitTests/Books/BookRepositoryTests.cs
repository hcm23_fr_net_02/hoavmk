using System.Collections.Generic;
using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Repositories;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.Books;

public class BookRepositoryTests
{
    private readonly Mock<IApplicationData> _mockApplicationData;
    private readonly IBookRepository _bookRepository;

    public BookRepositoryTests()
    {
        _mockApplicationData = new();
        _bookRepository = new BookRepository(_mockApplicationData.Object);
    }

    [Fact]
    public void AddBook_WhenBookIsValid_ShouldAdd()
    {
        // Arrange
        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book>());
        
        int bookCount = _mockApplicationData.Object.Books.Count;
        Book book = CreateBookUtils.CreateDefaultBook();

        // Act
        _bookRepository.Add(book);

        // Assert
        Book? addedBook = _bookRepository.GetById(book.Id);

        addedBook.Should().NotBeNull();
        _mockApplicationData.Object.Books.Count.Should().Be(bookCount + 1);
    }

    [Fact]
    public void DeleteBook_WhenBookIsValid_ShouldDelete()
    {
        // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book> { book });

        int bookCount = _mockApplicationData.Object.Books.Count;

        // Act
        _bookRepository.Delete(book);

        // Assert
        _mockApplicationData.Object.Books.Count
            .Should()
            .Be(bookCount - 1);
    }

    [Fact]
    public void GetBookById_WhenBookExists_ShouldReturnBook()
    {
         // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book> { book });
        
        // Act
        Book? foundBook = _bookRepository.GetById(book.Id);

        // Assert
        foundBook.Should().NotBeNull();
        foundBook.Should().Be(book);
    }

    [Fact]
    public void GetBookById_WhenBookNotExists_ShouldReturnNull()
    {
         // Arrange
        Book book = CreateBookUtils.CreateDefaultBook();
        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book> { book });
        
        // Act
        Book? foundBook = _bookRepository.GetById(book.Id + 1);

        // Assert
        foundBook.Should().BeNull();
    }

    [Fact]
    public void GetBooksListPage_WhenBooksNotEmpty_ShouldReturnBookListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        _mockApplicationData.Setup(m => m.Books)
            .Returns(CreateMockDataUtils.GenerateMockBooks());
        
        // Act
        PageResult<Book> result = _bookRepository.GetBooksListPage(page, pageSize);

        // Assert
        result.Items.Count.Should().BeLessThanOrEqualTo(pageSize);
        result.CurrentPage.Should().Be(page);
        result.PageSize.Should().Be(pageSize);
    }
}
