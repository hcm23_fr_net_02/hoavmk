using System.Collections.Generic;
using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Repositories;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.Books;

public class FilterBooksTests
{
    private readonly Mock<IApplicationData> _mockApplicationData;
    private readonly IBookRepository _bookRepository;

    public FilterBooksTests()
    {
        List<Book> books = CreateMockDataUtils.GenerateMockBooks();
        var book = CreateBookUtils.CreateDefaultBook();

        books.Add(book);

        _mockApplicationData = new();
        _mockApplicationData.Setup(m => m.Books)
            .Returns(books);

        _bookRepository = new BookRepository(_mockApplicationData.Object);
    }

    [Fact]
    public void FilterBook_WhenTitleIsProvide_ShouldFilter()
    {
        int page = 1;
        int pageSize = 5;

        PageResult<Book> result = _bookRepository.FilterBooks(
            title: TestingConstants.Book.Title,
            author: null,
            publicationYear: null,
            genre: null,
            page: page,
            pageSize: pageSize);

        result.Items.Count.Should().Be(1);
    }

    [Fact]
    public void FilterBook_WhenAuthorIsProvide_ShouldFilter()
    {
        int page = 1;
        int pageSize = 5;

        PageResult<Book> result = _bookRepository.FilterBooks(
            title: null,
            author: TestingConstants.Book.Author,
            publicationYear: null,
            genre: null,
            page: page,
            pageSize: pageSize);

        result.Items.Count.Should().Be(1);
    }

    [Fact]
    public void FilterBook_WhenPublicationYearIsProvide_ShouldFilter()
    {
        int page = 1;
        int pageSize = 5;

        PageResult<Book> result = _bookRepository.FilterBooks(
            title: null,
            author: null,
            publicationYear: TestingConstants.Book.PublicationYear,
            genre: null,
            page: page,
            pageSize: pageSize);

        result.Items.Count.Should().Be(1);
    }

    [Fact]
    public void FilterBook_WhenGenreIsProvide_ShouldFilter()
    {
        int page = 1;
        int pageSize = 5;

        PageResult<Book> result = _bookRepository.FilterBooks(
            title: null,
            author: null,
            publicationYear: null,
            genre: TestingConstants.Book.Genre,
            page: page,
            pageSize: pageSize);

        result.Items.Count.Should().Be(1);
    }
}
