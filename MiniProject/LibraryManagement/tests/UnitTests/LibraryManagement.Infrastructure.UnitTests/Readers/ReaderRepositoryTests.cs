using System.Collections.Generic;
using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Repositories;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.Readers;

public class ReaderRepositoryTests
{
    private readonly Mock<IApplicationData> _mockApplicationData;
    private readonly IReaderRepository _readerRepository;

    public ReaderRepositoryTests()
    {
        _mockApplicationData = new();
        _readerRepository = new ReaderRepository(_mockApplicationData.Object);
    }

    [Fact]
    public void AddReader_WhenReaderIsValid_ShouldAdd()
    {
        // Arrange
        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader>());

        Reader reader = CreateReaderUtils.CreateDefaultReader();

        // Act
        _readerRepository.Add(reader);

        // Assert
        Reader? addedReader = _readerRepository.GetById(reader.Id);

        _mockApplicationData.Object.Readers.Count.Should().Be(1);
        addedReader.Should().NotBeNull();
        addedReader.Should().Be(reader);
    }

    [Fact]
    public void DeleteReader_WhenReaderIsValid_ShouldDelete()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader> { reader });

        int readerCount = _mockApplicationData.Object.Readers.Count;

        // Act
        _readerRepository.Delete(reader);

        // Assert
        _mockApplicationData.Object.Readers.Count
            .Should()
            .Be(readerCount - 1);
    }

    [Fact]
    public void GetReaderById_WhenReaderExists_ShouldReturnReader()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader> { reader });

        // Act
        Reader? foundReader = _readerRepository.GetById(reader.Id);

        // Assert
        foundReader.Should().NotBeNull();
        foundReader.Should().Be(reader);
    }

    [Fact]
    public void GetReaderById_WhenReaderNotExists_ShouldReturnNull()
    {
        // Arrange
        Reader reader = CreateReaderUtils.CreateDefaultReader();

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader> { reader });

        // Act
        Reader? foundReader = _readerRepository.GetById(reader.Id + 1);

        // Assert
        foundReader.Should().BeNull();
    }

    [Fact]
    public void GetReadersListPage_WhenReadersNotEmpty_ShouldReturnListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(CreateMockDataUtils.GenerateMockReaders());

        // Act
        PageResult<Reader> result = _readerRepository.GetReadersListPage(page, pageSize);

        // Assert
        result.Items.Count.Should().BeLessThanOrEqualTo(pageSize);
        result.CurrentPage.Should().Be(page);
        result.PageSize.Should().Be(pageSize);
    }
}
