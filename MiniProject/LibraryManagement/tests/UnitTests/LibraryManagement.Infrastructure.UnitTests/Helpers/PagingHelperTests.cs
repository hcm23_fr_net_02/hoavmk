using System.Collections.Generic;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Infrastructure.Helpers;
using LibraryManagement.Infrastructure.UnitTests.Helpers.TestUtils;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.Helpers;

public class PagingHelperTests
{
    [Theory]
    [ClassData(typeof(PageIndexData))]
    public void GetListPage_WhenItemsNotEmpty_ShouldReturnPageResult(int page)
    {
        // Arrange
        var pageSize = 5;
        List<Book> books = CreateMockDataUtils.GenerateMockBooks();
        var totalItems = books.Count;
        var totalPages = totalItems / pageSize;

        if (totalItems % pageSize is not 0)
            totalPages++;

        // Act
        PageResult<Book> result = Paging<Book>.GetListPage(books, page, pageSize);

        // Assert
        result.Items.Count.Should().BeLessThanOrEqualTo(pageSize);
        result.CurrentPage.Should().Be(page);
        result.PageSize.Should().Be(pageSize);
        result.TotalPages.Should().Be(totalPages);
    }
}
