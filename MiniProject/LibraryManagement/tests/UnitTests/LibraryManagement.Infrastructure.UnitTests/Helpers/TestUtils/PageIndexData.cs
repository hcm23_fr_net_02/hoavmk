namespace LibraryManagement.Infrastructure.UnitTests.Helpers.TestUtils;

public class PageIndexData : TheoryData<int>
{
    public PageIndexData()
    {
        Add(1);
        Add(2);
        Add(3);
    }
}
