using System.Collections.Generic;
using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Repositories;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.BookRatings;

public class BookRatingRepositoryTests
{
    private readonly Mock<IApplicationData> _mockApplicationData;
    private readonly IBookRatingRepository _bookRatingRepository;

    public BookRatingRepositoryTests()
    {
        _mockApplicationData = new();
        _bookRatingRepository = new BookRatingRepository(
            _mockApplicationData.Object);
    }

    [Fact]
    public void AddBookRating_WhenBookRatingIsValid_ShouldAdd()
    {
        // Arrange
        _mockApplicationData.Setup(m => m.BookRatings)
            .Returns(new List<BookRating>());

        BookRating bookRating = CreateBookRatingUtils.CreateDefaultBookRating();

        // Act
        _bookRatingRepository.Add(bookRating);

        // Assert
        BookRating? addedBookRating = _bookRatingRepository.GetById(bookRating.Id);

        _mockApplicationData.Object.BookRatings.Count.Should().Be(1);
        addedBookRating.Should().NotBeNull();
        addedBookRating.Should().Be(bookRating);
    }

    [Fact]
    public void GetBookRatingsByBookId_WhenBookRatingsContainBookId_ShouldReturnBookRatings()
    {
        // Arrange
        BookRating bookRating = CreateBookRatingUtils.CreateDefaultBookRating();

        _mockApplicationData.Setup(m => m.BookRatings)
            .Returns(new List<BookRating> { bookRating });

        // Act
        List<BookRating> bookRatings = _bookRatingRepository
            .GetBookRatingsByBookId(bookRating.BookId);

        // Assert
        bookRatings.Count.Should().Be(1);
    }

    [Fact]
    public void GetBookRatingsByBookId_WhenBookRatingsNotContainBookId_ShouldReturnEmptyList()
    {
        // Arrange
        BookRating bookRating = CreateBookRatingUtils.CreateDefaultBookRating();

        _mockApplicationData.Setup(m => m.BookRatings)
            .Returns(new List<BookRating> { bookRating });
        
        // Act
        List<BookRating> bookRatings = _bookRatingRepository
            .GetBookRatingsByBookId(bookRating.BookId + 1);

        bookRatings.Count.Should().Be(0);
    }
}
