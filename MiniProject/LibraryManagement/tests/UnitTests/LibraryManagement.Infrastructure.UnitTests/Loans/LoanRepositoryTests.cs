using System.Collections.Generic;
using System.Linq;
using LibraryManagement.Application.Interfaces.Data;
using LibraryManagement.Domain.Common.Contracts;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.Repositories;
using LibraryManagement.Infrastructure.Repositories;
using LibraryManagement.Infrastructure.UnitTests.TestUtils;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Infrastructure.UnitTests.Loans;

public class LoanRepositoryTests
{
    private readonly Mock<IApplicationData> _mockApplicationData;
    private readonly ILoanRepository _loanRepository;

    public LoanRepositoryTests()
    {
        _mockApplicationData = new();
        _loanRepository = new LoanRepository(_mockApplicationData.Object);
    }

    [Fact]
    public void AddLoan_WhenLoanIsValid_ShouldAdd()
    {
        // Arrange
        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan>());

        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        // Act
        _loanRepository.Add(loan);

        // Assert
        Loan? addedLoan = _loanRepository.GetLoanById(loan.Id);

        _mockApplicationData.Object.Loans.Count.Should().Be(1);
        addedLoan.Should().NotBeNull();
        addedLoan.Should().Be(loan);
    }

    [Fact]
    public void GetLoanByBookTitle_WhenFound_ShouldReturnLoansWithBookAndReader()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            reader.Id);
        
        GetLoanByBookTitleSetup(book, reader, loan);

        // Act
        PageResult<Loan> result = _loanRepository.GetLoansByBookTitle(book.Title, page, pageSize);

        // Assert
        var firstFound = result.Items.FirstOrDefault();

        result.Items.Count.Should().Be(1);
        firstFound.Should().NotBeNull();
        firstFound!.Reader.Should().NotBeNull();
        firstFound.Book.Should().NotBeNull();
        firstFound.Book!.Title.ToLower()
            .Contains(book.Title.ToLower()).Should().BeTrue();
    }

    [Fact]
    public void GetLoanByBookTitle_WhenNotFound_ShouldReturnEmptyListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;

        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            reader.Id);

        GetLoanByBookTitleSetup(book, reader, loan);

        // Act
        PageResult<Loan> result = _loanRepository.GetLoansByBookTitle(
            TestingConstants.Book.Title2,
            page,
            pageSize);

        // Assert
        result.Items.Count.Should().Be(0);
    }

    [Fact]
    public void GetLoanById_WhenLoanExists_ShouldReturnLoan()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        // Act
        Loan? result = _loanRepository.GetLoanById(loan.Id);

        // Assert
        result.Should().NotBeNull();
        result.Should().Be(loan);
    }

    [Fact]
    public void GetLoanById_WhenLoanNotExists_ShouldReturnNull()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        // Act
        Loan? result = _loanRepository.GetLoanById(loan.Id + 1);

        // Assert
        result.Should().BeNull();
    }

    [Fact]
    public void GetLoanByReaderName_WhenFound_ShouldReturnLoansWithBookAndReader()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;

        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            reader.Id);

        GetLoanByReaderNameSetup(book, reader, loan);        

        // Act
        PageResult<Loan> result = _loanRepository.GetLoansByCustomerName(
            reader.FullName,
            page,
            pageSize);

        // Assert
        var firstFound = result.Items.FirstOrDefault();

        result.Items.Count.Should().Be(1);
        firstFound.Should().NotBeNull();
        firstFound!.Reader.Should().NotBeNull();
        firstFound.Book.Should().NotBeNull();
        firstFound.Reader!.FullName.ToLower()
            .Contains(reader.FullName.ToLower()).Should().BeTrue();
    }

    [Fact]
    public void GetLoanByReaderName_WhenNotFound_ShouldReturnEmptyListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            reader.Id);

        GetLoanByReaderNameSetup(book, reader, loan);

        // Act
        PageResult<Loan> result = _loanRepository.GetLoansByCustomerName(
            TestingConstants.Reader.FullName2,
            page,
            pageSize);

        // Assert
        result.Items.Count.Should().Be(0);
    }

    // [Fact]
    // public void GetLoansListPage_WhenLoansNotEmpty_ShouldReturnListPage()
    // {
    //     // Arrange
    //     var page = 1;
    //     var pageSize = 5;

    //     _mockApplicationData.Setup(m => m.Loans)
    //         .Returns(CreateMockDataUtils.GenerateMockLoans());

    //     // Act
    //     PageResult<Loan> result = _loanRepository.GetLoansListPage(page, pageSize);

    //     // Assert
    //     result.Items.Count.Should().BeLessThanOrEqualTo(pageSize);
    //     result.CurrentPage.Should().Be(page);
    //     result.PageSize.Should().Be(pageSize);
    // }

    [Fact]
    public void GetLoansWithReaderAndBook_WhenLoansNotEmpty_ShouldReturnListPage()
    {
        // Arrange
        var page = 1;
        var pageSize = 5;
        Book book = CreateBookUtils.CreateDefaultBook();
        Reader reader = CreateReaderUtils.CreateDefaultReader();
        Loan loan = CreateLoanUtils.CreateLoan(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            book.Id,
            reader.Id);

        GetLoanByReaderNameSetup(book, reader, loan);

        // Act
        PageResult<Loan> result = _loanRepository.GetLoansWithBookAndCustomerListPage(
            page,
            pageSize);

        // Assert
        var firstFound = result.Items.FirstOrDefault();

        result.Items.Count.Should().Be(1);
        firstFound.Should().NotBeNull();
        firstFound!.Reader.Should().NotBeNull();
        firstFound.Book.Should().NotBeNull();
    }

    [Fact]
    public void IsAnyLoanWithBookId_WhenHaveLoanWithBookId_ShouldReturnTrue()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        // Act
        bool isAnyLoan = _loanRepository.IsAnyLoansWithBook(loan.BookId);

        // Assert
        isAnyLoan.Should().BeTrue();
    }

    [Fact]
    public void IsAnyLoanWithBookId_WhenHaveNoLoanWithBookId_ShouldReturnFalse()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        // Act
        bool isAnyLoan = _loanRepository.IsAnyLoansWithBook(loan.BookId + 1);

        // Assert
        isAnyLoan.Should().BeFalse();
    }

    [Fact]
    public void IsAnyLoanWithReaderId_WhenHaveLoanWithReaderId_ShouldReturnTrue()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });
        
        // Act
        bool isAnyLoan = _loanRepository.IsAnyLoansWithReader(loan.ReaderId);

        // Assert
        isAnyLoan.Should().BeTrue();
    }

    [Fact]
    public void IsAnyLoanWithReaderId_WhenHaveNoLoanWithReaderId_ShouldReturnFalse()
    {
        // Arrange
        Loan loan = CreateLoanUtils.CreateDefaultLoan();

        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        // Act
        bool isAnyLoan = _loanRepository.IsAnyLoansWithReader(loan.ReaderId + 1);

        // Assert
        isAnyLoan.Should().BeFalse();
    }

    private void GetLoanByBookTitleSetup(Book book, Reader reader, Loan loan)
    {
        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book> { book });

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader> { reader });
    }

    private void GetLoanByReaderNameSetup(Book book, Reader reader, Loan loan)
    {
        _mockApplicationData.Setup(m => m.Loans)
            .Returns(new List<Loan> { loan });

        _mockApplicationData.Setup(m => m.Books)
            .Returns(new List<Book> { book });

        _mockApplicationData.Setup(m => m.Readers)
            .Returns(new List<Reader> { reader });
    }
}
