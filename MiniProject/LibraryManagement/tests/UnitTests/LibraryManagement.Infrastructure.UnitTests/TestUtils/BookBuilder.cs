using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Infrastructure.UnitTests.TestUtils;

public class BookBuilder
{
    public string? Title { get; private set; }

    public string? Author { get; private set; }

    public int PublicationYear { get; private set; }

    public int Quantity { get; private set; }

    public string? Genre { get; private set; }

    public string? Description { get; private set; }

    public BookBuilder WithTitle(string title)
    {
        Title = title;

        return this;
    }

    public BookBuilder WithAuthor(string author)
    {
        Author = author;

        return this;
    }

    public BookBuilder WithPublicationYear(int publicationYear)
    {
        PublicationYear = publicationYear;

        return this;
    }

    public BookBuilder WithQuantity(int quantity)
    {
        Quantity = quantity;

        return this;
    }

    public BookBuilder WithGenre(string genre)
    {
        Genre = genre;

        return this;
    }

    public BookBuilder WithDescription(string description)
    {
        Description = description;

        return this;
    }

    public Book Build()
    {
        return Book.Create(
            Title!,
            Author!,
            PublicationYear,
            Quantity,
            Genre!,
            Description!)
            .Value;
    }
}
