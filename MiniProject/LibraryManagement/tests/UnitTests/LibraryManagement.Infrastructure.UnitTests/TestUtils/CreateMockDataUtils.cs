using System;
using System.Collections.Generic;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Infrastructure.Data;

namespace LibraryManagement.Infrastructure.UnitTests.TestUtils;

public class CreateMockDataUtils
{
    public static void GenerateMockLibraryData(LibraryData libraryData)
    {
        libraryData.Books = GenerateMockBooks();
        libraryData.Readers = GenerateMockReaders();
        libraryData.Loans = GenerateMockLoans();
        libraryData.BookRatings = GenerateBookRatings();
    }

    public static List<Book> GenerateMockBooks()
    {
        var mockBooks = new List<Book>();

        for (var i = 1; i <= 20; i++)
        {
            var mockBook = Book.Create(
                title: $"Title {i}",
                author: $"Author {i}",
                publicationYear: 2000 + i,
                quantity: 5 + i,
                genre: $"Genre {i}",
                description: $"Description {i}"
            )
            .Value;

            mockBooks.Add(mockBook);
        }

        return mockBooks;
    }

    public static List<Reader> GenerateMockReaders()
    {
        var mockReaders = new List<Reader>();

        for (var i = 1; i <= 20; i++)
        {
            var mockReader = Reader.Create(
                fullName: $"FullName {i}",
                phoneNumber: $"PhoneNumber {i}",
                address: $"Address {i}");

            mockReaders.Add(mockReader);    
        }

        return mockReaders;
    }

    public static List<Loan> GenerateMockLoans()
    {
        var mockLoans = new List<Loan>();

        for (var i = 1; i <= 20; i++)
        {
            var mockLoan = Loan.Create(
                borrowedDate: DateTime.UtcNow.AddDays(-7),
                dueDate: DateTime.UtcNow.AddDays(5),
                bookId: i,
                readerId: i)
                .Value;

            mockLoans.Add(mockLoan);
        }

        return mockLoans;
    }

    public static List<BookRating> GenerateBookRatings()
    {
        var mockBookRatings = new List<BookRating>();

        for (var i = 1; i <= 20; i++)
        {
            var mockBookRating = BookRating.Create(
                title: $"Title {i}",
                content: $"Content {i}",
                rating: 5,
                bookId: i)
                .Value;

            mockBookRatings.Add(mockBookRating);
        }

        return mockBookRatings;
    }
}
