using System;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Domain.UnitTests.Loans.TestUtils;

public class InvalidDueDateData : TheoryData<DateTime>
{
    public InvalidDueDateData()
    {
        Add(TestingConstants.Loan.BorrowedDate.AddDays(-1));
    }
}
