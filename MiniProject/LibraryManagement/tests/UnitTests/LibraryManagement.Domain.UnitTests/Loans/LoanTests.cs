using System;
using ErrorOr;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.UnitTests.Loans.TestUtils;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Domain.UnitTests.Loans;

public class LoanTests
{
    [Fact]
    public void CreateLoan_WhenDueDateIsValid_ShouldCreate()
    {
        ErrorOr<Loan> result = Loan.Create(
            TestingConstants.Loan.BorrowedDate,
            TestingConstants.Loan.DueDate,
            TestingConstants.Loan.BookId,
            TestingConstants.Loan.ReaderId);

        Loan loan = result.Value;

        result.IsError.Should().BeFalse();
        loan.BorrowedDate.Should().Be(TestingConstants.Loan.BorrowedDate);
        loan.DueDate.Should().Be(TestingConstants.Loan.DueDate);
        loan.BookId.Should().Be(TestingConstants.Loan.BookId);
        loan.ReaderId.Should().Be(TestingConstants.Loan.ReaderId);
    }

    [Theory]
    [ClassData(typeof(InvalidDueDateData))]
    public void CreateLoan_WhenDueDateIsInvalid_ShouldReturnError(DateTime dueDate)
    {
        ErrorOr<Loan> result = Loan.Create(
            TestingConstants.Loan.BorrowedDate,
            dueDate,
            TestingConstants.Loan.BookId,
            TestingConstants.Loan.ReaderId);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Loan.InvalidDueDate);
    }
}
