using ErrorOr;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.UnitTests.BookRatings.TestUtils;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Domain.UnitTests.BookRatings;

public class BookRatingTests
{
    [Fact]
    public void CreateBookRating_WhenRatingIsValid_ShouldCreate()
    {
        ErrorOr<BookRating> result = BookRating.Create(
            TestingConstants.BookRating.Title,
            TestingConstants.BookRating.Content,
            TestingConstants.BookRating.Rating,
            TestingConstants.BookRating.BookId);

        var bookRating = result.Value;

        result.IsError.Should().BeFalse();
        bookRating.Title.Should().Be(TestingConstants.BookRating.Title);
        bookRating.Content.Should().Be(TestingConstants.BookRating.Content);
        bookRating.Rating.Should().Be(TestingConstants.BookRating.Rating);
        bookRating.BookId.Should().Be(TestingConstants.BookRating.BookId);
    }

    [Theory]
    [ClassData(typeof(InvalidRatingData))]
    public void CreateBookRating_WhenRatingIsInvalid_ShouldReturnError(int rating)
    {
         ErrorOr<BookRating> result = BookRating.Create(
            TestingConstants.BookRating.Title,
            TestingConstants.BookRating.Content,
            rating,
            TestingConstants.BookRating.BookId);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.BookRating.InvalidRatingRange);
    }
}
