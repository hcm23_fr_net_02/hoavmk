using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Domain.UnitTests.Readers;

public class ReaderTest
{
    [Fact]
    public void CreateReader_WhenInfoIsValid_ShouldCreate()
    {
        var reader = Reader.Create(
            TestingConstants.Reader.FullName,
            TestingConstants.Reader.PhoneNumber,
            TestingConstants.Reader.Address);

        reader.FullName.Should().Be(TestingConstants.Reader.FullName);
        reader.PhoneNumber.Should().Be(TestingConstants.Reader.PhoneNumber);
        reader.Address.Should().Be(TestingConstants.Reader.Address);
    }

    [Fact]
    public void UpdateReaderAddress_WhenAddressIsValid_ShouldUpdate()
    {
        var reader = CreateReaderUtils.CreateDefaultReader();

        reader.UpdateAddress(TestingConstants.Reader.Address);

        reader.Address.Should().Be(TestingConstants.Reader.Address);
    }

    [Fact]
    public void UpdateReaderFullName_WhenFullNameIsValid_ShouldUpdate()
    {
        var reader = CreateReaderUtils.CreateDefaultReader();

        reader.UpdateFullName(TestingConstants.Reader.FullNameForUpdate);

        reader.FullName.Should().Be(TestingConstants.Reader.FullNameForUpdate);
    }

    [Fact]
    public void UpdateReaderPhoneNumber_WhenPhoneNumberIsValid_ShouldUpdate()
    {
        var reader = CreateReaderUtils.CreateDefaultReader();

        reader.UpdatePhoneNumber(TestingConstants.Reader.PhoneNumberForUpdate);

        reader.PhoneNumber.Should().Be(TestingConstants.Reader.PhoneNumberForUpdate);
    }
}
