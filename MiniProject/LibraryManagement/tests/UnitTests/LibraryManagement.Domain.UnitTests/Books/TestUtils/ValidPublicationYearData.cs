using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Domain.UnitTests.Books.TestUtils;

public class ValidPublicationYearData : TheoryData<int>
{
    public ValidPublicationYearData()
    {
        Add(TestingConstants.Book.PublicationYear);
        Add(Book.MinPublicationYear);
        Add(Book.MaxPublicationYear);
    }
}
