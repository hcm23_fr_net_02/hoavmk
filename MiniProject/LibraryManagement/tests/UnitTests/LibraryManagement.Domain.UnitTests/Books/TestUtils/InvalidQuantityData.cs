using LibraryManagement.Domain.Entities;

namespace LibraryManagement.Domain.UnitTests.Books.TestUtils;

public class InvalidQuantityData : TheoryData<int>
{
    public InvalidQuantityData()
    {
        Add(Book.MinQuantity - 1);
    }
}
