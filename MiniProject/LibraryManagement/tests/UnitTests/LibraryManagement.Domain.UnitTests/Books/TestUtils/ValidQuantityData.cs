using LibraryManagement.Domain.Entities;
using LibraryManagement.UnitTesting.Shared.Constants;

namespace LibraryManagement.Domain.UnitTests.Books.TestUtils;

public class ValidQuantityData : TheoryData<int>
{
    public ValidQuantityData()
    {
        Add(TestingConstants.Book.Quantity);
        Add(Book.MinQuantity);
    }
}
