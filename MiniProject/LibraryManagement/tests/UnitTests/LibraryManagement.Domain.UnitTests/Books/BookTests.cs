using ErrorOr;
using LibraryManagement.Domain.Common.Errors;
using LibraryManagement.Domain.Entities;
using LibraryManagement.Domain.UnitTests.Books.TestUtils;
using LibraryManagement.UnitTesting.Shared.Constants;
using LibraryManagement.UnitTesting.Shared.TestUtils;

namespace LibraryManagement.Domain.UnitTests.Books;

public class BookTests
{
    [Theory]
    [ClassData(typeof(ValidPublicationYearData))]
    public void CreateBook_WhenPublicationYearIsValid_ShouldCreate(int publicationYear)
    {
        var result = Book.Create(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            publicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        result.IsError.Should().BeFalse();
        result.Value.Title.Should().Be(TestingConstants.Book.Title);
        result.Value.Author.Should().Be(TestingConstants.Book.Author);
        result.Value.PublicationYear.Should().Be(publicationYear);
        result.Value.Quantity.Should().Be(TestingConstants.Book.Quantity);
        result.Value.Genre.Should().Be(TestingConstants.Book.Genre);
        result.Value.Description.Should().Be(TestingConstants.Book.Description);
    }

    [Theory]
    [ClassData(typeof(InvalidPublicationYearData))]
    public void CreateBook_WhenPublicationYearIsInvalid_ShouldReturnError(int publicationYear)
    {
        var result = Book.Create(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            publicationYear,
            TestingConstants.Book.Quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidPublicationYear);
    }

    [Theory]
    [ClassData(typeof(ValidQuantityData))]
    public void CreateBook_WhenQuantityIsValid_ShouldCreate(int quantity)
    {
        var result = Book.Create(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            TestingConstants.Book.PublicationYear,
            quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        result.IsError.Should().BeFalse();
        result.Value.Title.Should().Be(TestingConstants.Book.Title);
        result.Value.Author.Should().Be(TestingConstants.Book.Author);
        result.Value.PublicationYear.Should().Be(TestingConstants.Book.PublicationYear);
        result.Value.Quantity.Should().Be(quantity);
        result.Value.Genre.Should().Be(TestingConstants.Book.Genre);
        result.Value.Description.Should().Be(TestingConstants.Book.Description);
    }

    [Theory]
    [ClassData(typeof(InvalidQuantityData))]
    public void CreateBook_WhenQuantityIsInvalid_ShouldCreate(int quantity)
    {
        var result = Book.Create(
            TestingConstants.Book.Title,
            TestingConstants.Book.Author,
            TestingConstants.Book.PublicationYear,
            quantity,
            TestingConstants.Book.Genre,
            TestingConstants.Book.Description);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidQuantity);
    }

    [Fact]
    public void UpdateBookAuthor_WhenAuthorIsValid_ShouldUpdate()
    {
        var book = CreateBookUtils.CreateDefaultBook();

        book.UpdateAuthor(TestingConstants.Book.Author);

        book.Author.Should().Be(TestingConstants.Book.Author);
    }

    [Fact]
    public void UpdateBookDescription_WhenDescriptionIsValid_ShouldUpdate()
    {
        var book = CreateBookUtils.CreateDefaultBook();

        book.UpdateDescription(TestingConstants.Book.DescriptionForUpdate);

        book.Description.Should().Be(TestingConstants.Book.DescriptionForUpdate);
    }

    [Fact]
    public void UpdateBookGenre_WhenGenreIsValid_ShouldUpdate()
    {
        var book = CreateBookUtils.CreateDefaultBook();

        book.UpdateGenre(TestingConstants.Book.GenreForUpdate);

        book.Genre.Should().Be(TestingConstants.Book.GenreForUpdate);
    }

    [Theory]
    [ClassData(typeof(ValidPublicationYearData))]
    public void UpdateBookPublicationYear_WhenPublicationYearIsValid_ShouldUpdate(int publicationYear)
    {
        var book = CreateBookUtils.CreateDefaultBook();

        ErrorOr<Success> result = book.UpdatePublicationYear(publicationYear);

        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Success);
        book.PublicationYear.Should().Be(publicationYear);
    }

    [Theory]
    [ClassData(typeof(InvalidPublicationYearData))]
    public void UpdateBookPublicationYear_WhenPublicationYearIsInvalid_ShouldReturnError(int publicationYear)
    {
        var book = CreateBookUtils.CreateDefaultBook();

        ErrorOr<Success> result = book.UpdatePublicationYear(publicationYear);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidPublicationYear);
    }

    [Theory]
    [ClassData(typeof(ValidQuantityData))]
    public void UpdateBookQuantity_WhenQuantityIsValid_ShouldUpdate(int quantity)
    {
        var book = CreateBookUtils.CreateDefaultBook();

        ErrorOr<Success> result = book.UpdateQuantity(quantity);

        result.IsError.Should().BeFalse();
        result.Value.Should().Be(Result.Success);
        book.Quantity.Should().Be(quantity);
    }

    [Theory]
    [ClassData(typeof(InvalidQuantityData))]
    public void UpdateBookQuantity_WhenQuantityIsInvalid_ShouldReturnError(int quantity)
    {
        var book = CreateBookUtils.CreateDefaultBook();

        ErrorOr<Success> result = book.UpdateQuantity(quantity);

        result.IsError.Should().BeTrue();
        result.FirstError.Should().Be(Errors.Book.InvalidQuantity);
    }

    [Fact]
    public void UpdateBookTitle_WhenTitleValid_ShouldUpdate()
    {
        var book = CreateBookUtils.CreateDefaultBook();

        book.UpdateTitle(TestingConstants.Book.TitleForUpdate);

        book.Title.Should().Be(TestingConstants.Book.TitleForUpdate);
    }
}
