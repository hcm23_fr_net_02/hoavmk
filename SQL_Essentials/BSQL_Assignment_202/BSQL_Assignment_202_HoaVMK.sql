CREATE DATABASE SQLEssential202;
GO

USE SQLEssential201;
GO


/* Q1. */
CREATE TABLE Trainee (
	TraineeID int PRIMARY KEY IDENTITY(1, 1),
	Full_Name nvarchar(100) NOT NULL,
	Birth_Date date NOT NULL,
	Gender bit NOT NULL,
	ET_IQ int CHECK (ET_IQ between 0 and 20),
	ET_Gmath int CHECK (ET_Gmath between 0 and 20),
	ET_English int CHECK (ET_English between 0 and 50),
	Training_Class nvarchar(20) NOT NULL,
	Evaluation_Notes text
);
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class)
VALUES ('Long Tran', '05-24-2000', 0, 12, 15, 30, '.NET'),
	('Pham Le', '12-04-2000', 0, 7, 12, 20, 'Java'),
	('Van Hoang', '11-12-2000', 0, 11, 14, 10, '.NET'),
	('Phu Quy', '10-26-2000', 0, 12, 5, 15, '.NET'),
	('Nguyen An', '09-11-2000', 0, 16, 7, 20, 'Java'),
	('Nguyen Binh', '07-20-2000', 1, 6, 12, 22, 'Java'),
	('Pham Chinh', '05-29-2000', 0, 8, 16, 11, '.NET'),
	('Pham Duong', '09-04-2000', 1, 9, 10, 8, 'Java'),
	('Pham Dat', '07-21-2000', 0, 12, 5, 32, '.NET'),
	('Pham Duc', '08-05-2000', 1, 12, 12, 24, 'Java')
GO

DELETE FROM Trainee
GO

/* Q2. */
ALTER TABLE Trainee
ADD Fsoft_Account nvarchar(20) UNIQUE NOT NULL
GO

INSERT INTO Trainee (Full_Name, Birth_Date, Gender, ET_IQ, ET_Gmath, ET_English, Training_Class, Fsoft_Account)
VALUES ('Long Tran', '05-24-2000', 0, 12, 15, 30, '.NET', 'TranLong'),
	('Pham Le', '12-04-2000', 0, 7, 12, 20, 'Java', 'LePham'),
	('Van Hoang', '11-12-2000', 0, 11, 14, 10, '.NET', 'HoangVan'),
	('Phu Quy', '10-26-2000', 0, 12, 5, 15, '.NET', 'PhuQuy'),
	('Nguyen An', '09-11-2000', 0, 16, 7, 20, 'Java', 'AnNguyen'),
	('Nguyen Binh', '07-20-2000', 1, 6, 12, 22, 'Java', 'NguyenBinh'),
	('Pham Chinh', '05-29-2000', 0, 8, 16, 11, '.NET', 'PhamChinh'),
	('Pham Duong', '09-04-2000', 1, 9, 10, 8, 'Java', 'PhamDuong'),
	('Pham Dat', '07-21-2000', 0, 12, 5, 32, '.NET', 'PhamDat'),
	('Pham Duc', '08-05-2000', 1, 12, 12, 24, 'Java', 'PhamDuc')
GO

/* Q3. */
CREATE VIEW ET_PassedTrainee
AS
SELECT *
FROM Trainee T
WHERE T.ET_IQ + T.ET_Gmath >= 20
	AND T.ET_IQ >= 8
	AND T.ET_Gmath >= 8
	AND T.ET_English >= 18
GO



/* Q5. */
SELECT *
FROM Trainee T1
WHERE DATALENGTH(T1.Full_Name) = (
	SELECT MAX(DATALENGTH(T.Full_Name)) Max_Name_Length
	FROM Trainee T
)
GO
