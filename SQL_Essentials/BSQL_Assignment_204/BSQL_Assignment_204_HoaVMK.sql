CREATE DATABASE SQLEssential204;
GO

USE SQLEssential204
GO

/* Q1.1 */
CREATE TABLE Movie (
	id int PRIMARY KEY IDENTITY(1, 1),
	name nvarchar(100) NOT NULL,
	duration float NOT NULL CHECK (duration >= 1.0),
	genre int NOT NULL CHECK (genre between 1 and 8),
	director nvarchar(100) NOT NULL,
	box_office_amount decimal(18, 2) NOT NULL,
	comments text
)
GO

/* Q1.2 */
CREATE TABLE Actor (
	id int PRIMARY KEY IDENTITY(1, 1),
	name nvarchar(100) NOT NULL,
	age int NOT NULL,
	average_movie_salary decimal(18, 2) NOT NULL,
	nationality nvarchar(100)
)
GO

/* Q1.3 */
CREATE TABLE ActIn (
	movie_id int NOT NULL FOREIGN KEY REFERENCES Movie(id),
	actor_id int NOT NULL FOREIGN KEY REFERENCES Actor(id),
	PRIMARY KEY (movie_id, actor_id)
)
GO

/* Q2.1 */
ALTER TABLE Movie
ADD ImageLink varchar(1000)UNIQUE NOT NULL
GO

/* Q2.2 */
INSERT INTO Movie (name, duration, genre, director, box_office_amount, ImageLink)
VALUES ('Movie 01', 1.5, 1, 'Mr. A', 20000, 'https://google.com/search?q=a'),
	('Movie 02', 2, 2, 'Mr. B', 100000, 'https://google.com/search?q=b'),
	('Movie 03', 1.2, 3, 'Mr. B', 200000, 'https://google.com/search?q=c'),
	('Movie 04', 1.4, 6, 'Mr. B', 105000, 'https://google.com/search?q=d'),
	('Movie 05', 1.3, 7, 'Mr. B', 10000, 'https://google.com/search?q=e')
GO

INSERT INTO Actor (name, age, average_movie_salary, nationality)
VALUES ('La', 30, 20000, 'Vietnam'),
	('Actor 01', 22, 10000, 'Vietnam'),
	('Actor 02', 18, 10000, 'Vietnam'),
	('Actor 03', 54, 10000, 'Vietnam'),
	('Actor 04', 40, 10000, 'Vietnam')
GO

INSERT INTO ActIn (actor_id, movie_id)
VALUES (1, 1),
	(1, 2),
	(2, 1),
	(3, 1),
	(2, 2),
	(2, 4),
	(3, 3),
	(3, 4),
	(4, 2),
	(4, 4),
	(5, 1),
	(5, 3),
	(5, 5)
GO

UPDATE Actor SET Actor.name = 'Lam' WHERE Actor.name = 'La'
GO

/* Q3.1 */
SELECT *
FROM Actor A
WHERE A.age > 50
GO

/* Q3.2 */
SELECT A.name, A.average_movie_salary
FROM Actor A
ORDER BY A.average_movie_salary
GO

/* Q3.3 */
SELECT M.name
FROM Actor A
INNER JOIN ActIn AI
ON A.id = AI.actor_id
INNER JOIN Movie M
ON AI.movie_id = M.id
WHERE A.name = 'Lam'
GO

/* Q3.4 */
SELECT M.id, M.name
FROM Movie M
INNER JOIN (
	SELECT AI.movie_id
	FROM Movie M
	INNER JOIN ActIn AI
	ON M.id = AI.movie_id
	WHERE M.genre = 1
	GROUP BY AI.movie_id
	HAVING COUNT(AI.actor_id) > 3) AS AM
ON M.id = AM.movie_id

