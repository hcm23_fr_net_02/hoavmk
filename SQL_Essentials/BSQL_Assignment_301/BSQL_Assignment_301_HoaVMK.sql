CREATE DATABASE SQLEssential301
GO

USE SQLEssential301
GO

/* 1. */

INSERT INTO Department (DeptName)
VALUES ('Development'),
	('HR'),
	('Accounting'),
	('Sales'),
	('Marketing'),
	('IT'),
	('Legal'),
	('Support')
GO

SELECT * FROM Department

INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, StartDate, Salary, Level, Status)
VALUES (1, 'Le Giang', '09-20-1999', 'a@gmail.com', 1, '09-20-2023', 1000, 1, 0),
	(2, 'Tran Long', '12-01-2000', 'b@gmail.com', 1, '09-20-2023', 2000, 1, 0),
	(3, 'Le Hoang', '05-16-2000', 'c@gmail.com',  2, '09-20-2023', 1500, 2, 0),
	(4, 'Nguyen An', '11-01-2000', 'd@gmail.com',3, '09-20-2023', 1200, 1, 0),
	(5, 'Nguyen Phuc', '05-26-2001', 'e@gmail.com', 5, '09-20-2023', 1000, 1, 0),
	(6, 'Tran Loc', '09-20-1999', 'f@gmail.com', 1, '09-20-2023', 2500, 4, 0),
	(7, 'Le Pham', '12-01-2000', 'g@gmail.com', 1, '09-20-2023', 2200, 5, 0),
	(8, 'Le Lam', '12-01-2000', 'h@gmail.com', 6, '09-20-2023', 1300, 5, 0),
	(9, 'Le Tai', '12-01-2000', 'j@gmail.com', 6, '09-20-2022', 1300, 5, 0)
GO


INSERT INTO SKILL (SkillName)
VALUES
	('Java'),
	('.NET'),
	('React'),
	('Angular'),
	('Database'),
	('English'),
	('Communication'),
	('Leader')
GO

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate)
VALUES
	(1, 1, 1, '09-20-2023'),
	(1, 2, 2, '09-20-2023'),
	(2, 1, 1, '09-20-2023'),
	(3, 3, 1, '09-20-2023'),
	(4, 5, 2, '09-20-2023'),
	(5, 4, 2, '09-20-2023'),
	(6, 7, 1, '09-20-2023'),
	(7, 8, 3, '09-20-2023')
GO

/* 2. */
SELECT E.EmpName, E.email, D.DeptName
FROM Employee E
INNER JOIN Department D
ON E.DeptNo = D.DeptNo
WHERE DATEDIFF(MONTH, E.StartDate, GETDATE()) >= 6
GO

/* 3. */
SELECT E.EmpName
FROM Employee E
INNER JOIN Emp_Skill ES
ON E.EmpNo = ES.EmpNo
INNER JOIN Skill S
ON ES.SkillNo = S.SkillNo
WHERE S.SkillName = '.NET' OR S.SkillName = 'C++'
GO

UPDATE Employee SET MgrNo = 1 WHERE EmpNo = 2
UPDATE Employee SET MgrNo = 2 WHERE EmpNo = 3
UPDATE Employee SET MgrNo = 3 WHERE EmpNo = 4
UPDATE Employee SET MgrNo = 5 WHERE EmpNo = 4
GO

/* 4. */
SELECT E.EmpName, M.EmpName ManagerName, M.Email ManagerEmail
FROM Employee E
LEFT JOIN Employee M
ON E.EmpNo = M.MgrNo
GO

/* 5. */
SELECT
	DE.DeptNo,
	DE.DeptName,
	DE.EmpNo,
	DE.DeptName,
	DE.BirthDay,
	DE.MgrNo,
	DE.StartDate,
	DE.Salary,
	DE.Email,
	DE.Level,
	DE.Status
FROM
	(SELECT
		D.DeptNo,
		D.DeptName,
		E.EmpNo,
		E.EmpName,
		E.BirthDay,
		E.StartDate,
		E.MgrNo,
		E.Salary,
		E.Status,
		E.Level,
		E.Email,
		COUNT (E.EmpNo) OVER (PARTITION BY E.DeptNo) AS num_employees
	FROM Employee E
	INNER JOIN Department D
	ON E.DeptNo = D.DeptNo) DE
WHERE num_employees >= 2
GO

/* 6. */
SELECT E.EmpName, E.Email, C.num_skills
FROM Employee E
INNER JOIN
	(SELECT E.EmpNo, COUNT(ES.SkillNo) num_skills
	FROM Employee E
	LEFT JOIN Emp_Skill ES
	ON E.EmpNo = ES.SkillNo
	GROUP BY E.EmpNo) C
ON E.EmpNo = C.EmpNo
ORDER BY E.EmpName
GO

/* 7. */
SELECT E.EmpName, E.Email, E.BirthDay
FROM Employee E
INNER JOIN
	(SELECT ES.EmpNo
	FROM Emp_Skill ES
	GROUP BY ES.EmpNo
	HAVING COUNT(ES.SkillNo) > 1) MS
ON E.EmpNo = MS.EmpNo
WHERE E.Status = 0
GO

/* 8. */
CREATE VIEW WorkingEmployees
AS
SELECT E.EmpName, E.Email, E.BirthDay
FROM Employee E
WHERE E.Status = 0
GO

SELECT * FROM WorkingEmployees